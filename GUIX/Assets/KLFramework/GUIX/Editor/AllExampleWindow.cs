using KLFramework.GUIX;
using UnityEditor;
using UnityEngine;

namespace KLFramework.Demo.GUIX
{
    public class AllExampleWindow : EditorWindow
    {
        [MenuItem("KL/Demo/GUI/All", false, 999)]
        public static void Open()
        {
            GetWindow<AllExampleWindow>("All GUIX Demo");
        }

        private IUI _UI;

        void OnGUI()
        {
            bool repaint = false;
            _GetOrCreateUI().Render(10, 10, ref repaint);
            if (repaint)
            {
                Repaint();
            }
        }

        private IUI _GetOrCreateUI()
        {
            if (_UI == null)
            {
                _UI = _CreateUI();
            }

            var size = _UI.Size();
            maxSize = minSize = size + new Vector2(20, 20);
            return _UI;
        }

        private IUI _CreateUI()
        {
            return new TabPanel()
                .SetHighlightTabFontColor(Color.cyan)
                .SetHighlightTabFontSize(16)
                .AddItem("Label", LabelWindow.Create())
                .AddItem("Button", ButtonWindow.Create())
                .AddItem("Checkbox", CheckboxWindow.Create())
                .AddItem("Slider", SliderWindow.Create())
                .AddItem("TextField", TextFieldWindow.Create())
                .AddItem("TextArea", TextAreaWindow.Create())
                .AddItem("ComboBox", ComboBoxWindow.Create())
                .AddItem("Table", TableWindow.Create())
                .AddItem("EditableTable", EditableTableWindow.Create())
                .AddItem("ScrollView", ScrollViewWindow.Create())
                .AddItem("TabPanel", TabPanelWindow.Create())
                .AddItem("CollapsePanel", CollapsePanelWindow.Create())
                .AddItem("Composite - Simple", SimpleCompositeWindow.Create())
                ;
        }
    }
}