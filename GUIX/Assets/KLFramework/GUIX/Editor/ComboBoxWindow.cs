using KLFramework.GUIX;
using KLFramework.GUIX.Editor;
using UnityEditor;
using UnityEngine;
using KLFramework.GUIX.Layout;

namespace KLFramework.Demo.GUIX
{
    public class ComboBoxWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/ComboBox", false, GUIXMenuIndex.COMBO_BOX)]
        public static void Open()
        {
            GetWindow<ComboBoxWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);
            int rowCount = 16;
            int columnCount = 6;
            float baseColor = 0.4f;
            for (int row = 0; row < rowCount; row++)
            {
                var linePanel = new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 5));
                for (int column = 0; column < columnCount; column++)
                {
                    float red = 0, green = 0, blue = 0;
                    red = row / (rowCount - 1f) * (1 - baseColor);
                    green = column / (columnCount - 1f) * (1 - baseColor);

                    var color = new Color(baseColor + red, baseColor + green, baseColor + blue);
                    
                    linePanel.AddUI(
                        new ComboBox()
                            .AddItem("Naruto")
                            .AddItem("Sasuke")
                            .AddItem("Sakura")
                            .SetFontSize(24)
                            .SetFontColor(Colors.ToHSV(color).AddV(-0.6f).ToColor())
                            .OnSelected((current) => { Debug.LogError("CLICK:" + current);})
                    );
                }

                panel.AddUI(linePanel);
            }

            return panel;
        }
    }
}