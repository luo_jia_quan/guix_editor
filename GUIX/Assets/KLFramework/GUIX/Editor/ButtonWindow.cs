using KLFramework.GUIX;
using UnityEditor;
using UnityEngine;
using KLFramework.GUIX.Layout;

namespace KLFramework.Demo.GUIX
{
    public class ButtonWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/Button", false, GUIXMenuIndex.BUTTON)]
        public static void Open()
        {
            GetWindow<ButtonWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);
            int rowCount = 16;
            int columnCount = 6;
            float baseColor = 0.4f;
            for (int row = 0; row < rowCount; row++)
            {
                var linePanel = new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 5));
                for (int column = 0; column < columnCount; column++)
                {
                    float red = 0, green = 0, blue = 0;
                    red = row / (rowCount - 1f) * (1 - baseColor);
                    green = column / (columnCount - 1f) * (1 - baseColor);

                    var color = new Color(baseColor + red, baseColor + green, baseColor + blue);
                    var text = "Button (" + (row < 10 ? "0" + row : row + "") + ", " + (column < 10 ? "0" + column : "" + column) + ")";
                    
                    linePanel.AddUI(
                        new Button(text)
                            .SetBackground(GUIColorTexture.Crystal(color))
                            .SetFontSize(24)
                            .SetFontColor(Colors.ToHSV(color).AddV(-0.6f).ToColor())
                            .AddClickEvent(() => { Debug.Log("Click " + text);})
                    );
                }

                panel.AddUI(linePanel);
            }

            return panel;
        }
    }
}