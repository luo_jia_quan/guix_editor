using KLFramework.GUIX;
using UnityEditor;
using KLFramework.GUIX.Layout;

namespace KLFramework.Demo.GUIX
{
    public class TextAreaWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/TextArea", false, GUIXMenuIndex.TEXT_AREA)]
        public static void Open()
        {
            GetWindow<TextAreaWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);
            Label label = null;
            panel.AddUI(
                    new TextArea().SetText("Description").SetFontSize(16).AddChangeEvent((tf, v) =>
                    {
                        label.SetText("Description : " + v);
                    })
                )
                .AddUI(label = new Label("Description : "));
            
            return panel;
        }
    }
}