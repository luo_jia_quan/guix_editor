using System;
using KLFramework.GUIX;
using KLFramework.GUIX.Layout;
using UnityEditor;
using UnityEngine;

namespace KLFramework.Demo.GUIX
{
    public class SimpleCompositeWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/Composite(Simple)", false, GUIXMenuIndex.SIMPLE_COMPOSITE)]
        public static void Open()
        {
            GetWindow<SimpleCompositeWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            IUI usernameUI;
            IUI passwordUI;
            TextField username;
            TextField password;

            Func<string, string> usernameValidtor = s => string.IsNullOrEmpty(s) ? "Username must not be empty" : null;
            Func<string, string> passwordValidtor = s => string.IsNullOrEmpty(s) ? "Password must not be empty" : (s.Length < 8 ? "Password's length should not be smaller than 8" : null);
            Func<TextField, bool> checkUsername = _InputLine("Username", out username, usernameValidtor, out usernameUI);
            Func<TextField, bool> checkPassword = _InputLine("Password", out password, passwordValidtor, out passwordUI);


            var colors = GUIColorTexture.Crystal(new Color(1, 0, 0, 0.5f));
            foreach (var color in colors)
            {
                Debug.Log(color.Color);
            }
            return new Panel()
                    .AddUI(new Label("Login").SetFontSize(20).SetFontColor(Color.cyan))
                    .AddUI(usernameUI)
                    .AddUI(passwordUI)
                    .AddUI(_Line(
                        new Label("").SetSizeAndReturn(100, -1).SetFontSize(16),
                        new Button("Submit").SetFontSize(16).SetFontColor(Color.cyan * new Color(0.7f, 0.7f, 0.7f, 1)).SetBackground(GUIColorTexture.Crystal(new Color(1,0,0,0.5f))).AddClickEvent(() =>
                        {
                            if (checkUsername(username) && checkPassword(password))
                            {
                                EditorUtility.DisplayDialog("Login", "Login successfully", "OK");
                            }
                            else
                            {
                                EditorUtility.DisplayDialog("Login", "Login failed", "OK");
                            }
                        }))
                    )
                    .SetPadding(5)
                ;
        }

        private static Func<TextField, bool> _InputLine(string title, out TextField textField, Func<string, string> valid, out IUI ui)
        {
            Label label = null;
            textField = new TextField();
            Func<TextField, bool> onCheck = (tf) =>
            {
                var error = valid(tf.Text);
                label.SetText(string.IsNullOrEmpty(error) ? "Correct" : error);
                label.SetFontColor(error != null ? Color.red : Color.green);
                return string.IsNullOrEmpty(error);
            };
            
            Action<TextField> check = (tf) => onCheck(tf);
            ui = _Line(
                new Label(title).SetSizeAndReturn(100, -1).SetFontSize(16),
                textField.SetFontSize(16).SetSizeAndReturn(400, -1).AddChangeEvent((tf, text) => check(tf)),
                label = new Label()
            );
            return onCheck;
        }

        private static Panel _Line(params IUI[] uis)
        {
            return new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 5)).SetPadding(5).AddUI(uis);
        }
    }
}