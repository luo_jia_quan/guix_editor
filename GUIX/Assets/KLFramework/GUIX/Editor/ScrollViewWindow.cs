using KLFramework.GUIX;
using UnityEditor;

namespace KLFramework.Demo.GUIX
{
    public class ScrollViewWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/ScrollView", false, GUIXMenuIndex.SCROLL_VIEW)]
        public static void Open()
        {
            GetWindow<ScrollViewWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            return new ScrollView(400, 300, ButtonWindow.Create());
        }
    }
}