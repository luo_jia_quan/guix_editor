using KLFramework.GUIX;
using UnityEditor;
using UnityEngine;
using KLFramework.GUIX.Layout;

namespace KLFramework.Demo.GUIX
{
    public class SliderWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/Slider", false, GUIXMenuIndex.SLIDER)]
        public static void Open()
        {
            GetWindow<SliderWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);
            int rowCount = 16;
            int columnCount = 7;
            float baseColor = 0f;
            for (int row = 0; row < rowCount; row++)
            {
                var linePanel = new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 5));
                for (int column = 0; column < columnCount; column++)
                {
                    float red = 0, green = 0, blue = 0;
                    float r = (row * columnCount + column) * 255f / columnCount / rowCount;
                    linePanel.AddUI(
                        new Slider(0, 255, r)
                            .SetFontSize(12)
                            .SetFontColor(new Color((255 - r) / 255f, 0, 0))
                            .AddValueEvent((s, value) =>
                            {
                                s.SetFontColor(new Color((255 - value) / 255f, 0, 0));
                            })
                    );
                }

                panel.AddUI(linePanel);
            }

            return panel;
        }
    }
}