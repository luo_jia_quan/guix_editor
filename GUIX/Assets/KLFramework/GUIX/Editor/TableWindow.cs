using KLFramework.GUIX;
using UnityEditor;
using KLFramework.GUIX.Layout;
using KLFramework.GUIX.Tables;

namespace KLFramework.Demo.GUIX
{
    public class TableWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/Table", false, GUIXMenuIndex.TABLE)]
        public static void Open()
        {
            GetWindow<TableWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);

            panel.AddUI(new Table(new []{"Name", "Ninjtsu", "Ability", "Level"}).SetPadding(5).SetDataModel(new ArrayDataModel(new object[][]
            {
                new []{ "ハタケ　カカシ", "万華鏡車輪がん、ライキ", "100", "5"},
                new []{ "渦巻　ナルト", "螺旋がん、仙人もど", "85", "4"},
                new []{ "ウチハ　サスケ", "千鳥、須佐男", "85", "4"},
                new []{ "春野 サクラ", "封印術、桜吹雪の術", "80", "3"}
            })));
            
            return panel;
        }
    }
}