using KLFramework.GUIX.Editor;

namespace KLFramework.GUIX.Tables.Editor
{
    public class ComboBoxRenderCreater : ITableRendererCreater
    {
        private string[] _Items;
        
        public ComboBoxRenderCreater(string[] items)
        {
            _Items = items;
        }

        public ITableRenderer Create(object data, int column, int row, Table table)
        {
            var comboBox = new ComboBox(_Items, int.Parse(data.ToString()));
            comboBox.OnSelected((index =>
            {
                table.GetDataModel().SetData(column, row, index.ToString());    
            }));
            return new UITableRenderer(comboBox, (o, model, c, r, tbl) => { comboBox.Select(int.Parse(o.ToString())); });
        }
    }
}