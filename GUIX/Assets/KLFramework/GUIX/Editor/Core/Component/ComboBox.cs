using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace KLFramework.GUIX.Editor
{
    //******************************************
    // ComboBox
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-23 15:00
    //******************************************
    public class ComboBox : AbstractUI<ComboBox>
    {
        public interface IItem
        {
            string GetItemShowText();
        }
        
        private List<IItem> _Items = new List<IItem>();
        private string[] _ItemStrings = new string[0];
        private Action<int, int, ComboBox> _OnSelected;
        private int _Index;
        
        private bool _Dirty = true;
        private Vector2 _Size;
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;

        public ComboBox() : this(null)
        {
        }

        public ComboBox(string[] items, int index = 0)
        {
            AddItem(items);
            OnSelected((a) => { });
            _Index = index;
        }

        public ComboBox Select(int index)
        {
            _Index = index;
            return this;
        }

        /// <param name="onSelected">
        ///    <int currentIndex, ComboBox comboBox,>
        /// </param>
        /// <returns></returns>
        public ComboBox OnSelected(Action<int, ComboBox> onSelected)
        {
            if (onSelected == null)
            {
                _OnSelected = (current, old, comboxBox) => { };
            }
            else
            {
                _OnSelected = (current, old, comboxBox) => onSelected(current, this);
            }
            return this;
        }

        /// <param name="onSelected">
        ///    <int currentIndex, int oldIndex, ComboBox comboBox,>
        /// </param>
        /// <returns></returns>
        public ComboBox OnSelected(Action<int, int, ComboBox> onSelected)
        {
            if (onSelected == null)
            {
                onSelected = (current, old, comboxBox) => { };
            }
            _OnSelected = onSelected;
            return this;
        }

        /// <param name="onSelected">
        ///    <int currentIndex, int oldIndex>
        /// </param>
        /// <returns></returns>
        public ComboBox OnSelected(Action<int, int> onSelected)
        {
            if (onSelected == null)
            {
                _OnSelected = (current, old, comboxBox) => { };
            }
            else
            {
                _OnSelected = (current, old, comboxBox) => onSelected(current, old);    
            }
            
            return this;
        }
        
        /// <param name="onSelected">
        ///    <int currentIndex>
        /// </param>
        /// <returns></returns>
        public ComboBox OnSelected(Action<int> onSelected)
        {
            if (onSelected == null)
            {
                _OnSelected = (current, old, comboxBox) => { };
            }
            else
            {
                _OnSelected = (current, old, comboxBox) => onSelected(current);    
            }
            
            return this;
        }

        public ComboBox AddItem(IEnumerable<IItem> items)
        {
            _Dirty = true;
            if (items != null)
            {
                foreach (var item in items)
                {
                    _Items.Add(item);
                }
            }
            _ResetItems();
            return this;
        }

        public ComboBox AddItem(params IItem[] items)
        {
            _Dirty = true;
            if (items != null)
            {
                foreach (var item in items)
                {
                    _Items.Add(item);
                }
            }
            _ResetItems();
            return this;
        }

        public ComboBox AddItem(IEnumerable<string> items)
        {
            _Dirty = true;
            if (items != null)
            {
                foreach (var item in items)
                {
                    _Items.Add(new StringItem(item ?? "NULL"));
                }
            }
            _ResetItems();
            return this;
        }

        public ComboBox AddItem(params string[] items)
        {
            _Dirty = true;
            if (items != null)
            {
                foreach (var item in items)
                {
                    _Items.Add(new StringItem(item ?? "NULL"));
                }    
            }
            _ResetItems();
            return this;
        }

        public ComboBox RemoveAll()
        {
            _Dirty = true;
            _Items.Clear();
            _ResetItems();
            return this;
        }

        public ComboBox SetItems(IEnumerable<string> items)
        {
            RemoveAll();
            return AddItem(items);
        }

        public ComboBox SetItems(params string[] items)
        {
            RemoveAll();
            return AddItem(items);
        }
        
        public ComboBox SetItems(IEnumerable<IItem> items)
        {
            RemoveAll();
            return AddItem(items);
        }

        public ComboBox SetItems(params IItem[] items)
        {
            RemoveAll();
            return AddItem(items);
        }

        public ComboBox SetFontSize(int fontSize)
        {
            _FontSize = fontSize;
            return this;
        }

        public ComboBox SetFontColor(Color color)
        {
            _SettedColor = true;
            _FontColor = color;
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var size = SelfSize();
            var style = _GetStyle();
            if (_Items.Count > 0)
            {
                
                if (GUI.Button(new Rect(x, y, size.x, size.y), _Items[_Index].GetItemShowText(), style))
                {
                    GenericMenu menu = new GenericMenu();
                    for (int i = 0; i < _ItemStrings.Length; i++)
                    {
                        int index = i;
                        menu.AddItem(new GUIContent(_ItemStrings[i]), _Index == i, () =>
                        {
                            if (_Index != index)
                            {
                                int oldIndex = _Index;
                                _Index = index;
                                _OnSelected(index, oldIndex, this);
                            }
                        });
                    }
                    menu.ShowAsContext(); 
                }
            }
            return new Rect(x, y, size.x, size.y);
        }

        private GUIStyle _GetStyle()
        {
            var style = new GUIStyle(GUI.skin.button);
            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;
            }
            
            return style;
        }

        protected override Vector2 SelfSize()
        {
            if (_Dirty)
            {
                _Dirty = false;
                _CalclateSize();
            }

            return _Size;
        }

        private void _CalclateSize()
        {
            var style = _GetStyle();
            float maxWidth = 0;
            float maxHeight = 0;
            for (int i = 0; i < _ItemStrings.Length; i++)
            {
                var size = style.CalcSize(new GUIContent(_ItemStrings[i]));
                maxWidth = Mathf.Max(size.x, maxWidth);
                maxHeight = Mathf.Max(size.y, maxHeight);
            }
            
            _Size = new Vector2(maxWidth, maxHeight);
        }

        private void _ResetItems()
        {
            _ItemStrings = new string[_Items.Count];
            for (int i = 0; i < _Items.Count; i++)
            {
                _ItemStrings[i] = _Items[i].GetItemShowText();
            }
        }

        public class DelegateItem : IItem
        {
            private Func<string> _GetItemShowText;

            public DelegateItem(Func<string> getItemShowText)
            {
                _GetItemShowText = getItemShowText;
            }

            public string GetItemShowText()
            {
                return _GetItemShowText();
            }
        }
        
        public class StringItem : DelegateItem
        {
            public StringItem(string text) : base(()=>text)
            {
            }
        }
    }
}