using KLFramework.GUIX;
using UnityEditor;
using UnityEngine;

namespace KLFramework.Demo.GUIX
{
    public class TabPanelWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/TabPanel", false, GUIXMenuIndex.TAB_PANEL)]
        public static void Open()
        {
            GetWindow<TabPanelWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            return new Panel(
                        new TabPanel()
                        .SetTabPosition(TabPanel.Position.LeftTop)
                        .SetHighlightTabFontColor(Color.red)
                        .SetHighlightTabFontSize(16)
                        .AddItem("Label", LabelWindow.Create())
                        .AddItem("Button", ButtonWindow.Create())
                        .AddItem("Checkbox", CheckboxWindow.Create())
                        .AddItem("Slider", SliderWindow.Create())
                        .AddItem("TextField", TextFieldWindow.Create())
                        .AddItem("TextArea", TextAreaWindow.Create())
                        .AddItem("Table", TableWindow.Create())
                        .AddItem("EditableTable", EditableTableWindow.Create())
                        .AddItem("ScrollView", ScrollViewWindow.Create())
                    )
                    .SetPadding(5)
                ;
        }
    }
}