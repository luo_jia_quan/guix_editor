using KLFramework.GUIX;
using UnityEditor;

namespace KLFramework.Demo.GUIX
{
    public abstract class BaseWindow : EditorWindow
    {
        private IUI _UI;

        void OnGUI()
        {
            bool repaint = false;
            _GetOrCreateUI().Render(10, 10, ref repaint);
            if (repaint)
            {
                Repaint();
            }
        }

        private IUI _GetOrCreateUI()
        {
            if (_UI == null)
            {
                _UI = _CreateUI();
            }
            return _UI;
        }

        protected abstract IUI _CreateUI();
    }
}