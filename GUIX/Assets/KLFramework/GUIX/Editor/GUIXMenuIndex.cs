namespace KLFramework.Demo.GUIX
{
    public class GUIXMenuIndex
    {
        public const int LABEL = 0;
        public const int BUTTON = 1;
        public const int CHEKCBOX = 2;
        public const int SLIDER = 3;
        public const int TEXT_FIELD = 4;
        public const int TEXT_AREA = 5;
        public const int COMBO_BOX = 6;
        
        public const int TABLE = 10;
        public const int EDITABLE_TABLE = 11;
        
        public const int SCROLL_VIEW = 20;
        public const int TAB_PANEL = 21;
        public const int COLLAPSE = 22;
        
        public const int SIMPLE_COMPOSITE = 30;
    }
}