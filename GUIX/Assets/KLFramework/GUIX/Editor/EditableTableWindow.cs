using System.Text;
using KLFramework.GUIX;
using UnityEditor;
using KLFramework.GUIX.Layout;
using KLFramework.GUIX.Tables;
using KLFramework.GUIX.Tables.Editor;
using UnityEngine;

namespace KLFramework.Demo.GUIX
{
    public class EditableTableWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/EditableTable", false, GUIXMenuIndex.EDITABLE_TABLE)]
        public static void Open()
        {
            GetWindow<EditableTableWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);

            Table table = null;
            panel.AddUI(table = new Table(new []{"Name", "Ninjtsu", "Ability", "Level", "Rank"}).SetPadding(5)
                    .SetDataModel(new ArrayDataModel(new object[][]
                    {
                        new []{ "ハタケ　カカシ", "万華鏡車輪がん、ライキ", "100", "5", "0"},
                        new []{ "渦巻　ナルト", "螺旋がん、仙人もど", "85", "4", "1"},
                        new []{ "ウチハ　サスケ", "千鳥、須佐男", "85", "4", "1"},
                        new []{ "春野 サクラ", "封印術、桜吹雪の術", "80", "3", "2"}
                    }))
                    .SetRendererCreaters(new ITableRendererCreater[]
                    {
                        new LabelRenderCreater(),
                        new TextFieldRenderCreater(200), 
                        new SliderRenderCreater(0, 100, f => f.ToString()),
                        new SliderRenderCreater(0, 5, f => f.ToString()),
                        new ComboBoxRenderCreater(new string[]{"S", "A", "B", "C"})
                    })
                )
                .AddUI(new Button("Print").AddClickEvent(() =>
                {
                    _Print(table);
                }));
            
            return panel;
        }

        private static void _Print(Table table)
        {
            var dataModel = table.GetDataModel();
            var titles = table.GetTitles();
                    
            StringBuilder stringBuilder = new StringBuilder();
            StringBuilder spliter = new StringBuilder();
                    
            stringBuilder.Append("| ");
            spliter.Append("| ");
            for (int i = 0; i < titles.Length; i++)
            {
                stringBuilder.Append(titles[i]).Append(" | ");
                for (int j = 0; j < titles[i].Length; j++)
                {
                    spliter.Append("-");
                }
                spliter.Append(" | ");
            }
            stringBuilder.Append("\n");
            stringBuilder.Append(spliter).Append("\n");

            for (int row = 0; row < dataModel.Row; row++)
            {
                stringBuilder.Append("| ");
                for (int col = 0; col < dataModel.Column; col++)
                {
                    var data = dataModel.GetData(col, row);
                    stringBuilder.Append(data).Append(" | ");
                }   
                stringBuilder.Append("\n");
            }

            var tableString = stringBuilder.ToString();
            Debug.Log(tableString);
            EditorUtility.DisplayDialog("Table", tableString, "OK");
        }
    }
}