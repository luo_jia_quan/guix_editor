using KLFramework.GUIX;
using UnityEditor;
using KLFramework.GUIX.Layout;

namespace KLFramework.Demo.GUIX
{
    public class TextFieldWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/TextField", false, GUIXMenuIndex.TEXT_FIELD)]
        public static void Open()
        {
            GetWindow<TextFieldWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(5);
            Label label = null;
            panel.AddUI(
                    new TextField().SetText("Input your name").SetFontSize(16).AddChangeEvent((tf, v) =>
                    {
                        label.SetText("Your name is : " + v);
                    })
                )
                .AddUI(label = new Label("Your name is : "));
            
            return panel;
        }
    }
}