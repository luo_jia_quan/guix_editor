using KLFramework.GUIX;
using UnityEditor;

namespace KLFramework.Demo.GUIX
{
    public class CollapsePanelWindow : BaseWindow
    {
        [MenuItem("KL/Demo/GUI/CollapsePanel", false, GUIXMenuIndex.COLLAPSE)]
        public static void Open()
        {
            GetWindow<CollapsePanelWindow>();
        }

        protected override IUI _CreateUI()
        {
            return Create();
        }

        public static IUI Create()
        {
            return new Panel()
                    .AddUI(new CollapsePanel("Label", LabelWindow.Create()))
                    .AddUI(new CollapsePanel("Button", ButtonWindow.Create()))
                    .AddUI(new CollapsePanel("Checkbox", CheckboxWindow.Create()))
                    .AddUI(new CollapsePanel("Slider", SliderWindow.Create()))
                    .AddUI(new CollapsePanel("TextField", TextFieldWindow.Create()))
                    .SetPadding(5)
                ;
        }
    }
}