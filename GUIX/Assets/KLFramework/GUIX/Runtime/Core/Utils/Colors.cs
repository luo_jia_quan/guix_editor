using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // Colors
    // Help to process color
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 11:28
    //******************************************
    public class Colors
    {
        public static HSV ToHSV(Color color)
        {
            return HSV.FromColor(color);
        }
        
        public static Color ToColor(HSV hsv)
        {
            return hsv.ToColor();
        }

        public struct HSV
        {
            public float H;
            public float S;
            public float V;
            
            public static HSV FromColor(Color color)
            {
                float cmax = Mathf.Max(color.r, color.g, color.b);
                float cmin = Mathf.Min(color.r, color.g, color.b);
                float delta = cmax - cmin;

                float s = Mathf.Approximately(cmax, 0) ? 0 : delta / cmax;
                float v = cmax;
                float h = 0;
                if (Mathf.Approximately(delta, 0))
                {
                    h = 0;
                }
                else if (Mathf.Approximately(cmax - color.r, 0))
                {
                    h = 60 * ((color.g - color.b) / delta + 0);
                }
                else if (Mathf.Approximately(cmax - color.g, 0))
                {
                    h = 60 * ((color.b - color.r) / delta + 2);
                }
                else if (Mathf.Approximately(cmax - color.b, 0))
                {
                    h = 60 * ((color.r - color.g) / delta + 4);
                }

                var hsv = new HSV();
                hsv.H = h;
                hsv.S = s;
                hsv.V = v;
                return hsv;
            }

            public static Color ToColor(HSV hsv)
            {
                return hsv.ToColor();
            }

            public HSV SetH(float h)
            {
                this.H = h;
                return this;
            }

            public HSV SetS(float s)
            {
                this.S = s;
                return this;
            }

            public HSV SetV(float v)
            {
                this.V = v;
                return this;
            }
            
            public HSV AddV(float add)
            {
                this.V = this.V + add;
                return this;
            }

            public Color ToColor()
            {
                float v = V;
                float s = S;
                float h = H;
                
                if (Mathf.Approximately(s, 0))
                {
                    return new Color(v, v, v, 1);
                }
                
                h = h / 60f;
                int i = Mathf.CeilToInt(h);
                
                float f = h - i;
                float p = v * (1 - s);
                float q = v * (1 - s * f);
                float t = v * (1 - s * (1 - f));
                
                switch( i ) {
                    case 0:
                        return new Color(v, t, p);
                    case 1:
                        return new Color(q, v, p);
                    case 2:
                        return new Color(p, v, t);
                    case 3:
                        return new Color(p, q, v);
                    case 4:
                        return new Color(t, p, v);
                    default:
                        return new Color(v, p, q);
                }
            }
        }
    }
}