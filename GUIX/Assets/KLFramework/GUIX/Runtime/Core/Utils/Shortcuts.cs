﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace KLFramework.GUIX
{
    public class Shortcuts
    {
        private const int KEY_NO_MODIFY = 0;
        private const int KEY_WITH_CTRL = 1 << 1;
        private const int KEY_WITH_SHIFT = 1 << 2;
        private const int KEY_WITH_ALT = 1 << 3;

        private static Dictionary<int, HashSet<string>> KEY_SYMBOL = new Dictionary<int, HashSet<string>>()
        {
            { KEY_NO_MODIFY, new HashSet<string>(){"_"}},
            { KEY_WITH_CTRL, new HashSet<string>(){"%"}},
            { KEY_WITH_SHIFT, new HashSet<string>(){"#"}},
            { KEY_WITH_ALT, new HashSet<string>(){"&"}},
            { KEY_WITH_CTRL|KEY_WITH_SHIFT, new HashSet<string>(){"%#", "#%"}},
            { KEY_WITH_CTRL|KEY_WITH_ALT, new HashSet<string>(){"%&", "&%"}},
            { KEY_WITH_SHIFT|KEY_WITH_ALT, new HashSet<string>(){"#&", "&#"}},
            { KEY_WITH_SHIFT|KEY_WITH_ALT|KEY_WITH_CTRL, new HashSet<string>(){ "%#&", "%&#", "#%&", "#&%", "&#%", "&%#"}},
        };

        private Dictionary<KeyCode, string> _KeyCodeToString = new Dictionary<KeyCode, string>();
        private Dictionary<string, List<Action>> _ShortcutActions = new Dictionary<string, List<Action>>();

        public Shortcuts()
        {
            var typeKeyCode = typeof(KeyCode);
            StringBuilder stringBuilder = new StringBuilder("KeyCodes\n");

            //special keycode
            _KeyCodeToString.Add(KeyCode.Delete, "DEL");

            for (int i = 0; i < 26; i++)
            {
                char c = (char)((int)'A' + i);
                string keyChar = c + "";

                var keycodeField = typeKeyCode.GetField(keyChar, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                KeyCode keyCode = (KeyCode)keycodeField.GetValue(typeKeyCode);
                _KeyCodeToString.Add(keyCode, keyChar);

                stringBuilder.Append(keyCode).Append(" > ").AppendLine(keyChar);
            }

            //Debug.LogError(stringBuilder);
        }

        public void SetKeyCode(Dictionary<KeyCode, string> keyCodeDict)
        {
            foreach (var keyCode in keyCodeDict)
            {
                if (_KeyCodeToString.ContainsKey(keyCode.Key))
                {
                    _KeyCodeToString[keyCode.Key] = keyCode.Value;
                }
                else
                {
                    _KeyCodeToString.Add(keyCode.Key, keyCode.Value);
                }
            }
        }

        public void AddShortcut(string shortcut, Action action)
        {
            shortcut = shortcut.ToUpper();
            List<Action> actions = null;
            if (_ShortcutActions.ContainsKey(shortcut))
            {
                actions = _ShortcutActions[shortcut];
            }
            else
            {
                actions = new List<Action>();
                _ShortcutActions.Add(shortcut, actions);
            }

            actions.Add(action);
        }

        public void ApplyShortcut()
        {
            if (Event.current.type == EventType.KeyUp)
            {
                int status = KEY_NO_MODIFY;
                if (Event.current.control)
                {
                    status |= KEY_WITH_CTRL;
                }
                if (Event.current.alt)
                {
                    status |= KEY_WITH_ALT;
                }
                if (Event.current.shift)
                {
                    status |= KEY_WITH_SHIFT;
                }

                //Debug.LogError("Keyup ： " + Event.current.keyCode + " - " + status);
                if (_KeyCodeToString.ContainsKey(Event.current.keyCode))
                {
                    foreach (var symbol in KEY_SYMBOL[status])
                    {
                        string shortcut = symbol + _KeyCodeToString[Event.current.keyCode];
                        if (_ShortcutActions.ContainsKey(shortcut))
                        {
                            var actions = _ShortcutActions[shortcut];
                            foreach (var action in actions)
                            {
                                action();
                            }
                        }
                    }
                }
            }
        }
    }
}