using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace KLFramework.GUIX
{
    
    //******************************************
    // GUIColorTexture
    // Help to create pure color texture and cache it
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-02-05 23:53
    //******************************************
    public class GUIColorTexture
    {
        public static Texture2D BLACK;
        public static Texture2D WHITE;
        public static Texture2D GRAY;
        public static Texture2D RED;
        public static Texture2D GREEN;
        public static Texture2D BLUE;
        
        private static Dictionary<Color, Texture2D> _ColorTextures = new Dictionary<Color, Texture2D>();
        private static Dictionary<string, Texture2D> _GradientTextures = new Dictionary<string, Texture2D>();

        static GUIColorTexture()
        {
            BLACK = GetOrCreateColorTexture(Color.black);
            WHITE = GetOrCreateColorTexture(Color.white);
            GRAY = GetOrCreateColorTexture(Color.gray);
            RED = GetOrCreateColorTexture(Color.red);
            GREEN = GetOrCreateColorTexture(Color.green);
            BLUE = GetOrCreateColorTexture(Color.blue);
        }
        
        public class GradientInfo
        {
            public readonly float Position;
            public readonly Color Color;

            public GradientInfo(float position, Color color)
            {
                Position = position;
                Color = color;
            }
        }
        
        public static Texture2D GetOrCreateGradientTexture(GradientInfo[] gradientInfos, int size = 64, bool horizontal = false)
        {
            const int min = 2;
            const int max = 2048;
            
            if (size < 2)
            {
                throw new Exception("Size can not smaller than " + min);
            }

            if (size > max)
            {
                throw new Exception("Size is not need larger than " + max);
            }

            string key = "XGRAD:" + _GradientKey(gradientInfos, size, horizontal);
            if (_GradientTextures.ContainsKey(key))
            {
                return _GradientTextures[key];
            }

            var t2d = new Texture2D(size, size, TextureFormat.RGBA32, false);
            t2d.filterMode = FilterMode.Bilinear;

            Func<float, Color> calColor = r =>
            {
                if (r <= gradientInfos[0].Position)
                {
                    return gradientInfos[0].Color;
                }
                if (r >= gradientInfos[gradientInfos.Length-1].Position)
                {
                    return gradientInfos[gradientInfos.Length-1].Color;
                }

                for (int i = 0; i < gradientInfos.Length; i++)
                {
                    if (gradientInfos[i].Position >= r)
                    {
                        float positionSeg = gradientInfos[i].Position - gradientInfos[i - 1].Position;
                        if (Mathf.Approximately(positionSeg, 0))
                        {
                            return gradientInfos[i].Color;
                        }

                        return Color.Lerp(gradientInfos[i-1].Color, gradientInfos[i].Color, (r - gradientInfos[i-1].Position) / positionSeg);
                    }
                }
                
                return gradientInfos[gradientInfos.Length-1].Color;
            }; 

            for (int x = 0; x < size; x++)
            {
                float rx = horizontal ? x / (size - 1f) : 1;
                for (int y = 0; y < size; y++)
                {
                    float ry = horizontal ? 1 : y / (size - 1f);
                    float r = 1 - rx * ry;
                    t2d.SetPixel(x, y, calColor(r));
                }   
            }

            t2d.Apply();
            
            _GradientTextures.Add(key, t2d);
            return t2d;
        }

        public static GradientInfo[] Crystal(Color color)
        {
            var hsv = Colors.ToHSV(color);

            float[] positions = 
            {
                0f,
                0.1f,
                0.3f,
                0.3f,
                0.7f,
                0.9f,
                1f
            };
            
            Colors.HSV[] hsvs = 
            {
                Colors.ToHSV(new Color(0.9f, 1f, 0.9f, 1)),
                Colors.ToHSV(new Color(0.8f, 0.9f, 0.8f, 1)),
                Colors.ToHSV(new Color(0.5f, 1f, 0.5f, 1)),
                Colors.ToHSV(new Color(0.3f, 1f, 0.3f, 1)),
                Colors.ToHSV(new Color(0.3f, 1f, 0.3f, 1)),
                Colors.ToHSV(new Color(0.5f, 1f, 0.5f, 1)),
                Colors.ToHSV(new Color(0.9f, 1f, 0.9f, 1))
            };
            
            GradientInfo[] gradientInfos = new GradientInfo[hsvs.Length];
            for (int i = 0; i < gradientInfos.Length; i++)
            {
                var ccolor = hsvs[i].SetH(hsv.H).SetV(hsvs[i].V * hsv.V).SetS(hsvs[i].S * hsv.S).ToColor();
                ccolor.a = color.a;
                gradientInfos[i] = new GradientInfo(positions[i], ccolor);
            }

            return gradientInfos;
        }

        public static Texture2D GetOrCreateGradientTexture(Color start, Color end, int size = 64, bool horizontal = false)
        {
            const int min = 2;
            const int max = 2048;
            
            if (size < 2)
            {
                throw new Exception("Size can not smaller than " + min);
            }

            if (size > max)
            {
                throw new Exception("Size is not need larger than " + max);
            }

            string key = "GRAD:" + _Key(start, end, size, horizontal);
            if (_GradientTextures.ContainsKey(key))
            {
                return _GradientTextures[key];
            }

            var t2d = new Texture2D(size, size, TextureFormat.RGBA32, false);
            t2d.filterMode = FilterMode.Bilinear;

            for (int x = 0; x < size; x++)
            {
                float rx = horizontal ? x / (size - 1f) : 1;
                for (int y = 0; y < size; y++)
                {
                    float ry = horizontal ? 1 : y / (size - 1f);
                    t2d.SetPixel(x, y, Color.Lerp(start, end, 1 - rx * ry));
                }   
            }

            t2d.Apply();
            
            _GradientTextures.Add(key, t2d);
            return t2d;
        }
        
        private static string _GradientKey(GradientInfo[] gradientInfos, int size, bool horizontal)
        {
            StringBuilder stringBuilder = new StringBuilder();
            
            for (int i = 0; i < gradientInfos.Length; i++)
            {
                stringBuilder.Append(_Key(gradientInfos[i])).Append("_");
            }

            stringBuilder.Append(size).Append("_").Append((horizontal ? 0 : 1));
            return stringBuilder.ToString();
        }

        private static string _Key(GradientInfo gradientInfo)
        {
            return (int)(gradientInfo.Position * 1000) + ":" + _Key(gradientInfo.Color);
        }

        private static string _Key(Color start, Color end, int size, bool horizontal)
        {
            int startKey = _Key(start);
            int endKey = _Key(end);
            return startKey + "_" + endKey + "_" + size + "_" + (horizontal ? 0 : 1);
        }

        private static int _Key(Color color)
        {
            return ((int)(color.r * 255) << 24) | ((int)(color.g * 255) << 16) | ((int)(color.b * 255) << 8) | ((int)(color.a * 255) & 0xFF);
        }

        public static Texture2D GetOrCreateColorTexture(Color color)
        {
            if (_ColorTextures.ContainsKey(color) && _ColorTextures[color])
            {
                return _ColorTextures[color];
            }
            
            var texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture2D.filterMode = FilterMode.Point;
            texture2D.SetPixel(0, 0, color);
            texture2D.Apply();
            
            if (_ColorTextures.ContainsKey(color))
            {
                _ColorTextures[color] = texture2D;
            }
            else
            {
                _ColorTextures.Add(color, texture2D);    
            }
            
            return texture2D;
        }

        public static void ClearCache()
        {
            foreach(var tex in _ColorTextures.Values) 
            {
                if (tex != null)
                {
                    GameObject.Destroy(tex);
                }
            }
            _ColorTextures.Clear();
            
            foreach(var tex in _GradientTextures.Values) 
            {
                if (tex != null)
                {
                    GameObject.Destroy(tex);
                }
            }
            _GradientTextures.Clear();
        }
    }
}