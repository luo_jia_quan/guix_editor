﻿using System;
using UnityEngine;


namespace KLFramework.GUIX
{
	//******************************************
	// IUI
	// Define how a ui should looks like
	//
	// @Author: Kakashi
	// @Email: junhong.cai@kunlun-inc.com
	// @Date: 2020-04-07 10:22
	//******************************************
	public interface IUI
	{
		Vector2 Size();

		void SetSize(int width, int height);
		
		Rect Render(int x, int y, ref bool repaint);
	}

	/// <summary>
	/// Abstract implementation of IUI.
	/// Full of following features :
	/// 1. padding
	/// 2. border
	///
	///                                       border top
	///                                           |
	///                                           V
	///                +-----------------------------------------------------+
	///                |                          |                          |
	///                |                     padding top                     |
	///                |                          |                          |
	///                |                +-------------------+                |
	///                |                |                   |                |
	/// border left -> |- padding left -|     size self     |- padding right-| <- border right
	///                |                |                   |                |
	///                |                +-------------------+                |
	///                |                          |                          |
	///                |                    padding bottom                   |
	///                |                          |                          |
	///                +-----------------------------------------------------+
	///                                           ^
	///                                           |
	///                                      border bottom
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class AbstractUI<T> : IUI where T : IUI
	{
		private readonly int[] _Paddings = new int[4];
		private readonly int[] _Borders = new int[4];
		private Color _BorderColor = Color.black;

		private Texture2D _Background;

		private Vector2 _FixedSize = new Vector2(-1, -1);

		public T SetBackground(Texture2D texture2D)
		{
			_Background = texture2D;
			return (T)(IUI)this;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="borders">new bool[]{TOP、RIGHT、BOTTOM、LEFT}</param>
		/// <returns></returns>
		public T SetBorders(params int[] borders)
		{
			if (borders == null)
			{
				_Borders[(int)Direction.TOP] = _Borders[(int)Direction.RIGHT] = _Borders[(int)Direction.BOTTOM] = _Borders[(int)Direction.LEFT] = 0;
			}
			else
			{
				for (int i = 0; i < _Borders.Length && i < borders.Length; i++)
				{
					_Borders[i] = borders[i];
				}
			}
			return (T)(IUI)this;
		}

		public T SetBorder(int border)
		{
			return SetBorders(border, border, border, border);
		}

		public T SetBorder(Direction dir, int border)
		{
			_Borders[(int)dir] = border;
			return (T)(IUI)this;
		}
		
		public T SetBorder(int dir, int border)
		{
			return SetBorder(Directions.ValueOf(dir), border);
		}
		
		public T SetBorderColor(Color color)
		{
			_BorderColor = color;
			return (T)(IUI)this;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="paddings">new int[]{TOP、RIGHT、BOTTOM、LEFT}</param>
		/// <returns></returns>
		public T SetPaddings(params int[] paddings)
		{
			if (paddings == null)
			{
				_Paddings[(int)Direction.TOP] = _Paddings[(int)Direction.RIGHT] = _Paddings[(int)Direction.BOTTOM] = _Paddings[(int)Direction.LEFT] = 0;
			}
			else
			{
				for (int i = 0; i < paddings.Length && i < _Paddings.Length; i++)
				{
					_Paddings[i] = paddings[i];
				}
			}
			return (T)(IUI)this;
		}

		public T SetPadding(int padding)
		{
			return SetPaddings(padding, padding, padding, padding);
		}

		public T SetPadding(Direction dir, int padding)
		{
			_Paddings[(int)dir] = padding;
			return (T)(IUI)this;
		}
		
		public T SetPadding(int dir, int padding)
		{
			return SetPadding(Directions.ValueOf(dir), padding);
		}

		public void SetSize(int width, int height)
		{
			_FixedSize = new Vector2(width, height);
		}
		
		public virtual T SetSizeAndReturn(int width, int height)
		{
			SetSize(width, height);
			return (T)(IUI)this;
		}

		public Vector2 Size()
		{
			if (_FixedSize.x >= 0 && _FixedSize.y >= 0)
			{
				return _FixedSize;
			}

			Vector2 selfSize = SelfSize();
			
			int horizontalPadding = _Paddings[(int) Direction.LEFT] + _Paddings[(int) Direction.RIGHT];
			int verticalPadding = _Paddings[(int)Direction.TOP] + _Paddings[(int)Direction.BOTTOM];

			if (_FixedSize.x >= 0)
			{
				selfSize.x = _FixedSize.x - horizontalPadding;
			}
			
			if (_FixedSize.y >= 0)
			{
				selfSize.y = _FixedSize.y - verticalPadding;
			}
			
			return new Vector2(selfSize.x + horizontalPadding, selfSize.y + verticalPadding);
		}

		public Rect Render(int x, int y, ref bool repaint)
		{
			int horizontalPadding = _Paddings[(int) Direction.LEFT] + _Paddings[(int) Direction.RIGHT];
			int verticalPadding = _Paddings[(int)Direction.TOP] + _Paddings[(int)Direction.BOTTOM];
			
			if (_Background != null)
			{
				var size = SelfSize() + new Vector2(horizontalPadding, verticalPadding);
				if (_FixedSize.x >= 0)
				{
					size.x = _FixedSize.x;
				}
			
				if (_FixedSize.y >= 0)
				{
					size.y = _FixedSize.y;
				}
				
				GUI.DrawTexture(new Rect(x, y, size.x, size.y), _Background);
			}

			var rect = RenderSelf(x + _Paddings[(int) Direction.LEFT], y + _Paddings[(int) Direction.TOP], ref repaint);
			var totalRect = new Rect(x, y, rect.width + horizontalPadding, rect.height + verticalPadding);

			if (_FixedSize.x >= 0)
			{
				totalRect.width = _FixedSize.x;
			}
			
			if (_FixedSize.y >= 0)
			{
				totalRect.height = _FixedSize.y;
			}

			RenderBorder(totalRect, _Borders, _BorderColor);
			
			return totalRect;
		}

		protected void RenderBorder(Rect rect, int[] borders, Color borderColor)
		{
			if (borders[(int) Direction.TOP] > 0)
			{
				GUI.DrawTexture(new Rect(rect.x, rect.y, rect.width, borders[(int) Direction.TOP]), GUIColorTexture.GetOrCreateColorTexture(borderColor), ScaleMode.StretchToFill);	
			}

			if (borders[(int) Direction.RIGHT] > 0)
			{
				GUI.DrawTexture(new Rect(rect.x + rect.width - borders[(int) Direction.RIGHT], rect.y, borders[(int) Direction.RIGHT], rect.height), GUIColorTexture.GetOrCreateColorTexture(borderColor), ScaleMode.StretchToFill);	
			}

			if (borders[(int) Direction.BOTTOM] > 0)
			{
				GUI.DrawTexture(new Rect(rect.x, rect.y + rect.height - borders[(int) Direction.BOTTOM], rect.width, borders[(int) Direction.BOTTOM]), GUIColorTexture.GetOrCreateColorTexture(borderColor), ScaleMode.StretchToFill);	
			}

			if (borders[(int) Direction.LEFT] > 0)
			{
				GUI.DrawTexture(new Rect(rect.x, rect.y, borders[(int) Direction.LEFT], rect.height), GUIColorTexture.GetOrCreateColorTexture(borderColor), ScaleMode.StretchToFill);	
			}
		}

		protected abstract Rect RenderSelf(int x, int y, ref bool repaint);

		protected abstract Vector2 SelfSize();
		
	}
}