﻿using System;
using SerializeExtend;
using UnityEngine;
using Reflections = KLFramework.Utils.Reflections;

namespace KLFramework.GUIX.Timeline
{
    public abstract class AbstractNode
    {
        [CustomSerializeField]
        [Reflections.CloneField]
        protected float start;
        [CustomSerializeField]
        [Reflections.CloneField]
        protected float end;
        [Reflections.CloneField]
        [CustomSerializeField]
        public int LayerIndex { get; set; }  //belong to which layer
        [Reflections.CloneField]
        [CustomSerializeField]
        public Type DataType { get; set; }

        protected abstract void RenderSelf(int x, int y, int width, int height);

        #region External Methods
        public bool IsRepaintInspector { get; set; }
        [Reflections.CloneField]
        public bool OnSelect { get; set; }

        public void Render(int x, int y, int width, int height)
        {
            RenderSelf(x, y, width, height);
        }

        public float GetStart()
        {
            return this.start;
        }

        public float GetEnd()
        {
            return this.end;
        }

        public void SetStartAndEnd(float start, float end, float duration = 0)
        {
            this.start = Mathf.Max(0, start);
            this.end = Mathf.Max(end, duration);
        }

        #endregion

    }
}
