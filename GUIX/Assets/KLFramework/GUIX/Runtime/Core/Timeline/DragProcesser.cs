﻿using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public interface IDragProcesser
    {
        bool IsInDragArea();
        void Process();
    }

    public class DragProcesser : IDragProcesser
    {
        public delegate void OnDrag(Vector2 delta, Vector2 mousePosition);
        public delegate bool IsCanDragArea(Vector2 mousePosition);

        public IsCanDragArea CanDragArea;
        public OnDrag OnDragHandler;

        public DragProcesser(IsCanDragArea canDragArea, OnDrag onDragHandler)
        {
            CanDragArea = canDragArea;
            OnDragHandler = onDragHandler;
        }

        public bool IsInDragArea()
        {
            return CanDragArea(Event.current.mousePosition);
        }

        public void Process()
        {
            OnDragHandler(Event.current.delta, Event.current.mousePosition);
        }
    }
}
