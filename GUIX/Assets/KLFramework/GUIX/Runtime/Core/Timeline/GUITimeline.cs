﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using KLFramework.GUIX.Timeline.Editor;
using SerializeExtend;
using UnityEditor;
using UnityEditor.SceneManagement;
using Object = UnityEngine.Object;
using Reflections = KLFramework.Utils.Reflections;

namespace KLFramework.GUIX.Timeline
{
    [CreateAssetMenu]
    public class GUITimeline : ScriptableObject, ITimeline
    {
        public string Path { get; set; }

        private float _RulerScale = 1;
        private float _MinRulerScale = TimelineConst.MIN_RULER_SCALE;
        private int _BaseFramePadding = TimelineConst.BASE_FRAME_PADDING;

        private float _LineBoundaryOffset = 0;
        private float _RulerOffset = 0;
        private float _VerticalOffset = 0;
        private float _HorizontalOffset = 0;
        private float _MouseWheelPrecision = TimelineConst.MOUSE_WHEEL_PRECISION;
        private float _MouseDragPrecision = TimelineConst.MOUSE_DRAG_PRECISION;
        private int _TimeRulerHeight = TimelineConst.TIME_RULER_HEIGHT;
        private int _LayerHeight = TimelineConst.LAYER_HEIGHT;
        private int _LayerInterval = TimelineConst.LAYER_INTERVAL;
        private int _ScrollRectHeight = TimelineConst.SCROLL_RECT_HEIGHT;
        private int _VerticalScrollRectWidth = TimelineConst.SCROLL_RECT_HEIGHT;
        private int _ScrollZoomHandleSize = TimelineConst.SCROLL_ZOOM_HANDLE_SIZE;
        private int _BaseFrameWidth = TimelineConst.BASE_FRAME_WIDTH;
        private int _TimePointerWidth = TimelineConst.TIME_POINTER_WIDTH;
        private int _TotalFrames = TimelineConst.DEFAULT_TOTAL_FRAMES;
        private int _NodeBorderDetectionWidth = TimelineConst.NODE_BORDER_DETECTION_WIDTH;
        private int _TitleHeight = TimelineConst.TITLE_HEIGHT;
        private int _LeftPanelMinWidth = TimelineConst.LEFT_PANEL_MIN_WIDTH;

        private float _SliderRatio;
        private int _ShowTotalFrames;
        private int _Width;
        private int _Height;
        private int _CurrentSelectedLayerIndex = -1;
        private int _LastSelectedLayerIndex = -1;
        private int _FrameRate = TimelineConst.DEFAULT_FRAME_RATE;
        private bool _IsNodeDraggingLeftBorder = false;
        private int _OnClickLayerIndex;
        private int _OnMouseLayerIndex;
        private float _OffsetTimeDist = 0;
        private int _ScrollTotalHandleSize = 0;
        private bool _IsPlaying = false;
        private double _LastRecordTime = 0;
        private double _PassTimeRecord = 0;

        private TimelineSerializeInfo _TimelineSerializeInfo = new TimelineSerializeInfo();
//        [CustomSerializeField]
//        private List<ITimelineLayer> _Layers = new List<ITimelineLayer>();
//        [CustomSerializeField]
//        private Dictionary<ITimelineLayer, Object> _OldBindingObjects = new Dictionary<ITimelineLayer, Object>();

        private IBindingController _BindingController;

//        private Dictionary<Object, int> _BindingHolder = new Dictionary<Object, int>();
//        private Dictionary<int, GameObjectInfo> _BindingInfos = new Dictionary<int, GameObjectInfo>();

        private List<IDragProcesser> _DragProcessers = new List<IDragProcesser>();
        private IDragProcesser _CurrentDragProcesser;
        private int _CurrentFrameIndex;
        private Rect _BoundaryRect;
        private Rect _TimeRulerRect;
        private Rect _TimelineAreaRect;
        private Rect _HorizontalScrollRect;
        private Rect _VerticalScrollRect;
        private Rect _ScrollLeftZoomRect;
        private Rect _ScrollRightZoomRect;
        private Rect _LayerAreaRect;
        private Rect _CurrentFrameTextFieldRect;
        private ITimelineLayer _CurrentSelectedLayer = null;
        private ITimelineLayer _LastSelectedLayer = null;
        private Node _CurrentSelectNode = null;
        private Node _LastSelectNode = null;

        private bool _ExpandCursor;
        private bool _LastExpandCursor;
        private int _VerticalHandleSize;
        private bool _IsDragging;
        private List<ITimelineLayer> _SelectedLayers = new List<ITimelineLayer>();
        private List<int> _SelectedLayerIndexs = new List<int>();
        private List<Node> _SelectedNodes = new List<Node>();
        private List<Node> _NodesList = new List<Node>();
        private Vector2 _CreateNodePoint = Vector2.zero;
        private List<TimelinePopupMenuItem> _MenuItems = new List<TimelinePopupMenuItem>();
        private List<TimelinePopupMenuItem> _ExtendItems = new List<TimelinePopupMenuItem>();
        private List<TimelinePopupMenuItem> _LayerItems = new List<TimelinePopupMenuItem>();
        private List<TimelinePopupMenuItem> _NodeItems = new List<TimelinePopupMenuItem>();
        private List<TimelinePopupMenuItem> _InsertItems = new List<TimelinePopupMenuItem>();

//        [CustomSerializeField]
//        public Dictionary<ITimelineLayer, Type> _LayersTypeDict = new Dictionary<ITimelineLayer, Type>();

        private Shortcuts _Shortcuts = new Shortcuts();
        private int _ControlID = 0;
        private ITimelineLayer _CurrentSelectedObjLayer;

        public GUITimeline()
        {
        }

        public GUITimeline(int width, int height)
        {
            Init(width, height);
        }

        void OnEnable()
        {
           
        }

        public void Init(int width, int height)
        {
            if (TimelineSerializeConst.LastDeSerializeGuiTimeline != this)
            {
                TimelineSerializeConst.LastDeSerializeGuiTimeline = this;
                _Deserialize();
                _BindingController = new DefaultBindingController();
                (_BindingController as DefaultBindingController).Init(_TimelineSerializeInfo._BindingObjects);
            }
            else
            {
                _TimelineSerializeInfo = TimelineSerializeConst.LastTimelineSerializeInfo;
                _BindingController = TimelineSerializeConst.LastTimelineBindingController;
            }

            _InitDataStruct();
            SetSize(width, height);

            _DragProcessers.Add(new DragProcesser(IsCanDragBoundary, OnDragBoundary));
            _DragProcessers.Add(new DragProcesser(IsCanDragTimeRulerArea, OnDragTimeRulerArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragScrollLeftZoomArea, OnDragScrollLeftZoomArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragScrollRightZoomArea, OnDragScrollRightZoomArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragScrollArea, OnDragScrollArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragVerticalScrollArea, OnDragVerticalScrollArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragNodeBorderArea, OnDragNodeBorderArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragNodeArea, OnDragNodeArea));
            _DragProcessers.Add(new DragProcesser(IsCanDragTimelineArea, OnDragTimelineArea));

            _InitLayerItems();
            _InitNodeItems();
        }

        private void _Deserialize()
        {
            _TimelineSerializeInfo = TimelineSerialize.Deserialize(_TimelineSerializeInfo, TimelineSerializeConst.SERIALIZE_TYPE_JSON, Path) as TimelineSerializeInfo;
            TimelineSerializeConst.LastTimelineSerializeInfo = _TimelineSerializeInfo;
        }

        private void OnDragBoundary(Vector2 delta, Vector2 mousePosition)
        {
            _LineBoundaryOffset = mousePosition.x > _LeftPanelMinWidth ? mousePosition.x : _LeftPanelMinWidth;
            //Debug.LogError("delta.x = " + delta.x + " _LineBoundaryOffset = " + _LineBoundaryOffset);
        }

        private bool IsCanDragBoundary(Vector2 mousePosition)
        {
            bool result = Event.current.button == 0 && _BoundaryRect.Contains(mousePosition);
            //Debug.LogError("result = " + result);
            return result;
        }

        private void _InitDataStruct()
        {
            _LayerHeight = TimelineConst.LAYER_HEIGHT;
            _RulerScale = 1;
            _MinRulerScale = TimelineConst.MIN_RULER_SCALE;
            _BaseFramePadding = TimelineConst.BASE_FRAME_PADDING;
            _LineBoundaryOffset = 0;
            _RulerOffset = 0;
            _VerticalOffset = 0;
            _HorizontalOffset = 0;
            _MouseWheelPrecision = TimelineConst.MOUSE_WHEEL_PRECISION;
            _MouseDragPrecision = TimelineConst.MOUSE_DRAG_PRECISION;
            _TimeRulerHeight = TimelineConst.TIME_RULER_HEIGHT;
            _ScrollRectHeight = TimelineConst.SCROLL_RECT_HEIGHT;
            _VerticalScrollRectWidth = TimelineConst.SCROLL_RECT_HEIGHT;
            _ScrollZoomHandleSize = TimelineConst.SCROLL_ZOOM_HANDLE_SIZE;
            _BaseFrameWidth = TimelineConst.BASE_FRAME_WIDTH;
            _TimePointerWidth = TimelineConst.TIME_POINTER_WIDTH;
            _TotalFrames = TimelineConst.DEFAULT_TOTAL_FRAMES;
            _NodeBorderDetectionWidth = TimelineConst.NODE_BORDER_DETECTION_WIDTH;
            _TitleHeight = TimelineConst.TITLE_HEIGHT;
            _CurrentSelectedLayerIndex = -1;
            _LastSelectedLayerIndex = -1;
            _FrameRate = TimelineConst.DEFAULT_FRAME_RATE;
            _OffsetTimeDist = 0;
            _CurrentSelectedLayer = null;
            _LastSelectedLayer = null;
            _CurrentSelectNode = null;
            _LastSelectNode = null;
            _CreateNodePoint = Vector2.zero;
            _LastRecordTime = 0;
            _PassTimeRecord = 0;

            if (_NodesList == null)
            {
                _NodesList = new List<Node>();
            }
            if (_TimelineSerializeInfo._Layers == null)
            {
                _TimelineSerializeInfo._Layers = new List<ITimelineLayer>();
            }
            else
            {
                _NodesList.Clear();
                foreach (var layer in _TimelineSerializeInfo._Layers)
                {
                    var nodeList = layer.GetAllNodes();
                    foreach (var node in nodeList)
                    {
                        _NodesList.Add(node);
                    }
                    _NodesList.Sort(_SortFunc);
                }
            }
            if (_DragProcessers == null)
            {
                _DragProcessers = new List<IDragProcesser>();
            }
            if (_SelectedLayers == null)
            {
                _SelectedLayers = new List<ITimelineLayer>();
            }
            if (_SelectedLayerIndexs == null)
            {
                _SelectedLayerIndexs = new List<int>();
            }
            if (_SelectedNodes == null)
            {
                _SelectedNodes = new List<Node>();
            }
            if (_MenuItems == null)
            {
                _MenuItems = new List<TimelinePopupMenuItem>();
            }
            if (_ExtendItems == null)
            {
                _ExtendItems = new List<TimelinePopupMenuItem>();
            }
            if (_LayerItems == null)
            {
                _LayerItems = new List<TimelinePopupMenuItem>();
            }
            if (_NodeItems == null)
            {
                _NodeItems = new List<TimelinePopupMenuItem>();
            }
            if (_InsertItems == null)
            {
                _InsertItems = new List<TimelinePopupMenuItem>();
            }
            if (_TimelineSerializeInfo._LayersTypeDict == null)
            {
                _TimelineSerializeInfo._LayersTypeDict = new Dictionary<ITimelineLayer, Type>();
            }
            if (_Shortcuts == null)
            {
                _Shortcuts = new Shortcuts();
            }
            _ExtendItems.Clear();
            _InsertItems.Clear();
        }

        private void _InitNodeItems()
        {
            Func<bool> isCanActiveNodeShortcut = () => (!(CalculateLayerAreaIndex() < _TimelineSerializeInfo._Layers.Count && _CurrentSelectNode == null)) && _CurrentSelectNode != null;
            _NodeItems.Clear();
            _AddMenuItem(_NodeItems, new TimelinePopupMenuItem("Delete", _DeleteNodes, () => true, "_DEL"), isCanActiveNodeShortcut);
            _AddMenuItem(_NodeItems, new TimelinePopupMenuItem("Copy", _CopyNodes, () => true, "%c"), isCanActiveNodeShortcut);
            _AddMenuItem(_NodeItems, new TimelinePopupMenuItem("Paste", _Paste, () => _IsCanPasteLayers() || _IsCanPasteNodes(), "%v"), isCanActiveNodeShortcut);
            _AddMenuItem(_NodeItems, new TimelinePopupMenuItem("Duplicate", _DuplicateNodes, () => true, "%d"), isCanActiveNodeShortcut);
        }

        private void _InitLayerItems()
        {
            Func<bool> isCanActiveLayerShortcut = () =>
            {
                return (_OnClickLayerIndex < _TimelineSerializeInfo._Layers.Count && _CurrentSelectNode == null);
            };
            _LayerItems.Clear();
            _AddMenuItem(_LayerItems, new TimelinePopupMenuItem("Delete", _DeleteLayers, () => true, "_DEL"), isCanActiveLayerShortcut);
            _AddMenuItem(_LayerItems, new TimelinePopupMenuItem("Copy", _CopyLayers, () => true, "%c"), isCanActiveLayerShortcut);
            _AddMenuItem(_LayerItems, new TimelinePopupMenuItem("Paste", _Paste, () => _IsCanPasteLayers() || _IsCanPasteNodes(), "%v"), isCanActiveLayerShortcut);
            _AddMenuItem(_LayerItems, new TimelinePopupMenuItem("Duplicate", _DuplicateLayers, () => true, "%d"), isCanActiveLayerShortcut);
            _AddMenuItem(_LayerItems, new TimelinePopupMenuItem(), isCanActiveLayerShortcut);
            _AddMenuItem(_LayerItems, new TimelinePopupMenuItem("添加节点", _AddNode, _IsCanAddNodes), isCanActiveLayerShortcut);
        }

        private void _AddMenuItem(List<TimelinePopupMenuItem> items, TimelinePopupMenuItem timelinePopupMenuItem, Func<bool> checker)
        {
            if (!string.IsNullOrEmpty(timelinePopupMenuItem.Shortcut))
            {
                _AddShortcut(timelinePopupMenuItem.Shortcut, () =>
                {
                    if (!timelinePopupMenuItem.Enable() || !checker())
                    {
                        return;
                    }

                    timelinePopupMenuItem.Action(_CreateContext());
                });
            }

            items.Add(timelinePopupMenuItem); 
        }

        public void AddExtendItem(TimelinePopupMenuItem item)
        {
            _AddMenuItem(_ExtendItems, item, () => true);
        }

        public void AddExtendItems(IEnumerable<TimelinePopupMenuItem> items)
        {
            foreach (var item in items)
            {
                AddExtendItem(item);
            }
        }

        public void RegisterNodeType(string menu, Type scriptableObjectType)
        {
            _AddMenuItem(_InsertItems, new TimelinePopupMenuItem("Insert/" + menu, (ctx) =>
            {
                AddLayer(scriptableObjectType, ctx.CurrentSelectLayerIndex);
            }, () => true), ()=>true);
        }

        private void OnDragNodeBorderArea(Vector2 delta, Vector2 mousePosition)
        {
            if (_CurrentSelectNode != null)
            {
                SelectNodes();

                var start = _CurrentSelectNode.GetNodeRenderer().GetStart();
                var end = _CurrentSelectNode.GetNodeRenderer().GetEnd();

                float offsetTime = GetDragNodeOffsetDistance(delta);
                float oneFrameTime = TimelineUtility.FramesConvertToTime(1, _FrameRate);
              
                if (delta.x != 0)
                {
                    _CurrentSelectNode.GetNodeRenderer().IsRepaintInspector = true;
                }
                float limitOffsetTime = 0;  //限制拖动到向左或向右的前一帧
                if (_IsNodeDraggingLeftBorder)
                {
                    limitOffsetTime = Mathf.Min(start + offsetTime,end - oneFrameTime); 
                    _CurrentSelectNode.GetNodeRenderer().SetStartAndEnd(limitOffsetTime, end);
                }
                else
                {
                    limitOffsetTime = Mathf.Max(end + offsetTime, start + oneFrameTime);
                    _CurrentSelectNode.GetNodeRenderer().SetStartAndEnd(start, limitOffsetTime);
                }

                float x = mousePosition.x - _LineBoundaryOffset;
                if (x < 0 || x > _TimelineAreaRect.width)
                {
                    OffsetRuler(delta.x);
                }
            }
        }

        private bool IsCanDragNodeBorderArea(Vector2 mousePosition)
        {
            //计算出node的位置
            mousePosition -= new Vector2(_LayerAreaRect.x, _LayerAreaRect.y);
            int layerClickIndex = ResetLayerClickAreaIndex();
            _CurrentSelectNode = null;
            if (layerClickIndex < _TimelineSerializeInfo._Layers.Count)
            {
                List<Node> nodeList = _TimelineSerializeInfo._Layers[layerClickIndex].GetAllNodes();
                mousePosition -= new Vector2(0, _LayerHeight * layerClickIndex);
                for (int i = nodeList.Count - 1; i >= 0; i--)  //从后往前
                {
                    Node node = nodeList[i];
                    int startFrame = TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetStart(), _FrameRate);
                    int endFrame = TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetEnd(), _FrameRate);
                    int startPosX = FrameConvertToDistance(startFrame, _RulerOffset);
                    int endPosX = FrameConvertToDistance(endFrame, _RulerOffset);

                    if (mousePosition.x >= startPosX && mousePosition.x <= startPosX + _NodeBorderDetectionWidth)
                    {
                        _IsNodeDraggingLeftBorder = true;
                        _CurrentSelectNode = node;
                        break;
                    }
                    else if (mousePosition.x >= endPosX - _NodeBorderDetectionWidth && mousePosition.x <= endPosX)
                    {
                        _IsNodeDraggingLeftBorder = false;
                        _CurrentSelectNode = node;
                        break;
                    }
                }
            }

            if (_CurrentSelectNode == null)
            {
                //置空Target.select
                if (Selection.activeObject != null)
                {
                    Selection.activeObject = null;
                }
            }

            return _CurrentSelectNode != null;
        }

        private void OnDragNodeArea(Vector2 delta, Vector2 mousePosition)
        {
            if (_CurrentSelectNode != null)
            {
                SelectNodes();

                float offsetTime = GetDragNodeOffsetDistance(delta);

                //暂时要遍历限制左移
                bool isCanMove = true;
                foreach (var nodes in _SelectedNodes)
                {
                    var start = nodes.GetNodeRenderer().GetStart();
                    var end = nodes.GetNodeRenderer().GetEnd();
                    if (start + offsetTime <= 0)
                    {
                        isCanMove = false;
                        break;
                    }
                }

                foreach (var nodes in _SelectedNodes)
                {
                    var start = nodes.GetNodeRenderer().GetStart();
                    var end = nodes.GetNodeRenderer().GetEnd();
                    var duration = end - start;

                    if (isCanMove)
                    {
                        if (offsetTime != 0)
                        {
                            nodes.GetNodeRenderer().IsRepaintInspector = true;
                        }
                        nodes.GetNodeRenderer().SetStartAndEnd(start + offsetTime, end + offsetTime, duration);
                    }
                    else
                    {
                        offsetTime = 0;
                    }
                }

                float x = mousePosition.x - _LineBoundaryOffset;
                if (x < 0 || x > _TimelineAreaRect.width)
                {
                    OffsetRuler(delta.x);
                }

                if (mousePosition.y < _TimeRulerHeight || mousePosition.y > _TimelineAreaRect.height - _TitleHeight - _ScrollRectHeight)
                {
                    if (IsShowVerticalScroll())
                    {
                        _VerticalOffset = mousePosition.y;
                        _VerticalOffset = Mathf.Max(0, _VerticalOffset);
                    }
                }

                //drag abstractNode change layer
                int clickLayerIndex = ResetLayerClickAreaIndex();
                if (clickLayerIndex < _TimelineSerializeInfo._Layers.Count && !_IsSelectMultiLayerNodes())
                {
                    ITimelineLayer layer = _GetLayerByIndex(clickLayerIndex);
                    for (int i = 0; i < _SelectedNodes.Count; i++)
                    {
                        Node node = _SelectedNodes[i];
                        if (_TimelineSerializeInfo._LayersTypeDict == null)
                        {
                            Debug.LogError("_LayersTypeDict == null");
                        }
                        //else
                        //{
                        //    Debug.LogError("_LayersTypeDict .count = " + _LayersTypeDict.Count);
                        //}
                        //同类型的层才可以放
                        int lastLayerIndex = node.GetNodeRenderer().LayerIndex;
                        if (lastLayerIndex != clickLayerIndex && _TimelineSerializeInfo._LayersTypeDict[layer] == node.GetNodeRenderer().DataType)
                        {
                            Debug.LogError("lastLayerIndex = " + lastLayerIndex + " clickLayerIndex = " + clickLayerIndex);
                            node.GetNodeRenderer().LayerIndex = clickLayerIndex;
                            _TimelineSerializeInfo._Layers[lastLayerIndex].RemoveNode(node);
                            _TimelineSerializeInfo._Layers[clickLayerIndex].AddNode(node);
                            _TimelineSerializeInfo._Layers[clickLayerIndex].GetAllNodes().Sort(SortLayerNodes);
                        }
                    }
                }
                if (_LastSelectNode == null || _LastSelectNode != _CurrentSelectNode)
                {
                    _LastSelectNode = _CurrentSelectNode;
                }

                Selection.activeObject = _CurrentSelectNode;
                //Selection.objects 
            }
        }

        private void SelectNodes()
        {
            if (_CurrentSelectNode != null)
            {
                if (Event.current.control)
                {
                    if (_SelectedNodes.Contains(_CurrentSelectNode))
                    {
                        _CurrentSelectNode.GetNodeRenderer().OnSelect = false;
                        _SelectedNodes.Remove(_CurrentSelectNode);
                    }
                    else
                    {
                        _SelectedNodes.Add(_CurrentSelectNode);
                    }
                }
                else if (Event.current.shift)  //select all nodes between two layer
                {
                    if (_LastSelectNode != null)
                    {
                        int lastLayerIndex = _LastSelectNode.GetNodeRenderer().LayerIndex;
                        int currentLayerIndex = _CurrentSelectNode.GetNodeRenderer().LayerIndex;
                        if (lastLayerIndex > currentLayerIndex)
                        {
                            int temp = lastLayerIndex;
                            lastLayerIndex = currentLayerIndex;
                            currentLayerIndex = temp;
                        }
                        for (int i = lastLayerIndex; i <= currentLayerIndex; i++)
                        {
                            List<Node> nodeList = _TimelineSerializeInfo._Layers[i].GetAllNodes();
                            for (int j = 0; j < nodeList.Count; j++)
                            {
                                Node node = nodeList[j];
                                if (!_SelectedNodes.Contains(node))
                                {
                                    _SelectedNodes.Add(node);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!_SelectedNodes.Contains(_CurrentSelectNode))
                    {
                        foreach (var nodes in _SelectedNodes)
                        {
                            nodes.GetNodeRenderer().OnSelect = false;
                        }
                        _SelectedNodes.Clear();
                        _SelectedNodes.Add(_CurrentSelectNode);
                    }
                }
                //show selected abstractNode
                foreach (var nodes in _SelectedNodes)
                {
                    nodes.GetNodeRenderer().OnSelect = true;
                }
            }
        }

        private bool IsCanDragNodeArea(Vector2 mousePosition)
        {
            //计算出node的位置
            mousePosition -= new Vector2(_LayerAreaRect.x, _LayerAreaRect.y);
            int layerClickIndex = ResetLayerClickAreaIndex();
            _CurrentSelectNode = null;
            if (layerClickIndex < _TimelineSerializeInfo._Layers.Count)
            {
                List<Node> nodeList = _TimelineSerializeInfo._Layers[layerClickIndex].GetAllNodes();
                mousePosition -= new Vector2(0, _LayerHeight * layerClickIndex);
                for (int i = nodeList.Count - 1; i >= 0; i--)
                {
                    Node node = nodeList[i];
                    int startFrame = TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetStart(), _FrameRate);
                    int endFrame = TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetEnd(), _FrameRate);
                    int startPosX = FrameConvertToDistance(startFrame, _RulerOffset);
                    int endPosX = FrameConvertToDistance(endFrame, _RulerOffset);
                    if (mousePosition.x >= startPosX && mousePosition.x <= endPosX)
                    {
                        _CurrentSelectNode = node;
                        break;
                    }
                }
            }
            if (_CurrentSelectNode == null)
            {
                foreach (var nodes in _SelectedNodes)
                {
                    nodes.GetNodeRenderer().OnSelect = false;
                }
                _SelectedNodes.Clear();
                //置空Target.select
                if (Selection.activeObject != null)
                {
                    Selection.activeObject = null;
                }
            }

            return _CurrentSelectNode != null;
        }

        private int ResetLayerClickAreaIndex()
        {
            var mousePosition = Event.current.mousePosition;
            mousePosition -= new Vector2(_LayerAreaRect.x, _LayerAreaRect.y);
            int onClickLayer = Mathf.FloorToInt(mousePosition.y / _LayerHeight);
            _OnClickLayerIndex = Mathf.Max(0, onClickLayer);
            //Debug.Log("ResetLayerClickAreaIndex = " + _OnClickLayerIndex + " - " + mousePosition);
            return Mathf.Max(0, onClickLayer);
        }

        private int CalculateLayerAreaIndex()
        {
            var mousePosition = Event.current.mousePosition;
            mousePosition -= new Vector2(_LayerAreaRect.x, _LayerAreaRect.y);
            int currentMouseLayer = Mathf.FloorToInt(mousePosition.y / _LayerHeight);
            _CreateNodePoint = mousePosition;
            _OnMouseLayerIndex = Mathf.Max(0, currentMouseLayer);
            return Mathf.Max(0, currentMouseLayer);
        }


        private void OnDragScrollRightZoomArea(Vector2 delta, Vector2 mousePosition)
        {
            _RulerScale -= (delta.x / _HorizontalScrollRect.width);
            _RulerScale = Mathf.Max(_RulerScale, _MinRulerScale);
        }

        private bool IsCanDragScrollRightZoomArea(Vector2 mousePosition)
        {
            mousePosition -= new Vector2(_HorizontalScrollRect.x, _HorizontalScrollRect.y);
            return Event.current.button == 0 && _ScrollRightZoomRect.Contains(mousePosition);
        }

        private int MiddleHandleSize()
        {
            float currentFramePadding = _BaseFramePadding * _RulerScale;
            int showTotalFrames = Mathf.FloorToInt(2 + _TimeRulerRect.width / currentFramePadding);
            int newMiddleHandleSize = Mathf.FloorToInt(1f * showTotalFrames / _TotalFrames * _HorizontalScrollRect.width);
            return newMiddleHandleSize;
        }

        private void OnDragScrollLeftZoomArea(Vector2 delta, Vector2 mousePosition)
        {
            int oldSize = MiddleHandleSize();
            _RulerScale += (delta.x / _HorizontalScrollRect.width);
            _RulerScale = Mathf.Max(_RulerScale, _MinRulerScale);
            int newSize = MiddleHandleSize();
            OffsetRuler(oldSize - newSize);
        }

        private bool IsCanDragScrollLeftZoomArea(Vector2 mousePosition)
        {
            mousePosition -= new Vector2(_HorizontalScrollRect.x, _HorizontalScrollRect.y);
            return Event.current.button == 0 && _ScrollLeftZoomRect.Contains(mousePosition);
        }

        private void OnDragScrollArea(Vector2 delta, Vector2 mousePosition)
        {
            mousePosition -= new Vector2(_HorizontalScrollRect.x, _HorizontalScrollRect.y);
            _RulerOffset = mousePosition.x / (1f * _ShowTotalFrames / _TotalFrames);
            _RulerOffset = Mathf.Max(0, _RulerOffset);

            _HorizontalOffset = mousePosition.x;
            _HorizontalOffset = Mathf.Max(0, _HorizontalOffset);

            //OffsetRuler(delta.x);
        }

        private bool IsCanDragScrollArea(Vector2 mousePosition)
        {
            return Event.current.button == 0 && _HorizontalScrollRect.Contains(mousePosition);
        }

        private void OnDragVerticalScrollArea(Vector2 delta, Vector2 mousePosition)
        {
            mousePosition -= new Vector2(_VerticalScrollRect.x, _VerticalScrollRect.y);
            _VerticalOffset = mousePosition.y;
            _VerticalOffset = Mathf.Max(0, _VerticalOffset);
        }

        private bool IsCanDragVerticalScrollArea(Vector2 mousePosition)
        {
            return IsShowVerticalScroll() && Event.current.button == 0 && _VerticalScrollRect.Contains(mousePosition);
        }

        private void OnDragTimelineArea(Vector2 delta, Vector2 mousePosition)
        {
            OffsetRuler(-delta.x);
        }

        private bool IsCanDragTimelineArea(Vector2 mousePosition)
        {
            return Event.current.button == 2 && _TimelineAreaRect.Contains(mousePosition);
        }

        private float GetDragNodeOffsetDistance(Vector2 delta)
        {
            float offsetTime = 0;
            float oneFrameDist = TimelineUtility.FramesConvertToDistance(1, _BaseFramePadding, _RulerScale);
            float oneFrameTime = TimelineUtility.FramesConvertToTime(1, _FrameRate);
            _OffsetTimeDist += delta.x;
            if (Mathf.Abs(_OffsetTimeDist) >= oneFrameDist)
            {
                //限制offsetTime是帧时间的倍数
                _OffsetTimeDist = Mathf.FloorToInt(_OffsetTimeDist / oneFrameDist) * oneFrameDist;
                offsetTime = TimelineUtility.DistanceConvertToTime(_OffsetTimeDist, _BaseFramePadding, _RulerScale, _FrameRate);
                _OffsetTimeDist = 0;
            }
            return offsetTime;
        }

        /// <summary>
        /// offset
        ///     positive : left
        ///     negative : right
        /// </summary>
        /// <param name="offset"></param>
        private void OffsetRuler(float offset)
        {
            _RulerOffset += offset * _MouseDragPrecision;
            _RulerOffset = Mathf.Max(0, _RulerOffset);
        }

        private void OffsetVertical(float offset)
        {
            _VerticalOffset += offset * _MouseDragPrecision;
            _VerticalOffset = Mathf.Max(0, _VerticalOffset);
        }

        private void OnDragTimeRulerArea(Vector2 delta, Vector2 mousePosition)
        {
            float intRulerOffset = _RulerOffset;
            mousePosition -= new Vector2(_TimeRulerRect.x, _TimeRulerRect.y);
            float currentFramePadding = _BaseFramePadding * _RulerScale;
            int currentFrameIndex = Mathf.FloorToInt((mousePosition.x + currentFramePadding / 2 + intRulerOffset) / currentFramePadding);
            SetCurrentFrameIndex(currentFrameIndex);
        }

        public bool IsCanDragTimeRulerArea(Vector2 mousePosition)
        {
            return Event.current.button == 0 && _TimeRulerRect.Contains(mousePosition);
        }

        private void AddSelectedLayer(int selectLayerIndex, bool toggleMode = false)
        {
            if (!_SelectedLayerIndexs.Contains(selectLayerIndex))
            {
                _CurrentSelectedLayerIndex = selectLayerIndex;
                _CurrentSelectedLayer = _TimelineSerializeInfo._Layers[selectLayerIndex];
                _SelectedLayers.Add(_CurrentSelectedLayer);
                _SelectedLayerIndexs.Add(selectLayerIndex);
            }
            else
            {
                if (toggleMode)
                {
                    _SelectedLayers.Remove(_TimelineSerializeInfo._Layers[selectLayerIndex]);
                    _SelectedLayerIndexs.Remove(selectLayerIndex);
                    if (selectLayerIndex == _CurrentSelectedLayerIndex)
                    {
                        _CurrentSelectedLayerIndex = _SelectedLayerIndexs.Count > 0 ? _SelectedLayerIndexs[_SelectedLayerIndexs.Count - 1] : -1;
                        _CurrentSelectedLayer = _CurrentSelectedLayerIndex != -1 ? _TimelineSerializeInfo._Layers[_CurrentSelectedLayerIndex] : null;
                    }
                }
            }
        }

        private void _ProcessSelectLayers()
        {
            var clickLayerIndex = ResetLayerClickAreaIndex();
            bool clickInLayerArea = clickLayerIndex >= 0 && clickLayerIndex < _TimelineSerializeInfo._Layers.Count;

            if (clickInLayerArea)
            {
                if (Event.current.control)
                {
                    AddSelectedLayer(clickLayerIndex, true);
                }
                else if (Event.current.shift)
                {
                    if (_LastSelectedLayer != null)
                    {
                        int lastSelectedLayerIndex = _LastSelectedLayerIndex;
                        int currentSelectedLayerIndex = clickLayerIndex;
                        if (lastSelectedLayerIndex > currentSelectedLayerIndex)
                        {
                            int temp = lastSelectedLayerIndex;
                            lastSelectedLayerIndex = currentSelectedLayerIndex;
                            currentSelectedLayerIndex = temp;
                        }

                        for (int i = lastSelectedLayerIndex; i <= currentSelectedLayerIndex; i++)
                        {
                            AddSelectedLayer(i);
                        }
                    }
                }
                else
                {
                    bool clickOnLayer = Event.current.button == 1 && !_SelectedLayers.Contains(_CurrentSelectedLayer);
                    clickOnLayer |= Event.current.button == 0;
                    if (clickOnLayer)
                    {
                        ClearSelectedLayers();
                        AddSelectedLayer(clickLayerIndex);
                        _LastSelectedLayer = _CurrentSelectedLayer;
                        _LastSelectedLayerIndex = _CurrentSelectedLayerIndex;
                    }
                }
            }
            else
            {
                ClearSelectedLayers();
                _LastSelectedLayer = null;
            }
        }

        private void ClearSelectedLayers()
        {
            _CurrentSelectedLayerIndex = -1;
            _CurrentSelectedLayer = null;
            _SelectedLayers.Clear();
            _SelectedLayerIndexs.Clear();
        }

        private bool _ProcessEvent()
        {
            bool processedEvent = false;
            var evt = Event.current;

//            if (evt.commandName == "ObjectSelectorUpdated")
//            {
//
//                return true;
//            }

            if (evt.isScrollWheel)
            {
                _RulerScale -= evt.delta.y * _MouseWheelPrecision;
                _RulerScale = Mathf.Max(_RulerScale, _MinRulerScale);
                processedEvent |= true;
            }

            if (Event.current.isKey)
            {
                if (Event.current.keyCode == KeyCode.Q)
                {
                    _RulerScale = 1;

                    processedEvent |= true;
                }
            }

            if (Event.current.isMouse)
            {
                switch (Event.current.rawType)
                {
                    case EventType.MouseDown:
                        _IsDragging = false;
                        foreach (var dragProcessers in _DragProcessers)
                        {
                            if (dragProcessers.IsInDragArea())
                            {
                                dragProcessers.Process();
                                _CurrentDragProcesser = dragProcessers;
                                processedEvent |= true;
                                _IsDragging = true;
                                break;
                            }
                        }

                        if (!_IsDragging)
                        {
                            _ProcessSelectLayers();
                        }
                        else
                        {
                            ClearSelectedLayers();
                        }

                        if (!_CurrentFrameTextFieldRect.Contains(Event.current.mousePosition))
                        {
                            GUI.FocusControl(null);
                        }

                        break;

                    case EventType.MouseDrag:
                        if (_CurrentDragProcesser != null)
                        {
                            _CurrentDragProcesser.Process();
                            processedEvent |= true;
                        }

                        break;

                    case EventType.MouseUp:
                        _CurrentDragProcesser = null;
                        if (_IsDragging)
                        {
                            for (int i = 0; i < _SelectedNodes.Count; i++)
                            {
                                int layerIndex = _SelectedNodes[i].GetNodeRenderer().LayerIndex;
                                _TimelineSerializeInfo._Layers[layerIndex].GetAllNodes().Sort(SortLayerNodes);
                            }
                            _OffsetTimeDist = 0;
                            _IsDragging = false;

                            _Serialize();
                        }

                        processedEvent |= true;

                        if (Event.current.button == 1)
                        {
                            _PopMenuItem();
                        }

                        break;
                }
            }

            return processedEvent;
        }

        private int SortLayerNodes(Node a, Node b)
        {
            var aStart = a.GetNodeRenderer().GetStart();
            var bStart = b.GetNodeRenderer().GetStart();
            if (Mathf.Approximately(aStart, bStart))
            {
                return 0;
            }
            else if (aStart < bStart)
            {
                return -1;
            }
            return 1;
        }

        /// <summary>
        /// 右键弹出创建层菜单
        /// </summary>
        private void _PopMenuItem()
        {
            GenericMenu menu = new GenericMenu();
            _CreateNodePoint = Event.current.mousePosition;
            _MenuItems.Clear();
            foreach (var menuItem in _InsertItems)
            {
                _MenuItems.Add(menuItem);
            }
            if (ResetLayerClickAreaIndex() < _TimelineSerializeInfo._Layers.Count && _CurrentSelectNode == null)
            {
                foreach (var layerItem in _LayerItems)
                {
                    _MenuItems.Add(layerItem);
                }
            }
            else if (_CurrentSelectNode != null)
            {
                foreach (var nodeItem in _NodeItems)
                {
                    _MenuItems.Add(nodeItem);
                }
                //_MenuItems.Add(new TimelinePopupMenuItem("Delete", _DeleteNodes, () => true));
                //_MenuItems.Add(new TimelinePopupMenuItem("Copy                  Ctrl + C", _CopyNodes, () => true));
                //_MenuItems.Add(new TimelinePopupMenuItem("Paste                 Ctrl + V", _Paste, () => _IsCanPasteLayers() || _IsCanPasteNodes()));
                //_MenuItems.Add(new TimelinePopupMenuItem("Duplicate             Ctrl + D", _DuplicateNodes, () => true));
            }
            else //点击空白区域
            {
                //todo 根据类型添加层菜单，for
                //_MenuItems.Add(new TimelinePopupMenuItem("添加层", AddLayer, () => true));
            }
            _MenuItems.Add(new TimelinePopupMenuItem());

            foreach (var menuItem in _ExtendItems)
            {
                _MenuItems.Add(menuItem);
            }

            foreach (var menuItem in _MenuItems)
            {
                if (menuItem.Separator)
                {
                    menu.AddSeparator("");
                }
                else
                {
                    string label = menuItem.Menu;
                    if (!string.IsNullOrEmpty(menuItem.Shortcut))
                    {
                        label += " " + menuItem.Shortcut;
                    }

                    if (menuItem.Enable())
                    {
                        menu.AddItem(new GUIContent(label), false, () => menuItem.Action(_CreateContext()));
                    }
                    else
                    {
                        menu.AddDisabledItem(new GUIContent(label));
                    }
                }
            }
            menu.ShowAsContext();
        }

        private TimelinePopupMenuItem.Context _CreateContext()
        {
            return new TimelinePopupMenuItem.Context(_CurrentSelectedLayerIndex, _SelectedLayerIndexs.ToArray());
        }

        private bool _IsCanAddNodes()
        {
            bool isCanAdd = false;
            for (int i = 0; i < _SelectedLayers.Count; i++)
            {
                //todo 根据层类型判断，选中不同层不可以add Node
                isCanAdd = true;
                break;
            }
            return isCanAdd;
        }

        private void _AddNode(TimelinePopupMenuItem.Context ctx)
        {
            if (_CurrentSelectedLayer != null)
            {
                //计算最后一个节点的位置
                //float start = _CurrentSelectedLayer.GetLastEndPosition();
                //Debug.Log(" createNodePointX = " + _CreateNodePoint.x + "  offset = " + _RulerOffset + " result = " + (_CreateNodePoint.x + _RulerOffset));
                //todo 在鼠标点击位置生成节点，此处缩小后误差较大
                float start = TimelineUtility.DistanceConvertToTime(_CreateNodePoint.x + _RulerOffset, _BaseFramePadding, _RulerScale, _FrameRate);
                //限制为帧时间得倍数
                start = start - start % TimelineUtility.FramesConvertToTime(1, _FrameRate);
                float duration = TimelineUtility.FramesConvertToTime(5, _FrameRate);
                AbstractNode baseAbstractNodeRenderer = new BaseNode(start, (start + duration), _CurrentSelectedLayerIndex);
                Node node = ScriptableObject.CreateInstance<Node>();
                node.SetNodeRenderer(baseAbstractNodeRenderer);
                //层的类型控制添加节点
                var scriptableType = _TimelineSerializeInfo._LayersTypeDict[_CurrentSelectedLayer];
                node.GetNodeRenderer().DataType = scriptableType;
                ScriptableObject data = ScriptableObject.CreateInstance(scriptableType);
                data.name = scriptableType.Name;
                node.SetData(data);
                //添加事件
                INodeEvent nodeEvent = null;
                var attr = scriptableType.GetCustomAttribute<NodeEventType>();
                if (attr != null)
                {
                    nodeEvent = Activator.CreateInstance(attr.EventType) as INodeEvent;
                }
                node.SetNodeEvent(nodeEvent);

                //Undo.RegisterCreatedObjectUndo(node, "Node");
                _CurrentSelectedLayer.AddNode(node);
                _NodeListAdd(node);
                EditorUtility.SetDirty(this);

                _Serialize();
            }
        }

        private void _Serialize()
        {
            string path = Path;
            TimelineSerialize.Serialize(_TimelineSerializeInfo, TimelineSerializeConst.SERIALIZE_TYPE_JSON, path);
        }

        private void _NodeListAdd(Node node)
        {
            _NodesList.Add(node);
            _NodesList.Sort(_SortFunc);
        }

        private int _SortFunc(Node x, Node y)
        {
            if (Mathf.Approximately(x.GetNodeRenderer().GetEnd(), y.GetNodeRenderer().GetEnd()))
            {
                return 0;
            }
            else if (x.GetNodeRenderer().GetEnd() > y.GetNodeRenderer().GetEnd())
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        #region Basic Operation On Layer Or Node

        private void _DeleteLayers(TimelinePopupMenuItem.Context ctx)
        {
            if (_SelectedLayerIndexs.Count > 0)
            {
                //排序，从后往前删除
                _SelectedLayerIndexs.Sort((a, b) => { return b - a; });

                for (int i = 0; i < _SelectedLayerIndexs.Count; i++)
                {
                    int index = _SelectedLayerIndexs[i];
                    ITimelineLayer layer = _TimelineSerializeInfo._Layers[index];
                    //delete nodes
                    List<Node> nodes = layer.GetAllNodes();
                    for (int j = 0; j < nodes.Count; j++)
                    {
                        Node node = nodes[j];
                        if (_SelectedNodes.Contains(node))
                        {
                            _SelectedNodes.Remove(node);
                        }
                        if (_NodesList.Contains(node))
                        {
                            _NodesList.Remove(node);
                        }
                    }
                    RemoveLayer(index);
                }
                ClearSelectedLayers();

                _Serialize();
            }
        }

        private void _CopyLayers(TimelinePopupMenuItem.Context ctx)
        {
            if (_SelectedLayers.Count > 0)
            {
                CopyBoard.copyLayers.Clear();
                CopyBoard.copyNodes.Clear();
                CopyBoard.copyNodesLayersList.Clear();
                for (int i = 0; i < _SelectedLayers.Count; i++)
                {
                    ITimelineLayer layer = _SelectedLayers[i];
                    //deep copy
                    ITimelineLayer newLayer = Reflections.Clone(layer, new[] {typeof(Reflections.CloneField) });
                    CopyLayerInfo copyLayerInfo = new CopyLayerInfo();
                    copyLayerInfo.layer = newLayer;
                    copyLayerInfo.scriptableObjectType = _TimelineSerializeInfo._LayersTypeDict[layer];
                    CopyBoard.copyLayers.Add(copyLayerInfo);
                }
            }
        }

        private bool _IsCanPasteLayers()
        {
            //todo 判断是否有copy内容
            if (CopyBoard.copyLayers != null && CopyBoard.copyLayers.Count > 0)
            {
                return true;
            }
            return false;
        }

        private bool _IsCanPasteNodes()
        {
            //todo 不支持拷贝到不同类型的层
            //todo 需求：支持多层拷贝(只能在选中的层中),

            if (CopyBoard.copyNodes != null && CopyBoard.copyNodes.Count > 0)
            {
                //int clickLayerIndex = ResetLayerClickAreaIndex();
                int mouseLayerIndex = CalculateLayerAreaIndex();
                if (CopyBoard.copyNodesLayersList.Count == 1) //only one layer
                {
                    ITimelineLayer layer = _GetLayerByIndex(mouseLayerIndex);
                    if (layer == null)
                    {
                        return false;
                    }
                    Type dataType = CopyBoard.copyNodes[0].Node.GetNodeRenderer().DataType;
                    //层类型相同
                    if (_TimelineSerializeInfo._LayersTypeDict[layer] == dataType)
                    {
                        return true;
                    }
                }
                else //multi layers
                {
                    //是否选中当前拷贝所在的层
                    if (CopyBoard.copyNodesLayersList.Contains(mouseLayerIndex))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void _Paste(TimelinePopupMenuItem.Context ctx)
        {
            //move copy layers to last pos
            if (CopyBoard.copyLayers != null && CopyBoard.copyLayers.Count > 0)
            {
                _PasteLayers();
            }
            else if(CopyBoard.copyNodes != null && CopyBoard.copyNodes.Count > 0)
            {
                _PasteNodes();
            }
        }

        private void _PasteLayers()
        {
            _SelectedLayers.Clear();
            _SelectedLayerIndexs.Clear();

            List<CopyLayerInfo> copyLayers = Reflections.Clone(CopyBoard.copyLayers, new[] {typeof(Reflections.CloneField)});
            for (int i = 0; i < copyLayers.Count; i++)
            {
                CopyLayerInfo copyLayerInfo = copyLayers[i];
                ITimelineLayer layer = copyLayerInfo.layer;
                //fix Node index
                List<Node> nodeList = layer.GetAllNodes();
                for (int j = 0; j < nodeList.Count; j++)
                {
                    Node node = nodeList[j];
                    node.GetNodeRenderer().LayerIndex = _TimelineSerializeInfo._Layers.Count;
                    if (!_NodesList.Contains(node))
                    {
                        _NodeListAdd(node);
                    }
                }
                _AddLayer(layer, copyLayerInfo.scriptableObjectType, -1);
                _SelectedLayers.Add(layer);
            }
            for (int i = _TimelineSerializeInfo._Layers.Count - _SelectedLayers.Count; i < _TimelineSerializeInfo._Layers.Count; i++)
            {
                _SelectedLayerIndexs.Add(i);
            }

            _Serialize();
        }

        private void _PasteNodes()
        {
            for (int i = 0; i < _SelectedNodes.Count; i++)
            {
                _SelectedNodes[i].GetNodeRenderer().OnSelect = false;
            }
            _SelectedNodes.Clear();

            List<CopyNodeInfo> temp = Reflections.Clone(CopyBoard.copyNodes, new[] {typeof(Reflections.CloneField)});
            bool isCanPasteInOtherLayer = CopyBoard.copyNodesLayersList.Count == 1;
            //鼠标点下位置,todo 消除误差
            float start = TimelineUtility.DistanceConvertToTime(_CreateNodePoint.x + _RulerOffset, _BaseFramePadding, _RulerScale, _FrameRate);
            for (int i = 0; i < temp.Count; i++)
            {
                CopyNodeInfo info = temp[i];
                Node node = info.Node;
                node.GetNodeRenderer().OnSelect = true;
                //fix layer
                if (isCanPasteInOtherLayer)
                {
                    node.GetNodeRenderer().LayerIndex = _OnMouseLayerIndex;
                }
                if (info.IsFirstNode)
                {
                    node.GetNodeRenderer().SetStartAndEnd(start, start + info.Duration);
                }
                else
                {
                    node.GetNodeRenderer().SetStartAndEnd(start + info.DiffBetweenFirstNode, start + info.DiffBetweenFirstNode + info.Duration);
                }
                if (!_NodesList.Contains(node))
                {
                    _NodeListAdd(node);
                }
                _TimelineSerializeInfo._Layers[node.GetNodeRenderer().LayerIndex].AddNode(node);
                _SelectedNodes.Add(node);
            }

            for (int i = 0; i < CopyBoard.copyNodesLayersList.Count; i++)
            {
                int layerIndex = CopyBoard.copyNodesLayersList[i];
                _TimelineSerializeInfo._Layers[layerIndex].GetAllNodes().Sort(SortLayerNodes);
            }

            ClearSelectedLayers();
            _Serialize();
        }

        private void _DuplicateLayers(TimelinePopupMenuItem.Context ctx)
        {
            if (_SelectedLayers != null)
            {
                List<ITimelineLayer> tempLayerList = new List<ITimelineLayer>(_SelectedLayers);
                ClearSelectedLayers();
                for (int i = 0; i < tempLayerList.Count; i++)
                {
                    ITimelineLayer layer = tempLayerList[i];
                    ITimelineLayer newLayer = Reflections.Clone(layer, new []{typeof(Reflections.CloneField)});
                    List<Node> nodeList = newLayer.GetAllNodes();
                    for (int j = 0; j < nodeList.Count; j++)
                    {
                        Node node = nodeList[j];
                        node.GetNodeRenderer().LayerIndex = _TimelineSerializeInfo._Layers.Count;
                        if (!_NodesList.Contains(node))
                        {
                            _NodeListAdd(node);
                        }
                    }
                    int layerIndex = _AddLayer(newLayer, _TimelineSerializeInfo._LayersTypeDict[layer], -1);
                    AddSelectedLayer(layerIndex);
                }
                _Serialize();
            }
        }

        private void _DeleteNodes(TimelinePopupMenuItem.Context ctx)
        {
            if (_SelectedNodes != null)
            {
                for (int i = 0; i < _SelectedNodes.Count; i++)
                {
                    Node node = _SelectedNodes[i];
                    if (_NodesList.Contains(node))
                    {
                        _NodesList.Remove(node);
                    }
                    //delete layers Node
                    int layerIndex = node.GetNodeRenderer().LayerIndex;
                    if (_TimelineSerializeInfo._Layers[layerIndex].GetAllNodes().Contains(node))
                    {
                        _TimelineSerializeInfo._Layers[layerIndex].RemoveNode(node);
                    }
                }
                _SelectedNodes.Clear();
                _CurrentSelectNode = null;

                _Serialize();
            }
        }

        private void _CopyNodes(TimelinePopupMenuItem.Context ctx)
        {
            if (_SelectedNodes != null)
            {
                CopyBoard.copyLayers.Clear();
                CopyBoard.copyNodes.Clear();
                CopyBoard.copyNodesLayersList.Clear();
                Node firstNode = _SelectedNodes[0];
                for (int i = 1; i < _SelectedNodes.Count; i++)
                {
                    if (firstNode.GetNodeRenderer().GetStart() > _SelectedNodes[i].GetNodeRenderer().GetStart())
                    {
                        firstNode = _SelectedNodes[i];
                    }
                }

                for (int i = 0; i < _SelectedNodes.Count; i++)
                {
                    Node node = _SelectedNodes[i];
                    int layerIndex = node.GetNodeRenderer().LayerIndex;
                    Node newNode = Reflections.Clone(node, new[] {typeof(Reflections.CloneField)});
                    CopyNodeInfo nodeInfo = new CopyNodeInfo();
                    nodeInfo.Node = newNode;
                    nodeInfo.Duration = node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart();
                    if (firstNode == node)
                    {
                        nodeInfo.IsFirstNode = true;
                        nodeInfo.DiffBetweenFirstNode = 0;
                    }
                    else
                    {
                        nodeInfo.IsFirstNode = false;
                        nodeInfo.DiffBetweenFirstNode = node.GetNodeRenderer().GetStart() - firstNode.GetNodeRenderer().GetStart();
                    }
                    CopyBoard.copyNodes.Add(nodeInfo);
                    if (!CopyBoard.copyNodesLayersList.Contains(layerIndex))
                    {
                        CopyBoard.copyNodesLayersList.Add(layerIndex);
                    }
                }
            }
        }

        private void _DuplicateNodes(TimelinePopupMenuItem.Context ctx)
        {
            if (_SelectedNodes != null)
            {
                List<Node> tempNodeList = new List<Node>();
                for (int i = 0; i < _SelectedNodes.Count; i++)
                {
                    Node node = _SelectedNodes[i];
                    node.GetNodeRenderer().OnSelect = false;
                    int layerIndex = node.GetNodeRenderer().LayerIndex;
                    //计算最后一个节点的位置
                    float start = _GetLayerLastEndPosition(_TimelineSerializeInfo._Layers[layerIndex]);
                    float duration = node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart();
                    Node newNode = Reflections.Clone(node, new[] {typeof(Reflections.CloneField)});
                    newNode.GetNodeRenderer().SetStartAndEnd(start, start + duration);
                    _TimelineSerializeInfo._Layers[layerIndex].AddNode(newNode);
                    tempNodeList.Add(newNode);
                    if (!_NodesList.Contains(newNode))
                    {
                        _NodeListAdd(node);
                    }
                }
                _SelectedNodes.Clear();
                for (int i = 0; i < tempNodeList.Count; i++)
                {
                    Node node = tempNodeList[i];
                    node.GetNodeRenderer().OnSelect = true;
                    _SelectedNodes.Add(node);
                }
                _CurrentSelectNode = _SelectedNodes[0];

                _Serialize();
            }
        }

        #endregion

        public Rect Render(int x, int y, ref bool repaint)
        {
            _ExpandCursor = false;
            _LineBoundaryOffset = _LineBoundaryOffset > 0 ? _LineBoundaryOffset : _LeftPanelMinWidth;
            x = (int)_LineBoundaryOffset;

            int lineWidth = 4;
            _DrawBoundary(x - lineWidth / 2, y, lineWidth, _Height);

            int rightPanelWidth = _Width - x - lineWidth / 2;
            //            _TimelineAreaRect = new Rect(x, y, _Width, _Height);
            //            int timeRulerWidth = _Width;
            _TimelineAreaRect = new Rect(x, y, rightPanelWidth, _Height);
            int timeRulerWidth = rightPanelWidth;
            int timeRulerHeight = _TimeRulerHeight;
            _TimeRulerRect = new Rect(x, y, timeRulerWidth, timeRulerHeight);

            repaint = _ProcessEvent();
            
            GUILayout.BeginArea(_TimeRulerRect);
            _DrawTimeRuler(timeRulerWidth, timeRulerHeight);
            GUILayout.EndArea();

            //            int layerWidth = _Width;
            int layerWidth = rightPanelWidth;
            int layerHeight = _Height - _TimeRulerHeight;
            _LayerAreaRect = new Rect(x, y + _TimeRulerHeight, layerWidth, layerHeight);
            GUILayout.BeginArea(_LayerAreaRect);
            _DrawLayer(layerWidth, layerHeight, ref repaint);
            GUILayout.EndArea();

            int overlapHeight = 5;
            //int timePointerWidth = _Width;
            int timePointerWidth = rightPanelWidth;
            int timePointerHeight = _Height - _TimeRulerHeight + overlapHeight;
            GUILayout.BeginArea(new Rect(x, y + _TimeRulerHeight - overlapHeight, timePointerWidth, timePointerHeight));
            _DrawTimePointer(timePointerWidth, timePointerHeight);
            GUILayout.EndArea();

            //int horizontalScrollRectWidth = _Width;
            int horizontalScrollRectWidth = rightPanelWidth;
            int horizontalScrollRectHeight = _ScrollRectHeight;
            _HorizontalScrollRect = new Rect(x, y + _Height - _ScrollRectHeight * 2, horizontalScrollRectWidth, horizontalScrollRectHeight);
            GUILayout.BeginArea(_HorizontalScrollRect);
            _DrawHorizontalScroller(horizontalScrollRectWidth, horizontalScrollRectHeight, ref repaint);
            GUILayout.EndArea();

            int verticalScrollRectWidth = _VerticalScrollRectWidth;
            int verticalScrollRectHeight = _Height - _TimeRulerHeight - _ScrollRectHeight - _TitleHeight;
            //_VerticalScrollRect = new Rect(x + _Width - verticalScrollRectWidth, y + _TimeRulerHeight, verticalScrollRectWidth, verticalScrollRectHeight);
            _VerticalScrollRect = new Rect(x + rightPanelWidth - verticalScrollRectWidth, y + _TimeRulerHeight, verticalScrollRectWidth, verticalScrollRectHeight);
            GUILayout.BeginArea(_VerticalScrollRect);
            _DrawVerticalScroller(verticalScrollRectWidth, verticalScrollRectHeight, ref repaint);
            GUILayout.EndArea();

            var _LeftPanelRect = new Rect(0, y, x - lineWidth / 2, _Height);
            _DrawLeftPanel(_LeftPanelRect);

            repaint |= _DrawCursor();
            repaint = true;

            _Shortcuts.ApplyShortcut();

            return new Rect(x, y, _Width, _Height);
        }

        public void _AddShortcut(string shortcut, Action action)
        {
            _Shortcuts.AddShortcut(shortcut, action);
        }

        private bool _DrawCursor()
        {
            bool changeCursor = _LastExpandCursor != _ExpandCursor;
            _LastExpandCursor = _ExpandCursor;
            if (_ExpandCursor)
            {
                var texture2D = TimelineUtility.GetAssetTexture(TimelineConst.MOUSE_DRAG_ICON);
                TimelineUtility.SetCurrentViewCursor(texture2D, new Vector2(texture2D.width, texture2D.height) / 2, MouseCursor.CustomCursor);
            }
            else
            {
                TimelineUtility.SetCurrentViewCursor(null, Vector2.zero, MouseCursor.Arrow);
            }
            return changeCursor;
        }

        private void _DrawLeftPanel(Rect leftPanelRect)
        {
            var iconsPanelRect = new Rect(leftPanelRect.x, leftPanelRect.y, leftPanelRect.width, _TimeRulerHeight);
            GUILayout.BeginArea(iconsPanelRect);
            _DrawIconsPanel(iconsPanelRect);
            GUILayout.EndArea();
            var layerInfoRect = new Rect(leftPanelRect.x, leftPanelRect.y + _TimeRulerHeight , leftPanelRect.width, leftPanelRect.height - _TimeRulerHeight);
            GUI.DrawTexture(layerInfoRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.16f, 0.16f, 0.16f)));
            GUILayout.BeginArea(layerInfoRect);
            _DrawLayerContentInfo(layerInfoRect.width, layerInfoRect.height);
            GUILayout.EndArea();
        }

        private void _DrawLayerContentInfo(float width, float height)
        {
            // Draw layers Infos
            int layerHeightAndInterval = _LayerHeight + _LayerInterval / 2;
            int verticalOffset = Mathf.FloorToInt(_VerticalOffset);
            verticalOffset = Mathf.FloorToInt(_TimelineSerializeInfo._Layers.Count * _LayerHeight / (height - _HorizontalScrollRect.height) * verticalOffset);
            for (int i = 0; i < _TimelineSerializeInfo._Layers.Count; i++)
            {
                int offsetY = _LayerHeight * (i - verticalOffset / _LayerHeight);
                Rect layerRect = new Rect(0, offsetY, width, _LayerHeight);

                ITimelineLayer layer = _TimelineSerializeInfo._Layers[i];
                GUILayout.BeginArea(layerRect);
                var contentRect = new Rect(4, 0, layerRect.width - 8, _LayerHeight - _LayerInterval / 2);
                GUI.DrawTexture(contentRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.24f, 0.24f, 0.24f)));
                bool isOnSelect = _SelectedLayers.Contains(layer);
                if (isOnSelect)
                {
                    GUI.DrawTexture(contentRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.21f, 0.25f, 0.324f, 1f)));
                }

                if (_TimelineSerializeInfo._LayersTypeDict.ContainsKey(layer))
                {
                    Type type = _TimelineSerializeInfo._LayersTypeDict[layer];
                    var colorAttribute = type.GetCustomAttribute<LayerColor>();
                    if (colorAttribute != null)
                    {
                        _DrawContentRectBorder(contentRect, colorAttribute._Color);
                    }
                    else
                    {
                        _DrawContentRectBorder(contentRect, Color.white);
                    }
                    var attribute = type.GetCustomAttribute<LayerBindingType>();
                    if (attribute != null)
                    {
                        var bindingType = attribute.type;
                        _DrawBindingObject(contentRect, layer, bindingType);

                    }
                    else
                    {
                        _DrawLayerName(contentRect, layer);
                    }
                }
                else
                {
                    Debug.LogError("_LayersTypeDict do not contains key, key object = " + layer);
                }
                GUILayout.EndArea();
            }
        }

        private void _DrawLayerName(Rect rect, ITimelineLayer layer)
        {
            float leftInterval = 10f;
            var style = new GUIStyle(GUI.skin.label);
            var content = new GUIContent(_TimelineSerializeInfo._LayersTypeDict[layer].Name + " Layer");
            var size = style.CalcSize(content);
            GUI.Label(new Rect(rect.x + leftInterval, rect.y + (rect.height - size.y) / 2, rect.width, size.y), content, style);
        }

        private void _DrawBindingObject(Rect rect, ITimelineLayer layer, Type bindingType)
        {
            int leftInterval = 10;
            float fieldHeight = rect.height / 2;
            float fieldWidth = rect.width / 2;
            Rect fieldRect = new Rect(rect.x + leftInterval, rect.y + (rect.height - fieldHeight) / 2, fieldWidth, fieldHeight);
            GUI.DrawTexture(fieldRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.32f, 0.32f, 0.32f)));
            int borderWidth = 1;
            GUI.DrawTexture(new Rect(fieldRect.x - borderWidth, fieldRect.y, borderWidth, fieldRect.height), GUIColorTexture.GetOrCreateColorTexture(Color.black));
            GUI.DrawTexture(new Rect(fieldRect.x + fieldRect.width + borderWidth, fieldRect.y, borderWidth, fieldRect.height), GUIColorTexture.GetOrCreateColorTexture(Color.black));
            GUI.DrawTexture(new Rect(fieldRect.x, fieldRect.y, fieldRect.width + borderWidth * 2, borderWidth), GUIColorTexture.GetOrCreateColorTexture(Color.black));
            GUI.DrawTexture(new Rect(fieldRect.x, fieldRect.y + fieldRect.height, fieldRect.width + borderWidth * 2, borderWidth), GUIColorTexture.GetOrCreateColorTexture(Color.black));
            var texture = TimelineUtility.GetAssetTexture(TimelineConst.SELECT_ICON);

            var buttonRect = new Rect(fieldRect.x + fieldWidth + leftInterval, rect.y + (rect.height - texture.height) / 2, texture.width, texture.height);
            var style = new GUIStyle(GUI.skin.button);
            style.padding = new RectOffset(0, 0, 0, 0);
            if (GUI.Button(buttonRect, texture, style))
            {
                //to fix: this control id can not use to mark different button click
                _ControlID = EditorGUIUtility.GetControlID(FocusType.Passive, buttonRect);
                _CurrentSelectedObjLayer = layer;
                if (bindingType == typeof(GameObject))
                {
                    EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, "", _ControlID);
                }
                else if (bindingType == typeof(Animator))
                {
                    EditorGUIUtility.ShowObjectPicker<Animator>(null, true, "", _ControlID);
                }
                //Debug.LogError("click controlID = " + _ControlID);
            }
            if (Event.current.commandName == "ObjectSelectorUpdated")
            {
                //if (_ControlID == EditorGUIUtility.GetObjectPickerControlID())
                if(_CurrentSelectedObjLayer != null && _CurrentSelectedObjLayer == layer)
                {
                    Object selectedObj = EditorGUIUtility.GetObjectPickerObject();
                    GameObject gameObject = selectedObj as GameObject;

                    //Object typeObj = null;
                    //if (bindingType == typeof(GameObject))
                    //{
                    //    typeObj = (GameObject)selectedObj;
                    //}
                    //else if (bindingType == typeof(Animator))
                    //{
                    //    GameObject gameObject = selectedObj as GameObject;
                    //    var animator = gameObject.GetComponent<Animator>();
                    //    if (animator != null)
                    //    {
                    //        typeObj = animator;
                    //    }
                    //    else
                    //    {
                    //        //todo auto create
                    //    }
                    //}
                    AddToBindingObjects(layer, gameObject);
                }
                Event.current.Use();
            }
            else if (Event.current.commandName == "ObjectSelectorClosed")
            {
                //Debug.LogError("ObjectSelectorClosed-----------");
                Event.current.Use();
            }

            //DragAndDrop Event
            _HandleDragAndDrop(fieldRect, layer, bindingType);

            if (_TimelineSerializeInfo._BindingObjects.ContainsKey(layer))
            {
                var info = _TimelineSerializeInfo._BindingObjects[layer];
                GUI.Label(new Rect(fieldRect.x, fieldRect.y, fieldRect.width, fieldRect.height), string.Format("{0}", info == null ? string.Format("{0} ({1})", "None", bindingType.Name) : info.Name));
            }
            else
            {
                GUI.Label(new Rect(fieldRect.x, fieldRect.y, fieldRect.width, fieldRect.height), string.Format("{0} ({1})", "None", bindingType.Name));
            }
        }

        private void AddToBindingObjects(ITimelineLayer layer, GameObject gameObject)
        {
            _BindingController.AddBindingObject(layer, gameObject);
            _Serialize();
        }

        private void _HandleDragAndDrop(Rect fieldRect, ITimelineLayer layer, Type bindingType)
        {
            if (fieldRect.Contains(Event.current.mousePosition))
            {
                //select the binding gameobject
                if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
                {
                    Selection.activeObject = _BindingController.GetBindingObject(layer);
                }
                if (Event.current.type == EventType.DragUpdated)
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    Event.current.Use();
                }
                else if (Event.current.type == EventType.DragPerform)
                {
                    if (DragAndDrop.objectReferences.Length > 0)
                    {
                        Func<GameObject, Type, bool> canAddFunc = (obj, type) =>
                        {
                            if (obj == null) return false;
                            //is instance in hierarchy
                            var gameObjects = GameObjectFindUtils.GetAllGameObjects(EditorSceneManager.GetActiveScene());
                            bool isFind = false;
                            for (int i = 0; i < gameObjects.Count; i++)
                            {
                                if (obj == gameObjects[i])
                                {
                                    isFind = true;
                                    break;
                                }
                            }
                            if (!isFind) return false;

                            Type objType = obj.GetType();
                            if (objType == type)
                            {
                                return true;
                            }
                            if (obj.GetComponent(type) != null)
                            {
                                return true;
                            }
                            return false;
                        };
                        GameObject gameObject = DragAndDrop.objectReferences[0] as GameObject;
                        bool isCanAdd = canAddFunc(gameObject, bindingType);
                        if (isCanAdd)
                        {
                            AddToBindingObjects(layer, gameObject);
                        }
                    }
                    else if (DragAndDrop.objectReferences.Length > 1)
                    {
                        Debug.LogError("can not drag multiple objects in one assignment!!!");
                    }
                    Event.current.Use();
                }
            }
        }

        private void _DrawContentRectBorder(Rect rect, Color color)
        {
            int borderWidth = 2;
            GUI.DrawTexture(new Rect(rect.x - borderWidth / 2, rect.y + borderWidth / 2, borderWidth, rect.height), GUIColorTexture.GetOrCreateColorTexture(color));
            GUI.DrawTexture(new Rect(rect.x + rect.width - borderWidth / 2, rect.y + borderWidth / 2, borderWidth, rect.height), GUIColorTexture.GetOrCreateColorTexture(color));
            GUI.DrawTexture(new Rect(rect.x, rect.y + borderWidth / 2, rect.width, borderWidth), GUIColorTexture.GetOrCreateColorTexture(color));
            GUI.DrawTexture(new Rect(rect.x, rect.y + rect.height - borderWidth / 2, rect.width, borderWidth), GUIColorTexture.GetOrCreateColorTexture(color));
        }

        private void _DrawIconsPanel(Rect rect)
        {
            var foreFrontTexture = TimelineUtility.GetAssetTexture(TimelineConst.FORE_FRONT_ICON);
            var frontFrameTexture = TimelineUtility.GetAssetTexture(TimelineConst.FORE_FRONT_ICON);
            var playIconTexture = TimelineUtility.GetAssetTexture(TimelineConst.PLAY_ICON);
            var behineFrameTexture = TimelineUtility.GetAssetTexture(TimelineConst.BEHINE_FRAME_ICON);
            var finalFrameTexture = TimelineUtility.GetAssetTexture(TimelineConst.FINAL_FRAME_ICON);
            var areaPlayTexture = TimelineUtility.GetAssetTexture(TimelineConst.AREA_PLAY_ICON);

            int iconWidth = 30;
            int iconHeight = 20;
            if (GUI.Button(new Rect(rect.x, rect.y, iconWidth, iconHeight), foreFrontTexture))
            {
                SetCurrentFrameIndex(0);
                _RulerOffset = 0;
                _HorizontalOffset = 0;
            }

            if (GUI.Button(new Rect(rect.x + iconWidth, rect.y, iconWidth, iconHeight), frontFrameTexture))
            {
                int frameIndex = _CurrentFrameIndex - 1;
                SetCurrentFrameIndex(frameIndex);
            }

            var playIconRect = new Rect(rect.x + iconWidth * 2, rect.y, iconWidth, iconHeight);
            if (GUI.Button(playIconRect, playIconTexture))
            {
                if (_IsPlaying)
                {
                    _IsPlaying = false;
                }
                else
                {
                    _IsPlaying = true;
                }
                GUI.FocusControl(null);
            }

            if (_IsPlaying)
            {
                double currentTime = EditorApplication.timeSinceStartup;
                if (_LastRecordTime > 0)
                {
                    var offsetTime = currentTime - _LastRecordTime;
                    _PassTimeRecord += offsetTime;
                    var oneFrameTime = TimelineUtility.FramesConvertToTime(1, _FrameRate);
                    if ((float) _PassTimeRecord >= oneFrameTime)
                    {
                        _PassTimeRecord = 0;
                        int frameIndex = _CurrentFrameIndex + 1;
                        SetCurrentFrameIndex(frameIndex);
                        CheckPlayFinish();
                    }
                }
                _LastRecordTime = EditorApplication.timeSinceStartup;
                GUI.DrawTexture(playIconRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.22f, 0.51f, 1f, 0.5f)));
            }
            else
            {
                _LastRecordTime = 0;
                _PassTimeRecord = 0;
            }

            if (GUI.Button(new Rect(rect.x + iconWidth * 3, rect.y, iconWidth, iconHeight), behineFrameTexture))
            {
                int frameIndex = _CurrentFrameIndex + 1;
                SetCurrentFrameIndex(frameIndex);
            }

            if (GUI.Button(new Rect(rect.x + iconWidth * 4, rect.y, iconWidth, iconHeight), finalFrameTexture))
            {
                if (_NodesList.Count > 0)
                {
                    _NodesList.Sort(_SortFunc);
                    int frameIndex = TimelineUtility.TimeConvertToFrames(_NodesList[_NodesList.Count - 1].GetNodeRenderer().GetEnd(), _FrameRate);
                    SetCurrentFrameIndex(frameIndex);
                    _RulerOffset = TimelineUtility.FramesConvertToDistance(_CurrentFrameIndex, _BaseFramePadding, _RulerScale) / 2;
                    _HorizontalOffset = (_CurrentFrameIndex * 1f / _TotalFrames) * (_Width - _LineBoundaryOffset - _ScrollTotalHandleSize);
                }
                GUI.FocusControl(null);
            }

            if (GUI.Button(new Rect(rect.x + iconWidth * 5, rect.y, iconWidth, iconHeight), areaPlayTexture))
            {
                Debug.LogError("areaPlayTexture click");
                GUI.FocusControl(null);
            }

            _CurrentFrameTextFieldRect = new Rect(rect.x + iconWidth * 6, rect.y, rect.width - (rect.x + iconWidth * 6) - 10, iconHeight);
            var currentframeStr = GUI.TextField(_CurrentFrameTextFieldRect, _CurrentFrameIndex.ToString());
            int currentFrameIndex = 0;
            if (int.TryParse(currentframeStr, out currentFrameIndex))
            {
                if (currentFrameIndex != _CurrentFrameIndex)
                {
                    SetCurrentFrameIndex(currentFrameIndex);
                }
            }
           
        }

        private void CheckPlayFinish()
        {
            int maxFrameIndex = 0;
            if (_NodesList.Count > 0)
            {
                maxFrameIndex = TimelineUtility.TimeConvertToFrames(_NodesList[_NodesList.Count - 1].GetNodeRenderer().GetEnd(), _FrameRate);
            }

            if (_CurrentFrameIndex > maxFrameIndex)
            {
                _IsPlaying = false;
                SetCurrentFrameIndex(0);
            }
        }

        private void _DrawHorizontalScroller(int width, int height, ref bool repaint)
        {
            //draw backgruond
            GUI.DrawTexture(new Rect(0,0,width,height), GUIColorTexture.GetOrCreateColorTexture(new Color(0.207f, 0.207f, 0.207f)));

            float maxEndTime = 0;
            for (int i = 0; i < _TimelineSerializeInfo._Layers.Count; i++)
            {
                float endTime = _GetLayerLastEndPosition(_TimelineSerializeInfo._Layers[i]);
                if (endTime > maxEndTime)
                {
                    maxEndTime = endTime;
                }
            }
            _TotalFrames = TimelineConst.DEFAULT_TOTAL_FRAMES;
            _TotalFrames = Mathf.Max(_TotalFrames, TimelineUtility.TimeConvertToFrames(maxEndTime, _FrameRate));
            int totalHandleSize = Mathf.FloorToInt(1f * _ShowTotalFrames / _TotalFrames * width);
            totalHandleSize = Mathf.Min(width, totalHandleSize);  //限制最大值
            totalHandleSize = Mathf.Max(totalHandleSize, _ScrollZoomHandleSize * 2 + 10);  //限制最小handlesize
            _ScrollTotalHandleSize = totalHandleSize;

            int middleHandleSize = totalHandleSize - _ScrollZoomHandleSize * 2;
            //draw middle handle
            var middleHandleRectX = Mathf.Min(_HorizontalOffset + _ScrollZoomHandleSize, width - middleHandleSize - _ScrollZoomHandleSize);
            Rect middleHandleRect = new Rect(middleHandleRectX, 0, middleHandleSize, height);
            GUI.DrawTexture(middleHandleRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.406f, 0.406f, 0.406f)));

            var leftHandleRectX = Mathf.Min(_HorizontalOffset, width - totalHandleSize);
            _ScrollLeftZoomRect = new Rect(leftHandleRectX, 0, _ScrollZoomHandleSize, height);

            var rightHandleRectX = Mathf.Min(_HorizontalOffset + middleHandleSize + _ScrollZoomHandleSize, width - _ScrollZoomHandleSize);
            _ScrollRightZoomRect = new Rect(rightHandleRectX, 0, _ScrollZoomHandleSize, height);

            bool isInLeft = _ScrollLeftZoomRect.Contains(Event.current.mousePosition);
            bool isInRight = _ScrollRightZoomRect.Contains(Event.current.mousePosition);
            if (isInLeft || isInRight)
            {
                _ExpandCursor = true;
            }

            //draw zoom handle
            GUI.DrawTexture(_ScrollLeftZoomRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.48f, 0.48f, 0.48f)));
            GUI.DrawTexture(_ScrollRightZoomRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.48f, 0.48f, 0.48f)));
        }

        private void _DrawVerticalScroller(int width, int height, ref bool repaint)
        {
            if (IsShowVerticalScroll())
            {
                //draw background
                GUI.DrawTexture(new Rect(0, 0, width, height), GUIColorTexture.GetOrCreateColorTexture(new Color(0.207f, 0.207f, 0.207f)));
                _SliderRatio = _VerticalOffset / height; 
                _SliderRatio = Mathf.Clamp(_SliderRatio, 0, 1);

                int handleSize = Mathf.FloorToInt(1f * height /_LayerHeight / _TimelineSerializeInfo._Layers.Count * height);
                handleSize = handleSize > height ? height : handleSize;
                _VerticalHandleSize = handleSize;
                var ratioY = _SliderRatio * (height - handleSize);
                var handleRectY = ratioY;
                Rect handleRect = new Rect(0, handleRectY, width, handleSize);
                GUI.DrawTexture(handleRect, GUIColorTexture.GetOrCreateColorTexture(new Color(0.406f, 0.406f, 0.406f)));
            }
        }

        public void SetCurrentFrameIndex(int frameIndex)
        {
            if (frameIndex >= 0 && frameIndex != _CurrentFrameIndex)
            {
                _CurrentFrameIndex = frameIndex;
                CheckAllNodesEvent();
            }
        }

        public int GetCurrentFrameIndex()
        {
            return _CurrentFrameIndex;
        }

        private void CheckAllNodesEvent()
        {
            float deltaTime = TimelineUtility.FramesConvertToTime(1, _FrameRate);
            
            for (int i = 0; i < _TimelineSerializeInfo._Layers.Count; i++)
            {
                var layer = _TimelineSerializeInfo._Layers[i];
                layer.GetAllNodes().Sort(SortLayerNodes);
                var nodeList = layer.GetAllNodes();
                int crossNodeCount = 0;
                for (int j = 0; j < nodeList.Count; j++)
                {
                    Node node = nodeList[j];
                    int nodeStartFrame = TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetStart(), _FrameRate);
                    int nodeEndFrame = TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetEnd(), _FrameRate);

                    var nodeEvent = node.GetNodeEvent();
                    if(nodeEvent == null)
                        continue;

                    if (_CurrentFrameIndex >= nodeStartFrame && _CurrentFrameIndex < nodeEndFrame)
                    {
                        if (!nodeEvent.IsEnteredNode)
                        {
                            nodeEvent.OnStartPlay();
                            nodeEvent.IsEnteredNode = true;
                        }
                        nodeEvent.ProcessFrame(deltaTime);
                        //mix检测实现
                        crossNodeCount++;
                        if (crossNodeCount > 1)
                        {
                            Node lastNode = nodeList[j - 1];
                            if (lastNode == null)
                            {
                                Debug.LogError("lastNode == null, totalNodes count = " + nodeList.Count + " j - 1 = " + (j - 1));
                                return;
                            }
                            int lastNodeEndFrame = TimelineUtility.TimeConvertToFrames(lastNode.GetNodeRenderer().GetEnd(), _FrameRate);
                            int overlapRegionFrames = lastNodeEndFrame - nodeStartFrame;
                            int offsetFrames = _CurrentFrameIndex - nodeStartFrame;
                            float currentWeight = offsetFrames * 1.0f / overlapRegionFrames;
                            if (Mathf.Approximately(currentWeight, 1))
                            {
                                currentWeight = 1f;
                            }
                            lastNode.GetNodeEvent().ProcessFrameWithWeight(deltaTime, 1 - currentWeight);
                            nodeEvent.ProcessFrameWithWeight(deltaTime, currentWeight);
                        }
                        else
                        {
                            nodeEvent.ProcessFrameWithWeight(deltaTime, 1);
                        }
                    }
                    else
                    {
                        if (nodeEvent.IsEnteredNode)
                        {
                            if (_CurrentFrameIndex == nodeEndFrame)
                            {
                                nodeEvent.ProcessFrame(deltaTime);
                            }
                            nodeEvent.OnEndPlay();
                            nodeEvent.IsEnteredNode = false;
                        }
                        //mix检测实现
                        crossNodeCount--;
                        nodeEvent.ProcessFrameWithWeight(deltaTime, 0);
                    }
                }
            }
        }

        private void _DrawLayer(int width, int height, ref bool repaint)
        {
            // Draw background
            GUI.DrawTexture(new Rect(0, 0, width, height), GUIColorTexture.GetOrCreateColorTexture(new Color(0.16f, 0.16f, 0.16f)));

            // Draw layers
            int verticalOffset = Mathf.FloorToInt(_VerticalOffset);
            verticalOffset = Mathf.FloorToInt(_TimelineSerializeInfo._Layers.Count * _LayerHeight / (height - _HorizontalScrollRect.height) * verticalOffset);
            //verticalOffset = Mathf.Min(verticalOffset,  Mathf.FloorToInt(_VerticalScrollRect.height - _VerticalHandleSize + _HorizontalScrollRect.height));
            //verticalOffset = Mathf.Min(verticalOffset, Mathf.FloorToInt(_Layers.Count * _LayerHeight + _HorizontalScrollRect.height));
            for (int i = 0; i < _TimelineSerializeInfo._Layers.Count; i++)
            {
                int offsetY = _LayerHeight * (i - verticalOffset / _LayerHeight);
                Rect layerRect = new Rect(0, offsetY, width, _LayerHeight);

                ITimelineLayer layer = _TimelineSerializeInfo._Layers[i];
                GUILayout.BeginArea(layerRect);
                bool isOnSelect = _SelectedLayers.Contains(layer);
                layer.OnDrawGUI(0, 0, width, _LayerHeight, isOnSelect);

                // Node
                var nodes = layer.GetAllNodes();
                for (int j = 0; j < nodes.Count; j++)
                {
                    AbstractNode abstractNode = nodes[j].GetNodeRenderer();
                    int startFrame = TimelineUtility.TimeConvertToFrames(abstractNode.GetStart(), _FrameRate);
                    int endFrame = TimelineUtility.TimeConvertToFrames(abstractNode.GetEnd(), _FrameRate);
                    //int startPosX = FrameConvertToDistanceOnDraw(startFrame, _RulerOffset);
                    //int endPosX = FrameConvertToDistanceOnDraw(endFrame, _RulerOffset);
                    int startPosX = FrameConvertToDistance(startFrame, _RulerOffset);
                    int endPosX = FrameConvertToDistance(endFrame, _RulerOffset);
                    abstractNode.Render(startPosX, 0, endPosX - startPosX, _LayerHeight);

                    Rect leftBorderRect = new Rect(startPosX, 0, _NodeBorderDetectionWidth, _LayerHeight);
                    Rect rightBorderRect = new Rect(endPosX - _NodeBorderDetectionWidth, 0, _NodeBorderDetectionWidth, _LayerHeight);
                    //GUI.DrawTexture(leftBorderRect, GUIColorTexture.GetOrCreateColorTexture(Color.black));
                    //GUI.DrawTexture(rightBorderRect, GUIColorTexture.GetOrCreateColorTexture(Color.black));
                    bool isInLeft = leftBorderRect.Contains(Event.current.mousePosition);
                    bool isInRight = rightBorderRect.Contains(Event.current.mousePosition);
                    if (isInLeft || isInRight)
                    {
                        _ExpandCursor = true;
                    }
                }
                GUI.Label(new Rect(0, 0, width, _LayerHeight), "#" + i);
                GUILayout.EndArea();
            }
        }

        private void _DrawBoundary(int x, int y, int lineWidth, int height)
        {
            var rect = new Rect(x, y, lineWidth, height);
            _BoundaryRect = rect;
            GUI.DrawTexture(rect, GUIColorTexture.GetOrCreateColorTexture(Color.gray));
            if (rect.Contains(Event.current.mousePosition))
            {
                _ExpandCursor = true;
            }
        }

        private void _DrawTimeRuler(int width, int height)
        {
            //Draw background
            GUI.DrawTexture(new Rect(0, 0, width, height), GUIColorTexture.GetOrCreateColorTexture(new Color(0.215f, 0.215f, 0.215f)));
            _DrawRuling(width, height);
        }

        private struct FrameIntervalResult
        {
            public int frameInterval;
            public int type;   //1-正常画，2-不画自身Label，3-不画子刻度
        }

        private void _DrawRuling(int width, int height)
        {
            float baseFrameHeight = 1f * height / 2;
            float intRulerOffset = _RulerOffset;
            float currentFramePadding = _BaseFramePadding * _RulerScale;
            _ShowTotalFrames = Mathf.FloorToInt(2 + width / currentFramePadding);
            Texture2D texture = GUIColorTexture.GetOrCreateColorTexture(new Color(0.7f, 0.7f, 0.7f));

            var style = new GUIStyle(GUI.skin.label);
            style.normal.textColor = Color.red;
            style.fontStyle = FontStyle.Bold;

            int[] frameInterval = {1, 5, 10, 30, 60, 300, 600, 1800, 3600, 7200};
            List<FrameIntervalResult> resultList = new List<FrameIntervalResult>();
            for (int j = 0; j < frameInterval.Length; j++)
            {
                int interval = frameInterval[j];
                FrameIntervalResult result = new FrameIntervalResult {frameInterval = interval};
                float showFrameDist =  interval * currentFramePadding;
                if (showFrameDist < 1f * _BaseFramePadding / 8) //不画子级的刻度
                {
                    result.type = 3;
                }
                else if (showFrameDist < _BaseFramePadding) //不画Label
                {
                    result.type = 2;
                }
                else
                {
                    result.type = 1;
                }
                resultList.Add(result);
            }

            //获取当前最小子刻度
            int currentInterval = resultList[0].frameInterval;
            for (int i = 0; i < resultList.Count; i++)
            {
                if (resultList[i].type == 2)
                {
                    currentInterval = resultList[i].frameInterval;
                }
            }

            for (int i = 0; i < _ShowTotalFrames; i++)
            {
                int currentFrame = Mathf.FloorToInt(i +  intRulerOffset / currentFramePadding);

                float frameHeight = baseFrameHeight;
                //询问当前帧要不要画
                bool isDrawTexture = false;
                //询问当前帧Label要不要画
                bool isDrawLabel = false;
                if (_RulerScale > 1f)
                {
                    isDrawTexture = true;
                    isDrawLabel = true;
                }
                for (int j = 0; j < resultList.Count; j++)
                {
                    int interval = resultList[j].frameInterval;
                    int type = resultList[j].type;
                    if (currentFrame % interval == 0 && (type == 1 || type == 2))
                    {
                        isDrawTexture = true;
                        if (type == 2)
                        {
                            if (currentFrame % currentInterval == 0)
                            {
                                frameHeight = baseFrameHeight / 2;
                            }
                            else
                            {
                                frameHeight = baseFrameHeight * _RulerScale;
                            }
                        }
                        break;
                    }
                }
                
                for (int j = 0; j < resultList.Count; j++)
                {
                    int interval = resultList[j].frameInterval;
                    if (currentFrame % interval == 0 && resultList[j].type == 1)
                    {
                        isDrawLabel = true;
                        frameHeight = baseFrameHeight;
                    }
                }

                if (isDrawTexture)
                {
                    GUI.DrawTexture(new Rect(Mathf.FloorToInt(currentFramePadding * i - (intRulerOffset % currentFramePadding)), height - frameHeight, _BaseFrameWidth, frameHeight), texture);
                }
                if (isDrawLabel)
                {
                    GUI.Label(new Rect(Mathf.FloorToInt(currentFramePadding * i - (intRulerOffset % currentFramePadding)), 0, width, height), currentFrame.ToString());
                }
            }

            GUI.Label(new Rect(0, 10, width, height), "F=" + _ShowTotalFrames + ", S=" + _RulerScale + ", O=" + _RulerOffset + ", IO=" + intRulerOffset + ", MOD=" + intRulerOffset % currentFramePadding, style);
        }

        private void _DrawTimePointer(int width, int height)
        {
            var style = new GUIStyle(GUI.skin.label);
            style.normal.textColor = Color.green;
            style.fontStyle = FontStyle.Bold;
            var guiContent = new GUIContent(_CurrentFrameIndex.ToString());
            var size = style.CalcSize(guiContent).x;

            int posX = FrameConvertToDistance(_CurrentFrameIndex, _RulerOffset);
            GUI.DrawTexture(new Rect(posX, 0, _TimePointerWidth, height), GUIColorTexture.GetOrCreateColorTexture(Color.white));
            GUI.Label(new Rect(posX - size / 2, 5, size, 30), guiContent, style);
        }

        public void SetSize(int width, int height)
        {
            _Width = width;
            _Height = height;
        }

        public Vector2 Size()
        {
            return new Vector2(_Width, _Height);
        }

        public void AddLayer(Type scriptableObjectType, int index)
        {
            var layer = new BaseLayer();
            //Undo.RegisterCreatedObjectUndo(layer, "Layer");
            _AddLayer(layer, scriptableObjectType, index);
            _Serialize();
        }

        private int _AddLayer(ITimelineLayer layer, Type scriptableObjectType, int index)
        {
            if (index >= 0 && index < _TimelineSerializeInfo._Layers.Count)
            {
                _TimelineSerializeInfo._Layers.Insert(index, layer);
                //_CurrentSelectedLayer = layer;
                //_CurrentSelectedLayerIndex = index;
                //_SelectedLayers.Clear();
                //_SelectedLayers.Add(_CurrentSelectedLayer);
            }
            else
            {
                _TimelineSerializeInfo._Layers.Add(layer);
                index = _TimelineSerializeInfo._Layers.Count - 1;
            }
            _TimelineSerializeInfo._LayersTypeDict.Add(layer, scriptableObjectType);
            EditorUtility.SetDirty(this);  
            return index;
        }

        public void RemoveLayer(int layerIndex)
        {
            var layer = _TimelineSerializeInfo._Layers[layerIndex];
            _TimelineSerializeInfo._LayersTypeDict.Remove(layer);
//            if (_OldBindingObjects.ContainsKey(_TimelineSerializeInfo._Layers[layerIndex]))
//            {
//                _OldBindingObjects.Remove(_TimelineSerializeInfo._Layers[layerIndex]);
//            }
            if (_TimelineSerializeInfo._BindingObjects.ContainsKey(_TimelineSerializeInfo._Layers[layerIndex]))
            {
                _TimelineSerializeInfo._BindingObjects.Remove(_TimelineSerializeInfo._Layers[layerIndex]);
                _Serialize();
            }

            _TimelineSerializeInfo._Layers.RemoveAt(layerIndex);
        }

        #region Internal Methods

        private int FrameConvertToDistance(int frameIndex, float offset)
        {
            float intRulerOffset = offset;
            float currentFramePadding = _BaseFramePadding * _RulerScale;
            int offsetFrameIndex = frameIndex - Mathf.FloorToInt(intRulerOffset / currentFramePadding);
            int posX = Mathf.FloorToInt(currentFramePadding * offsetFrameIndex - intRulerOffset % currentFramePadding);
            return posX;
        }

        private int FrameConvertToDistanceOnDraw(int frameIndex, float offset)
        {
            float intRulerOffset = offset;
            float currentFramePadding = _BaseFramePadding * _RulerScale;
            int offsetFrameIndex = Mathf.FloorToInt(frameIndex - intRulerOffset / currentFramePadding);
            int posX = Mathf.FloorToInt(currentFramePadding * offsetFrameIndex - intRulerOffset % currentFramePadding);
            return posX;
        }

        private bool IsShowVerticalScroll()
        {
            if (GetTotalShowUIHeight() > _Height)
            {
                return true;
            }
            return false;
        }

        private int GetTotalShowUIHeight()
        {
            int totalShowUIHeight = 0;
            //+ ruler height
            totalShowUIHeight += _TimeRulerHeight;
            //+ layers height
            totalShowUIHeight += _TimelineSerializeInfo._Layers.Count * _LayerHeight;
            //+ horizontal scroll height
            totalShowUIHeight += _ScrollRectHeight;
            //+ title height = 20
            totalShowUIHeight += _TitleHeight;

            return totalShowUIHeight;
        }

        private bool _IsSelectMultiLayerNodes()
        {
            int layer = -1;
            for (int i = 0; i < _SelectedNodes.Count; i++)
            {
                int nodeLayerIndex = _SelectedNodes[i].GetNodeRenderer().LayerIndex;
                if (layer == -1)
                {
                    layer = nodeLayerIndex;
                }
                if (layer > 0 && layer != nodeLayerIndex)
                {
                    return true;
                }
            }
            return false;
        }

        private float _GetLayerLastEndPosition(ITimelineLayer layer)
        {
            List<Node> nodes = layer.GetAllNodes();
            float maxEnd = 0;
            foreach (var node in nodes)
            {
                float end = node.GetNodeRenderer().GetEnd();
                if (end > maxEnd)
                    maxEnd = end;
            }
            return maxEnd;
        }

        private ITimelineLayer _GetLayerByIndex(int layerIndex)
        {
            if (layerIndex >= 0 && layerIndex < _TimelineSerializeInfo._Layers.Count)
            {
                return _TimelineSerializeInfo._Layers[layerIndex];
            }
            return null;
        }

        #endregion


        #region External Methods

        public void Destroy()
        {
            ClearSelectedLayers();
            for (int i = 0; i < _SelectedNodes.Count; i++)
            {
                _SelectedNodes[i].GetNodeRenderer().OnSelect = false;
            }
            _SelectedNodes.Clear();
            _CurrentSelectNode = null;
            _LastSelectNode = null;
            _MenuItems.Clear();
            _ExtendItems.Clear();
            _LayerItems.Clear();
            _NodeItems.Clear();
            _InsertItems.Clear();
            _Shortcuts = null;
            _CurrentSelectNode = null;
            _CurrentSelectedLayerIndex = -1;
        }

        public List<Node> GetSelectedNodes()
        {
            return _SelectedNodes;
        }

        public int GetFrameRate()
        {
            return _FrameRate;
        }

        public List<Node> GetAllNodes()
        {
            return _NodesList;
        }

        public ITimelineLayer GetLayerByIndex(int layerIndex)
        {
            return _GetLayerByIndex(layerIndex);
        }

        public void RegisterNodeType<T>(string menu)
        {
            RegisterNodeType(menu, typeof(T));
        }

        private List<Object> _BindingObjectsList = new List<Object>();
        public List<Object> GetBindingObjects()
        {
            _BindingObjectsList.Clear();
            for (int i = 0; i < _TimelineSerializeInfo._Layers.Count; i++)
            {
                var layer = _TimelineSerializeInfo._Layers[i];
                var obj = _BindingController.GetBindingObject(layer);
                if(obj == null) continue;
                _BindingObjectsList.Add(obj);
            }
            return _BindingObjectsList;
        }

        #endregion

    }
}
