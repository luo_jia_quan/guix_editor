﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    [Serializable]
    public class GUITimelineDictionary : Dictionary<ITimelineLayer, SerializableSystemType>, ISerializationCallbackReceiver
    {
        [SerializeReference]
        private List<ITimelineLayer> _keys = new List<ITimelineLayer>();
        [SerializeField]
        private List<SerializableSystemType> _values = new List<SerializableSystemType>();

        public void OnBeforeSerialize()
        {
            _keys.Clear();
            _values.Clear();
            foreach (var temp in this)
            {
                _keys.Add(temp.Key);
                _values.Add(temp.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            Clear();
            int minCount = Math.Min(_keys.Count, _values.Count);
            for (int i = 0; i < minCount; i++)
            {
                Add(_keys[i], _values[i]);
            }
        }
    }
}
