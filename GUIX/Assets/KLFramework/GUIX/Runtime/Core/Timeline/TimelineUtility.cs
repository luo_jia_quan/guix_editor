﻿using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

namespace KLFramework.GUIX.Timeline.Editor
{
    public class TimelineUtility
    {
        private static Dictionary<string, Texture> _CacheTextures = new Dictionary<string, Texture>();

        public static Texture GetAssetTexture(string assetPath)
        {
            if (_CacheTextures == null)
            {
                _CacheTextures = new Dictionary<string, Texture>();
            }

            if (!_CacheTextures.ContainsKey(assetPath))
            {
                _CacheTextures.Add(assetPath, AssetDatabase.LoadAssetAtPath<Texture>(assetPath));
            }

            return _CacheTextures[assetPath];
        }

        /// <summary>
        /// Editor下设置鼠标图标
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="hostPost"></param>
        /// <param name="mode"></param>
        public static void SetCurrentViewCursor(Texture texture, Vector2 hostPost, MouseCursor mode)
        {
            var setCurrentViewCursorMethod = typeof(EditorGUIUtility).GetMethod("SetCurrentViewCursor", BindingFlags.Static | BindingFlags.NonPublic);
            setCurrentViewCursorMethod.Invoke(typeof(EditorGUIUtility), new Object[]{ texture, hostPost, mode });
        }

        /// <summary>
        /// 时间转换成坐标距离
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static float TimeConvertToDistance(float seconds, int baseFramePadding, float rulerScale, int frameRate)
        {
            //return Mathf.FloorToInt(seconds * frameRate * baseFramePadding * rulerScale);
            return seconds * frameRate * baseFramePadding * rulerScale;
        }

        /// <summary>
        /// 帧数转换成坐标距离
        /// </summary>
        /// <param name="frameIndex"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static float FramesConvertToDistance(int frames, int baseFramePadding, float rulerScale)
        {
            return frames * baseFramePadding * rulerScale;
        }

        /// <summary>
        /// 坐标距离转换为时间
        /// </summary>
        /// <param name="dist"></param>
        /// <returns></returns>
        public static float DistanceConvertToTime(float dist, int baseFramePadding, float rulerScale, int frameRate)
        {
            return 1f / frameRate * (dist / baseFramePadding / rulerScale);
        }

        /// <summary>
        /// 坐标距离转换为帧数
        /// </summary>
        /// <param name="dist"></param>
        /// <returns></returns>
        public static int DistanceConvertToFrames(float dist, int baseFramePadding, float rulerScale)
        {
            float currentFramePadding = baseFramePadding * rulerScale;
            int frames = Mathf.FloorToInt(dist / currentFramePadding);
            return frames;
        }

        /// <summary>
        /// 帧转换时间
        /// </summary>
        /// <param name="frames"></param>
        /// <returns>seconds</returns>
        public static float FramesConvertToTime(int frames, int frameRate)
        {
            return 1f * frames / frameRate;
        }

        /// <summary>
        /// 时间转换帧
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static int TimeConvertToFrames(float seconds, int frameRate)
        {
            return Mathf.FloorToInt(seconds * frameRate);
        }

    }
}
