﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    [ExecuteInEditMode]
    public class DirectorTest : MonoBehaviour
    {
        public GameObject _GameObject;
        public Object[] _object;

        void OnEnable()
        {
            if (_GameObject != null)
            {
//                List<ObjectOverride> list = PrefabUtility.GetObjectOverrides(_GameObject, false);
//                foreach (var temp in list)
//                {
//                    Debug.LogError("temp = " + temp);
//                }

//                 var gameObjects = GameObject.FindObjectsOfType<GameObject>();
//                 foreach (var o in gameObjects)
//                 {
////                     //if(PrefabUtility.IsPartOfPrefabInstance(o))
////                     if (PrefabUtility.IsPartOfAnyPrefab(o))
////                     //if(PrefabUtility.IsAnyPrefabInstanceRoot(o))
//                     if(PrefabUtility.IsPartOfPrefabInstance(o))
//                     {
//                        Debug.LogError(o.name);
//                     }
//
//                 }
//
//
                string path = _GameObject.transform.GetFullHierarchyPath();
                var obj = GameObject.Find(path);
                if (obj != null)
                {
                    Debug.LogError("name = " + obj.name);
                }
                else
                {
                    //Debug.LogError("can not find");
                }
            }
        }

    }
}
