﻿using System;
using SerializeExtend;
using UnityEditor.Animations;
using UnityEngine;
using Color = UnityEngine.Color;
using Reflections = KLFramework.Utils.Reflections;

namespace KLFramework.GUIX.Timeline
{
    [Reflections.ClonePublicField]
    [NodeEventType(typeof(AnimationNodeEvent))]
    [LayerBindingType(typeof(GameObject))]
    [LayerColor(1,0,0)]
    public class AnimationNodeData : ScriptableObject
    {
        [Reflections.NoCloneField]
        [CustomSerializeField]
        public float AAA;
        public int BBB;
        public AnimationNodeData DDD;
        public AnimatorController AnimatorController;
        [Reflections.CloneField]
        private int EEEEE;

    }



    [Reflections.ClonePublicField]
    [Serializable]
    [LayerBindingType(typeof(Animator))]
    public class DDDDD : ScriptableObject
    {
        [Reflections.NoCloneField]
        public float FFFF;
        public int AASDASD;
        public int AASDASD2222;

    }
}
