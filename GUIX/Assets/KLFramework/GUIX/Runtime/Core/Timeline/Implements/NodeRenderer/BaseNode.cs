﻿using System;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    [Serializable]
    public class BaseNode : AbstractNode
    {
        public BaseNode()
        {
        }

        public BaseNode(float start, float end, int layerIndex)
        {
            this.start = _LimitStartValue(start);
            this.end = end;
            this.LayerIndex = Mathf.Max(0, layerIndex);
        }

        protected override void RenderSelf(int x, int y, int width, int height)
        {
            height = height - TimelineConst.LAYER_INTERVAL / 2;
            Rect rect = new Rect(x, y, width, height);
            GUI.DrawTexture(rect, GUIColorTexture.GetOrCreateGradientTexture(GUIColorTexture.Crystal(new Color(0.25f, 0.265f, 0.289f, 0.75f))));
            
            if (OnSelect)
            {
                //Debug.LogError("OnSelect = " + OnSelect);
                GUI.DrawTexture(rect, GUIColorTexture.GetOrCreateGradientTexture(GUIColorTexture.Crystal(new Color(0.406f, 0.425f, 0.476f, 0.9f))));
                Color borderColor = Color.white;
                int borderWidth = 1;
                GUI.DrawTexture(new Rect(rect.x, rect.y, rect.width, borderWidth), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                GUI.DrawTexture(new Rect(x + width - borderWidth, y, borderWidth, height), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                GUI.DrawTexture(new Rect(x - borderWidth, y , borderWidth, height), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                GUI.DrawTexture(new Rect(x, y + height - 2 + borderWidth, width, borderWidth), GUIColorTexture.GetOrCreateColorTexture(borderColor));
            }

            var style = new GUIStyle(GUI.skin.label);
            style.fontStyle = FontStyle.Bold;
            style.fontSize = 14;
            var guiContent = new GUIContent(DataType.Name);
            var size = style.CalcSize(guiContent);
            GUI.Label(new Rect(x + width / 2 - size.x / 2, 0, rect.width, rect.height), guiContent, style);
        }

        private float _LimitStartValue(float start)
        {
            return Mathf.Max(0, start);
        }

        #region external methods

        #endregion

        #region Internal methods

        private GUIStyle _BGStyle()
        {
            var style = new GUIStyle(GUI.skin.box);
            style.normal.background = GUIColorTexture.GetOrCreateGradientTexture(GUIColorTexture.Crystal(new Color(0.25f, 0.265f, 0.289f, 0.75f)));
            style.border = new RectOffset(1,1,1,1);
            return style;
        }

        private GUIStyle _OnSelectStyle()
        {
            var style = new GUIStyle(GUI.skin.box);
            style.normal.background = GUIColorTexture.GetOrCreateGradientTexture(GUIColorTexture.Crystal(new Color(0.406f, 0.425f, 0.476f, 0.9f)));
            style.border = new RectOffset();
            return style;
        }

        #endregion
    }
   
}
