﻿
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public class AnimationNodeEvent : INodeEvent
    {
        public bool IsEnteredNode { get; set; }
        public void OnStartPlay()
        {
            Debug.LogError("AnimationNodeEvent OnStartPlay");
        }

        public void OnEndPlay()
        {
            Debug.LogError("AnimationNodeEvent OnEndPlay");
        }

        public void ProcessFrame(float delta)
        {
            Debug.LogError("AnimationNodeEvent ProcessFrame delta = " + delta);
        }

        public void ProcessFrameWithWeight(float delta, float weight)
        {
            //Debug.LogError("ProcessFrameWithWeight delta = " + delta + " weight = " + weight);
        }
    }
}
