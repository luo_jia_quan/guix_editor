﻿using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public class BaseNodeEvent : INodeEvent
    {
        public bool IsEnteredNode { get; set; }

        public void OnStartPlay()
        {
            Debug.Log("On Node Start");
        }

        public void OnEndPlay()
        {
            Debug.Log("On Node End");
        }

        public void ProcessFrame(float delta)
        {
            Debug.Log("On Node Update， delta = " + delta);
        }

        public void ProcessFrameWithWeight(float delta, float weight)
        {
            
        }
    }
}
