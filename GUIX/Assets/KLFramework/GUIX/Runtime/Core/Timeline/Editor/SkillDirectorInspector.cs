﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using KLFramework.GUIX;
using KLFramework.GUIX.Timeline;
using KLFramework.GUIX.Timeline.Editor;
using SerializeExtend;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.KLFramework.GUIX
{
    [CustomEditor(typeof(SkillDirector))]
    public class SkillDirectorInspector : Editor
    {
        private List<string> _TypeListDesc = new List<string>();
        private ITimelineLayer _CurrentSelectedObjLayer;
        private string _Path;
        private int _SerializeType;

        void OnEnable()
        {
            _TypeListDesc.Clear();
            foreach (var serializeTypeInfo in TimelineSerializeConst.TypeList)
            {
                _TypeListDesc.Add(serializeTypeInfo._Type);
            }
        }

        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();

            var asset = target as SkillDirector;
            if(asset == null) return;

            serializedObject.Update();
            var timelineSourceProp = serializedObject.FindProperty("_Timeline");
            EditorGUILayout.PropertyField(timelineSourceProp);

            EditorGUILayout.Space(2);
            EditorGUILayout.LabelField("SerializeType");
            asset._SerializeType = EditorGUILayout.Popup(asset._SerializeType, _TypeListDesc.ToArray());
            _SerializeType = asset._SerializeType;

            if (asset._Timeline != null)
            {
                _Path = AssetDatabase.GetAssetPath(asset._Timeline);
                //deserialize
                if (TimelineSerializeConst.LastDeSerializeGuiTimeline != asset._Timeline)
                {
                    //Debug.LogError("TimelineSerializeConst.LastDeSerializeGuiTimeline != asset._Timeline");
                    TimelineSerializeConst.LastDeSerializeGuiTimeline = asset._Timeline;
                    TimelineSerializeConst.LastTimelineSerializeInfo = new TimelineSerializeInfo();
                    TimelineSerializeConst.LastTimelineSerializeInfo = TimelineSerialize.Deserialize(TimelineSerializeConst.LastTimelineSerializeInfo, TimelineSerializeConst.SERIALIZE_TYPE_JSON, _Path) as TimelineSerializeInfo;
                    var defaultController = new DefaultBindingController();
                    defaultController.Init(TimelineSerializeConst.LastTimelineSerializeInfo._BindingObjects);
                    TimelineSerializeConst.LastTimelineBindingController = defaultController;
                }

                //draw bindings property
                asset._ShowBindingsHeader = EditorGUILayout.BeginFoldoutHeaderGroup(asset._ShowBindingsHeader, "Bindings", null, null);
                if (asset._ShowBindingsHeader)
                {
                    float totalHeight = 0;
                    float paddingLastRectTop = 20f;
                    float selectIconWidth = 20f;
                    var lastRect = GUILayoutUtility.GetLastRect();
                    TimelineSerializeInfo serializeInfo = TimelineSerializeConst.LastTimelineSerializeInfo;
                    IBindingController controller = TimelineSerializeConst.LastTimelineBindingController;
                    for (int i = 0; i < serializeInfo._Layers.Count; i++)
                    {
                        ITimelineLayer layer = serializeInfo._Layers[i];
                        if (serializeInfo._LayersTypeDict.ContainsKey(layer))
                        {
                            Type layerType = serializeInfo._LayersTypeDict[layer];
                            var attribute = layerType.GetCustomAttribute<LayerBindingType>();
                            if (attribute == null) continue;
                            totalHeight += paddingLastRectTop;
                            Type bindingType = attribute.type;
                            //draw layer name
                            GUI.Label(new Rect(lastRect.x, lastRect.y + totalHeight, lastRect.width / 2, lastRect.height), string.Format("{0} Layer", layerType.Name));
                            Rect rect = new Rect(lastRect.x + lastRect.width / 2, lastRect.y + totalHeight, lastRect.width / 2 - selectIconWidth, lastRect.height);
                            var bindObject = controller.GetBindingObject(layer);
                            if (bindObject == null)
                            {
                                GUI.Label(rect,  string.Format("None ({0})", bindingType.Name));
                            }
                            else
                            {
                                GUI.Label(rect, bindObject.name);
                            }
                            //draw bounds
                            float borderWidth = 2f;
                            Color borderColor = Color.grey;
                            GUI.DrawTexture(new Rect(rect.x - borderWidth / 2, rect.y + borderWidth / 2, borderWidth, rect.height), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                            GUI.DrawTexture(new Rect(rect.x + rect.width - borderWidth / 2, rect.y + borderWidth / 2, borderWidth, rect.height), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                            GUI.DrawTexture(new Rect(rect.x, rect.y + borderWidth / 2, rect.width, borderWidth), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                            GUI.DrawTexture(new Rect(rect.x, rect.y + rect.height - borderWidth / 2, rect.width, borderWidth), GUIColorTexture.GetOrCreateColorTexture(borderColor));
                            //draw select icon
                            var texture = TimelineUtility.GetAssetTexture(TimelineConst.SELECT_ICON);
                            var btnRect = new Rect(lastRect.x + lastRect.width - selectIconWidth, lastRect.y + totalHeight, selectIconWidth, lastRect.height);
                            var style = new GUIStyle(GUI.skin.button);
                            style.padding = new RectOffset(0, 0, 0, 0);
                            if (GUI.Button(btnRect, texture, style))
                            {
                                if (bindingType != null)
                                {
                                    _CurrentSelectedObjLayer = layer;
                                    if (bindingType == typeof(GameObject))
                                    {
                                        EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, "", 0);
                                    }
                                    else if (bindingType == typeof(Animator))
                                    {
                                        EditorGUIUtility.ShowObjectPicker<Animator>(null, true, "", 0);
                                    }
                                }
                            }
                            if (Event.current.commandName == "ObjectSelectorUpdated")
                            {
                                if (_CurrentSelectedObjLayer != null && _CurrentSelectedObjLayer == layer)
                                {
                                    Object selectedObj = EditorGUIUtility.GetObjectPickerObject();
                                    GameObject gameObject = selectedObj as GameObject;
                                    AddToBindingObjects(layer, gameObject);
                                }
                            }
                            //DragAndDrop Event
                            _HandleDragAndDrop(rect, layer, bindingType);
                            
                        }
                    }
                    EditorGUILayout.Space(totalHeight);
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
            }
            serializedObject.ApplyModifiedProperties();
        }

        private void AddToBindingObjects(ITimelineLayer layer, GameObject gameObject)
        {
            TimelineSerializeConst.LastTimelineBindingController.AddBindingObject(layer, gameObject);
            _Serialize();
        }

        private void _Serialize()
        {
            TimelineSerialize.Serialize(TimelineSerializeConst.LastTimelineSerializeInfo, _SerializeType, _Path);
        }

        private void _HandleDragAndDrop(Rect fieldRect, ITimelineLayer layer, Type bindingType)
        {
            if (fieldRect.Contains(Event.current.mousePosition))
            {
                //select the binding gameobject
                if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
                {
                    //Selection.activeObject = _BindingController.GetBindingObject(layer);
                    Selection.activeObject = TimelineSerializeConst.LastTimelineBindingController.GetBindingObject(layer);
                }
                if (Event.current.type == EventType.DragUpdated)
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    Event.current.Use();
                }
                else if (Event.current.type == EventType.DragPerform)
                {
                    if (DragAndDrop.objectReferences.Length > 0)
                    {
                        Func<GameObject, Type, bool> canAddFunc = (obj, type) =>
                        {
                            if (obj == null) return false;
                            //is instance in hierarchy
                            var gameObjects = GameObjectFindUtils.GetAllGameObjects(EditorSceneManager.GetActiveScene());
                            bool isFind = false;
                            for (int i = 0; i < gameObjects.Count; i++)
                            {
                                if (obj == gameObjects[i])
                                {
                                    isFind = true;
                                    break;
                                }
                            }
                            if (!isFind) return false;

                            Type objType = obj.GetType();
                            if (objType == type)
                            {
                                return true;
                            }
                            if (obj.GetComponent(type) != null)
                            {
                                return true;
                            }
                            return false;
                        };
                        GameObject gameObject = DragAndDrop.objectReferences[0] as GameObject;
                        bool isCanAdd = canAddFunc(gameObject, bindingType);
                        if (isCanAdd)
                        {
                            AddToBindingObjects(layer, gameObject);
                        }
                    }
                    else if (DragAndDrop.objectReferences.Length > 1)
                    {
                        Debug.LogError("can not drag multiple objects in one assignment!!!");
                    }
                    Event.current.Use();
                }
            }
        }

    }
}
