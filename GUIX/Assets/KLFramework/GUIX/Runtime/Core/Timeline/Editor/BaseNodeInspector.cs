﻿using System.Collections.Generic;
using System.Reflection;
using KLFramework.GUIX.Layout;
using KLFramework.GUIX.Timeline.Editor;
using UnityEditor;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{

    [CustomEditor(typeof(Node))]
    public class BaseNodeInspector : UnityEditor.Editor
    {
        private IUI _UI;
        private float _NumberTextFieldPrecision;

        public BaseNodeInspector()
        {
        }

        public override void OnInspectorGUI()
        {
            if (TimelineGUIs.CurrentTimeline == null) return;
            _NumberTextFieldPrecision = 1f / TimelineGUIs.CurrentTimeline.GetFrameRate();

            var node = target as Node;
            if (node == null)
                return;

            IUI ui = _GetOrCreate();
            if (node.GetNodeRenderer().IsRepaintInspector)
            {
                node.GetNodeRenderer().IsRepaintInspector = false; 
                _UI = null;
            }

            bool repaint = false;
            var nodeHeaderArea = ui.Render(0, 0, ref repaint);
            if (repaint)
            {
                Repaint();
            }

            var obj = new SerializedObject(target);
            obj.Update();

            var data = obj.FindProperty("_Data");
            var dataValue = target.GetType().GetField("_Data", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(target) as ScriptableObject;

            GUILayout.BeginArea(new Rect(5, 10 + nodeHeaderArea.y + nodeHeaderArea.height, Screen.width - 5, Screen.height - 10 - (nodeHeaderArea.y + nodeHeaderArea.height)));
            var dataValueEditor = UnityEditor.Editor.CreateEditor(dataValue);
            dataValueEditor.OnInspectorGUI();
            //EditorGUILayout.PropertyField(data, true);
            //EditorGUILayout.ObjectField(data);
            GUILayout.EndArea();
            GUILayout.Space(EditorGUIUtility.currentViewWidth);

            if (obj.ApplyModifiedProperties())
            {
                EditorUtility.SetDirty(node);
            }
        }

        private NumberTextField _StartFrameInput;
        private NumberTextField _StartSecondInput;
        private NumberTextField _EndFrameInput;
        private NumberTextField _EndSecondInput;
        private NumberTextField _DurationFrameInput;
        private NumberTextField _DurationSecondInput;

        private IUI _GetOrCreate()
        {
            if (_UI != null)
            {
                return _UI;
            }

            var node = target as Node;
            // Draw time & frame
            var panel = new Panel(new FlowLayout(FlowLayout.Direction.Vertical, 5)).SetPadding(Direction.TOP, 10).SetPadding(Direction.LEFT, 10);
            //start
            panel.AddUI(new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 10))
                .AddUI(new Label("Time Start").SetSizeAndReturn(100, -1).SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(new Label("s").SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(_StartSecondInput = new NumberTextField(_NumberTextFieldPrecision, 0).SetSizeAndReturn(120, -1).SetText(node.GetNodeRenderer().GetStart() + "")
                    .AddChangeEvent((tf, val) =>
                    {
                        var duration = node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart();
                        node.GetNodeRenderer().SetStartAndEnd(val, val + duration);

                        _StartFrameInput.SetText(TimelineUtility.TimeConvertToFrames(val, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                        _EndSecondInput.SetText(val + duration);
                        _EndFrameInput.SetText(TimelineUtility.TimeConvertToFrames(val + duration, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                    }))
                .AddUI(new Label("f").SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(_StartFrameInput = new NumberTextField(1, 0).SetSizeAndReturn(120, -1).SetText(TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetStart(), TimelineGUIs.CurrentTimeline.GetFrameRate()) + "")
                    .AddChangeEvent((tf, v) =>
                    {
                        int val = Mathf.FloorToInt(v);
                        var duration = node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart();
                        float time = TimelineUtility.FramesConvertToTime(val, TimelineGUIs.CurrentTimeline.GetFrameRate());
                        node.GetNodeRenderer().SetStartAndEnd(time, time + duration);

                        _StartSecondInput.SetText(time + "");
                        _EndSecondInput.SetText((time + duration) + "");
                        _EndFrameInput.SetText(TimelineUtility.TimeConvertToFrames(time + duration, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                    })));
            //end
            panel.AddUI(new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 10))
                .AddUI(new Label("Time End").SetSizeAndReturn(100, -1).SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(new Label("s").SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(_EndSecondInput = new NumberTextField(_NumberTextFieldPrecision, node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart()).SetSizeAndReturn(120, -1).SetText(node.GetNodeRenderer().GetEnd() + "")
                    .AddChangeEvent((tf, v) =>
                    {
                        var duration = node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart();
                        node.GetNodeRenderer().SetStartAndEnd(v - duration, v);

                        _EndFrameInput.SetText(TimelineUtility.TimeConvertToFrames(v, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                        _StartSecondInput.SetText((v - duration) + "");
                        _StartFrameInput.SetText(TimelineUtility.TimeConvertToFrames(v - duration, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                    }))
                .AddUI(new Label("f").SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(_EndFrameInput = new NumberTextField(1, 1f * TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart(), TimelineGUIs.CurrentTimeline.GetFrameRate())).SetSizeAndReturn(120, -1)
                    .SetText(TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetEnd(), TimelineGUIs.CurrentTimeline.GetFrameRate()) + "")
                    .AddChangeEvent((tf, v) =>
                    {
                        var val = Mathf.FloorToInt(v);
                        var duration = node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart();
                        float time = TimelineUtility.FramesConvertToTime(val, TimelineGUIs.CurrentTimeline.GetFrameRate());
                        node.GetNodeRenderer().SetStartAndEnd(time - duration, time);

                        _EndSecondInput.SetText(time);
                        _StartSecondInput.SetText((time - duration) + "");
                        _StartFrameInput.SetText(TimelineUtility.TimeConvertToFrames(time - duration, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                    })));
            //Duration
            panel.AddUI(new Panel(new FlowLayout(FlowLayout.Direction.Horizontal, 10))
                .AddUI(new Label("Duration").SetSizeAndReturn(100, -1).SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(new Label("s").SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(_DurationSecondInput = new NumberTextField(_NumberTextFieldPrecision, 1f / TimelineGUIs.CurrentTimeline.GetFrameRate()).SetSizeAndReturn(120, -1).SetText((node.GetNodeRenderer().GetEnd() - node.GetNodeRenderer().GetStart()) + "")
                    .AddChangeEvent((tf, v) =>
                    {
                        var start = node.GetNodeRenderer().GetStart();
                        node.GetNodeRenderer().SetStartAndEnd(start, start + v);
                        _EndFrameInput.Min(TimelineUtility.TimeConvertToFrames(v, TimelineGUIs.CurrentTimeline.GetFrameRate()));

                        _DurationFrameInput.SetText(TimelineUtility.TimeConvertToFrames(v, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                        _EndSecondInput.SetText((start + v) + "");
                        _EndFrameInput.SetText(TimelineUtility.TimeConvertToFrames(start + v, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                    }))
                .AddUI(new Label("f").SetBorderColor(Color.gray).SetBorder(1))
                .AddUI(_DurationFrameInput = new NumberTextField(1, 1).SetSizeAndReturn(120, -1).SetText((TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetEnd(), TimelineGUIs.CurrentTimeline.GetFrameRate()) -
                            TimelineUtility.TimeConvertToFrames(node.GetNodeRenderer().GetStart(), TimelineGUIs.CurrentTimeline.GetFrameRate()) + ""))
                    .AddChangeEvent((tf, v) =>
                    {
                        var val = Mathf.FloorToInt(v);
                        var start = node.GetNodeRenderer().GetStart();
                        float time = TimelineUtility.FramesConvertToTime(val, TimelineGUIs.CurrentTimeline.GetFrameRate());
                        node.GetNodeRenderer().SetStartAndEnd(start, start + time);

                        _DurationSecondInput.SetText(time + "");
                        _EndSecondInput.Min(time);
                        _EndFrameInput.Min(TimelineUtility.TimeConvertToFrames(start + time, TimelineGUIs.CurrentTimeline.GetFrameRate()) * 1f);
                        _EndSecondInput.SetText((start + time) + "");
                        _EndFrameInput.SetText(TimelineUtility.TimeConvertToFrames(start + time, TimelineGUIs.CurrentTimeline.GetFrameRate()) + "");
                    })));

            _UI = panel;
            return panel;
        }
    }
}