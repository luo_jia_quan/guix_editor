﻿using System.Collections.Generic;
using KLFramework.GUIX.Timeline;
using UnityEditor;
using UnityEngine;

namespace Assets.KLFramework.GUIX
{
    [CustomEditor(typeof(Timeline))]
    public class TimelineInspector : Editor
    {
        private Timeline _Timeline;


        public override void OnInspectorGUI()
        {
            _Timeline = Timeline.SelectedTimeline = target as Timeline;
            //Debug.Log(string.Format("{0} - {1}", _Timeline, TimelineGUIs.CurrentTimeline));
            if (_Timeline == null) return;

            if (TimelineGUIs.CurrentTimeline == null)
            {
                return;
            }

            var selectedNodes = TimelineGUIs.CurrentTimeline.GetSelectedNodes();
            if (selectedNodes == null)
            {

            }
            else
            {
                _DrawSelectedNodes(selectedNodes);
            }
        }

        private void _DrawSelectedNodes(List<Node> selectedNodes)
        {
            if (selectedNodes.Count == 0) return;

            //selectedNodes[0].GetInspectorRenderer().OnInspectorGUI();
        }
    }
}
