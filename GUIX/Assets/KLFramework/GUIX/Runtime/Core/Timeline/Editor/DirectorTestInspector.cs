﻿using KLFramework.GUIX.Timeline;
using UnityEditor;
using UnityEngine;

namespace Assets.KLFramework.GUIX
{
    [CustomEditor(typeof(DirectorTest))]
    public class DirectorTestInspector : Editor
    {
        bool toggleGroup;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            string commandName = Event.current.commandName;

            toggleGroup = EditorGUILayout.BeginToggleGroup("Group", toggleGroup);
            if (toggleGroup)
            {
                toggleGroup = false;
                //为控件获取一个唯一ID
                int controlID = EditorGUIUtility.GetControlID(FocusType.Passive);
                //显示对象选择窗口
                EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, "", controlID);
                GameObject selectObj = EditorGUIUtility.GetObjectPickerObject() as GameObject;
                Debug.Log(selectObj);
            }
            EditorGUILayout.EndToggleGroup();

            if (commandName == "ObjectSelectorUpdated")
            {
                Debug.Log(commandName);
                GameObject selectObj = EditorGUIUtility.GetObjectPickerObject() as GameObject;
                Debug.Log("selectObj = " + selectObj);
            }
            else if (commandName == "ObjectSelectorClosed")
            {
                Debug.Log(commandName);
            }

            if (EditorGUI.EndChangeCheck())
                serializedObject.ApplyModifiedProperties();
        }
    }
}
