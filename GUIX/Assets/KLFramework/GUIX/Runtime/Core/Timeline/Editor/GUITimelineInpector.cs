﻿
using UnityEditor;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    [CustomEditor(typeof(GUITimeline))]
    public class GUITimelineInpector : UnityEditor.Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var asset = target as GUITimeline;
            if(asset == null) return;

            if (GUILayout.Button("Debug Log"))
            {
                
            }

        }
    }
}
