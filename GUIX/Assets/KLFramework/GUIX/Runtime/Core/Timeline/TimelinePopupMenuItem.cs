﻿using System;

namespace KLFramework.GUIX.Timeline
{
    public class TimelinePopupMenuItem
    {
        public class Context
        {
            public int CurrentSelectLayerIndex;
            public int[] CurrentSelectLayerIndexes;

            public Context(int currentSelectLayerIndex, int[] currentSelectLayerIndexes)
            {
                CurrentSelectLayerIndex = currentSelectLayerIndex;
                CurrentSelectLayerIndexes = currentSelectLayerIndexes;
            }
        }

        public readonly bool Separator;
        public readonly string Menu;
        public readonly string Shortcut;
        public readonly Action<Context> Action;
        public readonly Func<bool> Enable;

        public TimelinePopupMenuItem()
        {
            Separator = true;
        }

        public TimelinePopupMenuItem(string menu, Action<Context> action, Func<bool> enable, string shortcut = null)
        {
            Menu = menu;
            Action = action;
            Enable = enable ?? AlwaysEnable;
            Separator = false;
            Shortcut = shortcut;
        }

        private bool AlwaysEnable()
        {
            return true;
        }
    }
}