﻿using System;
using System.Collections.Generic;

namespace KLFramework.GUIX.Timeline
{
    public interface ITimeline : IUI
    {
        int GetFrameRate();

        List<Node> GetSelectedNodes();

        List<Node> GetAllNodes();

        void AddLayer(Type scriptableObjectType, int index);

        void RemoveLayer(int index);

        void SetCurrentFrameIndex(int frameIndex);

        int GetCurrentFrameIndex();

        void Destroy();
    }

    public class TimelineGUIs
    {
        public static ITimeline CurrentTimeline;
        public static Object CurrentAsset;
    }
}
