﻿using SerializeExtend;

namespace KLFramework.GUIX.Timeline
{
    public interface INodeEvent
    {
        bool IsEnteredNode { get; set; }

        void OnStartPlay();

        void OnEndPlay();

        /// <summary>
        /// Only Execute In Node Time
        /// </summary>
        /// <param name="delta"></param>
        void ProcessFrame(float delta);

        /// <summary>
        /// Execute In Whole Time
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="weight"></param>
        void ProcessFrameWithWeight(float delta, float weight);
    }
}
