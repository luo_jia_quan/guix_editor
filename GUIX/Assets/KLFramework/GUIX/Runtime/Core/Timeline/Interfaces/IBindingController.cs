﻿
using System.Collections.Generic;
using SerializeExtend;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public interface IBindingController
    {
        Object GetBindingObject(ITimelineLayer layer);

        void AddBindingObject(ITimelineLayer layer, Object obj);
    }

    public class DefaultBindingController : IBindingController
    {
        private Dictionary<ITimelineLayer, GameObjectInfo> _BindingObjects;
        private Dictionary<Object, GameObjectInfo> _Object2Info = new Dictionary<Object, GameObjectInfo>();
        private Dictionary<GameObjectInfo, Object> _Info2Object = new Dictionary<GameObjectInfo, Object>();

        public void Init(Dictionary<ITimelineLayer, GameObjectInfo> bindingObjects)
        {
            _BindingObjects = bindingObjects;
            foreach (var bind in _BindingObjects)
            {
                GameObject bindingObject = null;
                if (bind.Value.IsPrefab)
                {
#if UNITY_EDITOR
//                    var prefabObj = AssetDatabase.LoadAssetAtPath<GameObject>(bind.Value.Path);
//                    //var cloneObj = GameObject.Instantiate(prefabObj);
//                    bindingObject = GameObjectFindUtils.Find(prefabObj.transform, bind.Value.Name);
                    
                    GameObject instance = GetPrefabInstanceObject(bind.Value.Path, bind.Value.Name);
                    bindingObject = instance;
#endif
                }
                else
                {
                    //todo find object with path
                    bindingObject = GameObject.Find(bind.Value.Path);
                }

                if (bindingObject == null)
                {
                    Debug.LogWarning("missing BindingObject, isPrefab = " + bind.Value.IsPrefab + " Path = " + bind.Value.Path + " name = " + bind.Value.Name);
                    continue;
                }

                if (!_Object2Info.ContainsKey(bindingObject))
                {
                    _Object2Info.Add(bindingObject, bind.Value);
                }
                if (!_Info2Object.ContainsKey(bind.Value))
                {
                    _Info2Object.Add(bind.Value, bindingObject);
                }
            }

        }

        public Object GetBindingObject(ITimelineLayer layer)
        {
            if (_BindingObjects != null && _BindingObjects.ContainsKey(layer))
            {
                GameObjectInfo info = _BindingObjects[layer];
                if (_Info2Object.ContainsKey(info))
                {
                    return _Info2Object[info];
                }
            }
            return null;
        }

        public void AddBindingObject(ITimelineLayer layer, Object obj)
        {
            if (obj == null)
            {
                SetLayerBindings(layer, null);
                return;
            }
            if (!_Object2Info.ContainsKey(obj))
            {
                GameObjectInfo info = null;
                if (PrefabUtility.IsPartOfAnyPrefab(obj))
                {
                    Object prefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(obj);
                    string path = AssetDatabase.GetAssetPath(prefab);
                    info = new GameObjectInfo(path, true, obj.name);
                    _Object2Info.Add(obj, info);
                    _Info2Object.Add(info, obj);
                    SetLayerBindings(layer, info);
                }
                else
                {
                    GameObject gameObject = obj as GameObject;
                    if (gameObject != null)
                    {
                        string scenePath = gameObject.transform.GetFullHierarchyPath();
                        info = new GameObjectInfo(scenePath, false, obj.name);
                        _Object2Info.Add(obj, info);
                        _Info2Object.Add(info, obj);
                        SetLayerBindings(layer, info);
                    }
                    else
                    {
                        Debug.LogError("obj's Type is not GameObject!!!");
                    }
                }
            }
            else
            {
                SetLayerBindings(layer, _Object2Info[obj]);
            }
        }

        private void SetLayerBindings(ITimelineLayer layer, GameObjectInfo info)
        {
            if (!_BindingObjects.ContainsKey(layer))
            {
                _BindingObjects.Add(layer, info);
            }
            else
            {
                _BindingObjects[layer] = info;
            }
        }

        private GameObject GetPrefabInstanceObject(string path, string name)
        {
            if (string.IsNullOrEmpty(path)) return null;
            var gameObjects = GameObjectFindUtils.GetAllGameObjects(EditorSceneManager.GetActiveScene());
            GameObject resultGameObject = null;
            for (int i = 0; i < gameObjects.Count; i++)
            {
                GameObject o = gameObjects[i];
                if (PrefabUtility.IsPartOfPrefabInstance(o))
                {
                    var sourcePrefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(o);
                    string assetPath = AssetDatabase.GetAssetPath(sourcePrefab);
                    if(string.IsNullOrEmpty(assetPath)) continue;
                    if (assetPath == path)
                    {
                        if (PrefabUtility.IsAnyPrefabInstanceRoot(o) && o.name.StartsWith(name))
                        {
                            resultGameObject = o;
                        }
                        else
                        {
                            resultGameObject = GameObjectFindUtils.FindChildByName(o.transform, name).gameObject;
                        }
                        break;
                    }
                }
            }
            //Create a Instantiate object
            if (resultGameObject == null)
            {
                var prefabObj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                var cloneObj = GameObject.Instantiate(prefabObj);
                resultGameObject = GameObjectFindUtils.Find(name);
                if (resultGameObject == null)
                {
                    string cloneName = name + " (clone)";
                    resultGameObject = GameObjectFindUtils.Find(cloneName);
                }
            }

            return resultGameObject;
        }

    }

}
