﻿using System;
using System.Collections.Generic;

namespace KLFramework.GUIX.Timeline
{
    public interface ITimelineLayer
    {
        object BindingObject { get; set; }

        void AddNode(Node node);

        void RemoveNode(Node node);

        List<Node> GetAllNodes();

        void OnDrawGUI(float x, float y, float width, float height, bool onSelect);

    }
}
