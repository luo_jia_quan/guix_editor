﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SerializeExtend;

namespace KLFramework.GUIX.Timeline
{
    public class TimelineSerializeInfo 
    {
        [CustomSerializeField]
        public List<ITimelineLayer> _Layers = new List<ITimelineLayer>();
        [CustomSerializeField]
        public Dictionary<ITimelineLayer, Type> _LayersTypeDict = new Dictionary<ITimelineLayer, Type>();
        [CustomSerializeField]
        public Dictionary<ITimelineLayer, GameObjectInfo> _BindingObjects = new Dictionary<ITimelineLayer, GameObjectInfo>();
    }
}
