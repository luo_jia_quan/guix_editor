﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    [ExecuteInEditMode]
    public class SkillDirector : MonoBehaviour
    {
        public GUITimeline _Timeline;
        public bool _ShowBindingsHeader;
        [SerializeField]
        [HideInInspector]
        public int _SerializeType = TimelineSerializeConst.SERIALIZE_TYPE_JSON;

        [SerializeField]
        public Dictionary<string, GameObject> _BindingsSerializeDict;

        void OnEnable()
        {
        }
        
    }

    public class TimelineSerializeConst
    {
        public const int SERIALIZE_TYPE_JSON = 0;
        public const int SERIALIZE_TYPE_BINARY = 1;

        public static List<SerializeTypeInfo> TypeList = new List<SerializeTypeInfo>()
        {
            new SerializeTypeInfo(SERIALIZE_TYPE_JSON, "Json"),
            new SerializeTypeInfo(SERIALIZE_TYPE_BINARY, "Binary"),
        };

        public static GUITimeline LastDeSerializeGuiTimeline;
        public static TimelineSerializeInfo LastTimelineSerializeInfo;
        public static IBindingController LastTimelineBindingController;
    }

    public class SerializeTypeInfo
    {
        public int _Id;
        public string _Type;

        public SerializeTypeInfo(int id, string type)
        {
            _Id = id;
            _Type = type;
        }
    }

}
