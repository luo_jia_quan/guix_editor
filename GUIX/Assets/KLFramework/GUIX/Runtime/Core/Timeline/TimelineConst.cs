﻿namespace KLFramework.GUIX.Timeline
{
    public static class TimelineConst
    {
        public const int TITLE_HEIGHT = 20;                     //标题高度
        public const int LEFT_PANEL_MIN_WIDTH = 250;            //左侧面板最小宽度
        public const int TIME_RULER_HEIGHT = 30;                //上方刻度尺区域高度
        public const int LAYER_HEIGHT = 30;                     //层高度
        public const int LAYER_INTERVAL = 10;                     //层间距
        public const int SCROLL_RECT_HEIGHT = 20;               //水平滑动条高度
        public const int SCROLL_ZOOM_HANDLE_SIZE = 10;          //滑动条缩放区域宽度
        public const float MOUSE_WHEEL_PRECISION = 0.01f;       //鼠标中键精度
        public const float MOUSE_DRAG_PRECISION = 1f;           //鼠标左键点击拖拽精度
        public const int BASE_FRAME_WIDTH = 1;                  //每帧占用宽度
        public const int BASE_FRAME_PADDING = 40;               //每帧间距
        public const int TIME_POINTER_WIDTH = 1;                //刻度指针宽度
        public const int DEFAULT_TOTAL_FRAMES = 200;            //默认显示的总帧数
        public const int DEFAULT_FRAME_RATE = 30;               //默认帧率
        public const int NODE_BORDER_DETECTION_WIDTH = 5;       //node边界检测宽度
        public const float MIN_RULER_SCALE = 0.001f;           //最小缩放值


        //icons
        public const string MOUSE_DRAG_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/cursor.png";
        public const string FORE_FRONT_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/forefront.png";
        public const string FRONT_FRAME_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/frontFrame.png";
        public const string PLAY_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/play.png";
        public const string BEHINE_FRAME_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/behineFrame.png";
        public const string FINAL_FRAME_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/final.png";
        public const string AREA_PLAY_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/areaPlay.png";
        public const string SELECT_ICON = "Assets/KLFramework/GUIX/Runtime/Core/Timeline/Assets/Icons/select.png";

    }
}
