﻿using System;

namespace KLFramework.GUIX.Timeline
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class NodeEventType : Attribute
    {

        public readonly Type EventType;

        public NodeEventType(Type eventType)
        {
            EventType = eventType;
        }
    }
}
