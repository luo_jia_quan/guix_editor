﻿using System;

namespace KLFramework.GUIX.Timeline
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LayerBindingType : Attribute
    {
        public readonly Type type;

        public LayerBindingType(Type type)
        {
            this.type = type;
        }
    }
}
