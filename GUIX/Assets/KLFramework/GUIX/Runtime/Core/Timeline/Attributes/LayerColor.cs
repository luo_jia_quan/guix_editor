﻿using System;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LayerColor : Attribute
    {
        public Color _Color;

        public LayerColor(float r, float g, float b, float a = 1f)
        {
            this._Color = new Color(r, g, b, a);
        }
    }
}
