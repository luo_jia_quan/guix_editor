﻿using System;
using System.IO;
using SerializeExtend;
using UnityEditor;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public class TimelineSerialize
    {

        public static void Serialize(object obj, int type, string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogError("serialize path is empty!");
                return;
            }
            path = path.Substring(0, path.LastIndexOf("."));
            switch (type)
            {
                case TimelineSerializeConst.SERIALIZE_TYPE_BINARY:
                    path = path + ".bin";
                    CheckPathExist(path);
                    break;
                case TimelineSerializeConst.SERIALIZE_TYPE_JSON:
                    path = path + ".json";
                    CheckPathExist(path);
                    _JsonSerialize(obj, type, path);
                    break;
                default:
                    break;
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private static void CheckPathExist(string path)
        {
            if (!File.Exists(path))
            {
                FileStream fs = File.Create(path);
                fs.Close();
            }
        }


        public static object Deserialize(object obj, int serializeType, string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogError("deserialize path is empty!");
                return null;
            }
            path = path.Substring(0, path.LastIndexOf("."));
            object o = null;
            switch (serializeType)
            {
                case TimelineSerializeConst.SERIALIZE_TYPE_BINARY:

                    break;
                case TimelineSerializeConst.SERIALIZE_TYPE_JSON:
                    path = path + ".json";
                    o = _JsonDeserialize(obj, path);
                    break;

                default:
                    break;
            }
            return o;
        }

        private static void _JsonSerialize(object obj, int type, string path)
        {
            //string json = JsonUtility.ToJson(obj, true);

            object json = new JsonSerializer().Serialize(obj);
            //Debug.LogError(json);
            //写入文件
            File.WriteAllText(path, json.ToString());
        }

        private static object _JsonDeserialize(object obj, string path)
        {
            if (!File.Exists(path))
            {
                Debug.LogWarning("file do not exists, path = " + path);
                return obj;
            }

            //JsonUtility.FromJson can not deserialize inherit from MonoBehaviour or ScriptableObject
            string json = File.ReadAllText(path);
            var o = new JsonDeSerializer().DeSerialize(json, obj.GetType());
            return o;
        }

    }
}
