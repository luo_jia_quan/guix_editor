﻿using System.Collections.Generic;
using SerializeExtend;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public class BaseLayer : ITimelineLayer
    {

        public object BindingObject { get; set; }

        [CustomSerializeField]
        [Reflections.CloneField]
        private List<Node> _Nodes = new List<Node>();

        public BaseLayer()
        {
        }

        public BaseLayer(List<Node> nodes)
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                Node node = nodes[i];
                Node newNode = new Node();
                newNode.SetNodeRenderer(node.GetNodeRenderer());
                newNode.SetData(node.GetData());
                newNode.SetNodeEvent(node.GetNodeEvent());
                _Nodes.Add(newNode);
            }
        }

        public void AddNode(Node node)
        {
            if (_Nodes.Contains(node))
            {
                Debug.LogError("BaseLayer add node ,node already contains!!");
            }
            _Nodes.Add(node);
        }

        public void RemoveNode(Node node)
        {
            _Nodes.Remove(node);
        }

        public List<Node> GetAllNodes()
        {
            return _Nodes;
        }

        public void OnDrawGUI(float x, float y, float width, float height, bool onSelect)
        {
            GUI.DrawTexture(new Rect(x, y + 1, width, height - TimelineConst.LAYER_INTERVAL / 2), GUIColorTexture.GetOrCreateColorTexture(new Color(0.195f, 0.195f, 0.195f, 1)));
            if (onSelect)
            {
                GUI.DrawTexture(new Rect(x, y + 1, width, height - TimelineConst.LAYER_INTERVAL / 2), GUIColorTexture.GetOrCreateColorTexture(new Color(0.21f, 0.25f, 0.324f, 1f)));
            }
        }

    }
}
