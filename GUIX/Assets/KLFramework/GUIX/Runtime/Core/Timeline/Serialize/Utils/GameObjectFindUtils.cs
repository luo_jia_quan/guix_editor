﻿using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameObjectFindUtils
{

    public static List<GameObject> GetAllGameObjects(Scene scene)
    {
        List<GameObject> list = new List<GameObject>();
        GameObject[] objects = scene.GetRootGameObjects();
        for (int i = 0; i < objects.Length; i++)
        {
            GetAllGameObjects(objects[i], list);
        }
        return list;
    }

    public static void GetAllGameObjects(GameObject obj, List<GameObject> list)
    {
        if (obj != null)
        {
            list.Add(obj);
            for (int i = 0; i < obj.transform.childCount; i++)
            {
                Transform tran = obj.transform.GetChild(i);
                if (tran != null)
                {
                    GetAllGameObjects(tran.gameObject, list);
                }
            }
        }
    }

    public static GameObject Find(string goName, Scene scene)
    {
        GameObject[] goes = GetSceneRootGameObjects(scene);
        foreach (var gameObject in goes)
        {
            var found = Find(gameObject.transform, goName);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    public static GameObject Find(string goName)
    {
        GameObject[] goes = GetSceneRootGameObjects(SceneManager.GetActiveScene());
        foreach (var gameObject in goes)
        {
            var found = Find(gameObject.transform, goName);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    public static GameObject Find(Transform transform, string name)
    {
        if (transform.name.Equals(name))
        {
            return transform.gameObject;
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            var go = Find(transform.GetChild(i), name);
            if (go != null)
            {
                return go;
            }
        }
        return null;
    }

    public static Transform FindChildByName(Transform root, string name)
    {
        for (int i = 0; i < root.childCount; i++)
        {
            Transform child = root.GetChild(i);
            if (child.name == name)
                return child;
            if (child.childCount > 0)
            {
                child = FindChildByName(child, name);
                if (child != null)
                    return child;
            }
        }
        return null;
    }

    public static void Find<T>(List<T> list, Transform transform) where T : Component
    {
        if (transform.GetComponent<T>() != null)
        {
            list.Add(transform.GetComponent<T>());
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            Find<T>(list, transform.GetChild(i));
        }
    }

    public static List<T> Find<T>() where T : Component
    {
        GameObject[] goes = GetSceneRootGameObjects(SceneManager.GetActiveScene());
        var ts = new List<T>();
        foreach (var gameObject in goes)
        {
            Find<T>(ts, gameObject.transform);
        }
        return ts;
    }

    public static List<T> Find<T>(Scene scene) where T : Component
    {
        GameObject[] goes = GetSceneRootGameObjects(scene);
        var ts = new List<T>();
        foreach (var gameObject in goes)
        {
            Find<T>(ts, gameObject.transform);
        }
        return ts;
    }
 
    public static GameObject[] GetSceneRootGameObjects(Scene scene)
    {
        GameObject[] goes = null;
#if UNITY_EDITOR
        if (Application.isPlaying)
        {
            //goes = SceneManager.GetActiveScene().GetRootGameObjects();
            goes = scene.GetRootGameObjects();
        }
        else
        {
            //goes = EditorSceneManager.GetActiveScene().GetRootGameObjects();
            goes = scene.GetRootGameObjects();
        }
#else
        goes = scene.GetRootGameObjects();
#endif
        return goes;
    }

}
