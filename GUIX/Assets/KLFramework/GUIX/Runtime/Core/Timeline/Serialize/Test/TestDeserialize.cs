﻿
using System.IO;
using System.Net;
using KLFramework.GUIX.Timeline;
using SerializeExtend;
using UnityEditor;
using UnityEngine;

public class TestDeserialize
{

    [MenuItem("CustomSerialize/TestDeserialize")]
    public static void Test_Deserialize()
    {
        string path = "Assets/SkillEditorWindow/Editor/Base/Node/123.json";
        string json = File.ReadAllText(path);

        GUITimeline timeline = new JsonDeSerializer().DeSerialize<GUITimeline>(json);
        Debug.LogError("timeline = " + timeline);
    }

}
