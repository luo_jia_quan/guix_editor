﻿using System;

namespace SerializeExtend
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class CustomSerializeField : Attribute
    {
        public string KeyName;

    }
}
