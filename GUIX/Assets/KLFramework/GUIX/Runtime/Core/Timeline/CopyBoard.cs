﻿using System;
using System.Collections.Generic;
using KLFramework.Utils;

namespace KLFramework.GUIX.Timeline
{
    public class CopyBoard
    {
        [Reflections.CloneField]
        public static List<CopyNodeInfo> copyNodes = new List<CopyNodeInfo>();

        public static List<int> copyNodesLayersList = new List<int>();  //辅助拷贝多层的节点判断

        [Reflections.CloneField]
        public static List<CopyLayerInfo> copyLayers = new List<CopyLayerInfo>();
    }

    public class CopyNodeInfo
    {
        [Reflections.CloneField]
        public Node Node;
        [Reflections.CloneField]
        public float Duration;
        [Reflections.CloneField]
        public bool IsFirstNode;
        [Reflections.CloneField]
        public float DiffBetweenFirstNode;
    }

    public class CopyLayerInfo
    {
        [Reflections.CloneField]
        public ITimelineLayer layer;
        [Reflections.CloneField]
        public Type scriptableObjectType;
    }

}
