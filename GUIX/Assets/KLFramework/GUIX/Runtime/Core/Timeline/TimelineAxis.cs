﻿using KLFramework.GUIX.Timeline.Editor;
using UnityEngine;

namespace KLFramework.GUIX.Timeline
{
    public class TimelineAxis
    {
        private float _currentTime;
        private float _currentFrame;

        public void SetCurrentFrame(int frame)
        {
            _currentFrame = frame;
            _currentTime = TimelineUtility.FramesConvertToTime(frame, TimelineGUIs.CurrentTimeline.GetFrameRate());
        }

        public void Update(float delta)
        {

        }

    }
}
