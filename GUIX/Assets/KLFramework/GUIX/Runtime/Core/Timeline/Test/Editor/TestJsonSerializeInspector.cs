﻿using System;
using System.Collections.Generic;
using KLFramework.GUIX.Timeline;
using UnityEditor;
using UnityEngine;

//[ExecuteInEditMode]
[CustomEditor(typeof(TestJsonSerialize))]
public class TestJsonSerializeInspector : Editor //: MonoBehaviour
{
    private string _Json = null;
    private TestJsonSerializeWrapper _JsonSerializeWrapper;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TestJsonSerialize asset = target as TestJsonSerialize;
        if(asset == null) return;

        if (GUILayout.Button("Json Serialize"))
        {

            //Dictionary<int, string> tempDict = new Dictionary<int, string>();
            //tempDict.Add(1000, "JHU");
            //tempDict.Add(2000, "724Luo");
            //asset._SerializationDictionary = new SerializationDictionary<int, string>(tempDict);

            //string json = JsonUtility.ToJson(asset._SerializationDictionary);
            //Debug.LogError("Dictionary ToJson = " + json);
            ////Dictionary<int, string> tempDictionary = new Dictionary<int, string>();
            ////JsonUtility.FromJsonOverwrite(toJson, tempDictionary);

            //TestJsonSerialize tempDictionary = JsonUtility.FromJson<TestJsonSerialize>(json);
            //foreach (var info in tempDictionary._SerializationDictionary.ToDictionary())
            //{
            //    Debug.LogError("info.Key = " + info.Key + " info.value = " + info.Value);
            //}

            Debug.LogError("++++++++++++++++++++++++++++++++++++++++++++++++++++");

            Dictionary<int, string> temDictionary = new Dictionary<int, string>();
            temDictionary.Add(250, "Quan");
            asset._SampleDictionary = new SampleDictionary(temDictionary);

            string toJson = JsonUtility.ToJson(asset);
            Debug.LogError("toJson = " + toJson);

            //TestJsonSerialize tjs = JsonUtility.FromJson<TestJsonSerialize>(toJson);
            //tjs.Output();

            TestJsonSerialize serialize = new TestJsonSerialize();
            JsonUtility.FromJsonOverwrite(toJson, serialize);
            serialize.Output();

            //string json = JsonUtility.ToJson(asset);
            //Debug.LogError("json = " + json);
            //_Json = json;

            //TestJsonSerializeWrapper testJsonSerializeWrapper = new TestJsonSerializeWrapper(asset._id, asset._name, asset._List, asset._Dict, asset._Type, asset._LayerDict);
            //_JsonSerializeWrapper = testJsonSerializeWrapper;

            //Custom Json Implement

        }

        if (GUILayout.Button("Json Deserialize"))
        {
            if (_Json != null)
            {
                //TestJsonSerialize newAsset = new TestJsonSerialize(20, "Quan");
                //JsonUtility.FromJsonOverwrite(_Json, newAsset);
                //newAsset.Output();

                //TestJsonSerializeWrapper serializeWrapper = JsonUtility.FromJson<TestJsonSerializeWrapper>(_Json);
                //serializeWrapper.Output();
            }
        }
    }

    //void OnGUI()
    //{
    //    if (GUI.Button(new Rect(10, 10, 160, 20), "Json Serialize"))
    //    {
    //        TestJsonSerialize tjs = new TestJsonSerialize(100, "Quan");
    //        string json = JsonUtility.ToJson(tjs);
    //        _Json = json;
    //        Debug.LogError("json = " + json);
    //    }

    //    if (GUI.Button(new Rect(10, 50, 160, 20), "Json Deserialize"))
    //    {
    //        if (_Json != null)
    //        {
    //            TestJsonSerialize tjs = new TestJsonSerialize(200, "Luo");
    //            JsonUtility.FromJsonOverwrite(_Json, tjs);
    //            tjs.Output();
    //        }
    //    }
    //}

}
