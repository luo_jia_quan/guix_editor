﻿using System;
using System.Collections.Generic;
using KLFramework.GUIX.Timeline;
using UnityEngine;

public class SerializeClass : ScriptableObject
{
    [HideInInspector]
    public int age = 18;
    [HideInInspector]
    public float height = 175.3f;

    [NonSerialized]
    public string Keys;

    [NonSerialized] public string Values;

    [SerializeField]
    public List<ScriptableObject> scriptableObjectsList;

    [SerializeField]
    public SerializeClassDictionary SerializeClassDictionary = new SerializeClassDictionary();

    [SerializeField]
    public SerializableSystemType MyType;

    void OnEnable()
    {
        //Debug.LogError("OnEnable");

        if (SerializeClassDictionary == null)
        {
            SerializeClassDictionary = new SerializeClassDictionary();
        }

        //foreach (var temp in SerializeClassDictionary)
        //{
        //    Debug.LogError("Dictionary key = " + temp.Key + " Value = " + temp.Value);
        //} 
    }

}

[Serializable]
public class SerializeClassDictionary : Dictionary<int, string>, ISerializationCallbackReceiver
{
    [SerializeField]
    private List<int> _keys = new List<int>();
    [SerializeField]
    private List<string> _values = new List<string>();

    public SerializeClassDictionary()
    {
        //Debug.LogError("SerializeClassDictionary construct!!!");
    }


    public void OnBeforeSerialize()
    {
        //Debug.LogError("SerializeClassDictionary OnBeforeSerialize");
        _keys.Clear();
        _values.Clear();
        foreach (var temp in this)
        {
            //Debug.LogError("OnBeforeSerialize Add Keys");
            _keys.Add(temp.Key);    
            _values.Add(temp.Value);
        }
    }

    public void OnAfterDeserialize()
    {
        //Debug.LogError("SerializeClassDictionary OnAfterDeserialize");
        Clear();
        int minCount = Math.Min(_keys.Count, _values.Count);
        for (int i = 0; i < minCount; i++)
        {
            //Debug.LogError("Revert Dictionary Key-Value");
            Add(_keys[i], _values[i]);
        }
    }
}


