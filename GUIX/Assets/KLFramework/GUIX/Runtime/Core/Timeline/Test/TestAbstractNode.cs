﻿using System;
using UnityEngine;

[Serializable]
public abstract class TestAbstractNode
{
    [SerializeField]
    protected float start;
    [SerializeField]
    protected float end;

    protected abstract void TestMethod();
}

[Serializable]
public class ImplementNode : TestAbstractNode
{
    public ImplementNode(float start, float end)
    {
        this.start = start;
        this.end = end;
    }

    protected override void TestMethod()
    {
        Debug.LogError("ImplementNode");
    }
}