﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializeInterfaceSubClass : ISerializeInterface
{
    public Vector2 size;

    //[SerializeReference]
    //public List<TestNode> NodeList = new List<TestNode>();


    public void Method()
    {
        Debug.LogError("SerializeInterfaceSubClass Method");
    }
}
