﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[ExecuteInEditMode]
public class TestEditorEvent : MonoBehaviour
{
    private Vector2 _SV;

    private void OnGUI()
    {
        _SV = GUI.BeginScrollView(new Rect(10, 10, Screen.width - 20, Screen.height - 20), _SV, new Rect(0, 0, Screen.width - 40, Screen.height * 2));
        if (GUI.Button(new Rect(10, 10, 300, 60), "AAAAA"))
        {
            Debug.Log("-------");
        }
        GUI.EndScrollView();
    }

    void Update()
    {
#if UNITY_EDITOR
        GameView_Repaint();
#endif
    }

#if UNITY_EDITOR
    static EditorWindow gameView = null;
    public static void GameView_Repaint()
    {
        if (gameView == null)
        {
            System.Reflection.Assembly assembly = typeof(UnityEditor.EditorWindow).Assembly;
            System.Type type = assembly.GetType("UnityEditor.GameView");
            gameView = EditorWindow.GetWindow(type);
        }

        if (gameView != null)
        {
            Debug.LogError("repaint");
            gameView.Repaint();
        }
    }
#endif
}