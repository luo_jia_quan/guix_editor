﻿using System;
using KLFramework.GUIX.Timeline;
using UnityEngine;

[Serializable]
public class TestNode : ScriptableObject
{
    public float TestNodeWidth = 10f;
    public float TestNodeHeight = 20f;

    public ScriptableObject NodeData;

    //[SerializeField]
    //protected TestAbstractNode _AbstractNode;
    //protected ScriptableObject _Data;

    //public void SetAbstractNode(TestAbstractNode abstractNode)
    //{
    //    _AbstractNode = abstractNode;
    //}
}

