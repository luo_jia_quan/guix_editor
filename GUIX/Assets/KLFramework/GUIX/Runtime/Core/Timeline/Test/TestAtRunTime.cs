﻿using KLFramework.GUIX.Timeline;
using UnityEditor;
using UnityEngine;

public class TestAtRunTime : MonoBehaviour
{
    public GUITimeline _Timeline;

    void OnEnable()
    {
        var timeline = AssetDatabase.LoadAssetAtPath<GUITimeline>("Assets/SkillEditorWindow/Editor/Base/Node/test2.asset");
        if (timeline != null)
        {
            _Timeline = timeline;
        }
    }
}
