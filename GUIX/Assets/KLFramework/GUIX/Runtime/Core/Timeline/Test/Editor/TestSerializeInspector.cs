﻿using System.Collections.Generic;
using KLFramework.GUIX.Timeline;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SerializeClass))]
public class TestSerializeInspector : Editor
{
    private SerializedProperty AgeProperty;
    private SerializedProperty HeightProperty;
    private SerializedProperty ObjectsListProperty;
    private SerializedProperty DictionaryProp;

    public void OnEnable()
    {
        AgeProperty = serializedObject.FindProperty("age");
        HeightProperty = serializedObject.FindProperty("height");
        ObjectsListProperty = serializedObject.FindProperty("scriptableObjectsList");
        DictionaryProp = serializedObject.FindProperty("SerializeClassDictionary");
    }

    public override void OnInspectorGUI()
    {
        var asset = target as SerializeClass;
        if (!asset) return;

        serializedObject.Update();

        EditorGUILayout.PropertyField(AgeProperty);
        EditorGUILayout.PropertyField(HeightProperty);
        EditorGUILayout.PropertyField(ObjectsListProperty);
        EditorGUILayout.PropertyField(DictionaryProp);

        if(GUILayout.Button("AddScriptableObject"))
        {
            if (asset.scriptableObjectsList == null)
            {
                asset.scriptableObjectsList = new List<ScriptableObject>();
            }
            var node = ScriptableObject.CreateInstance<TestNode>();
            node.NodeData = CreateInstance<ScriptableObject>();
            
            asset.scriptableObjectsList.Add(node);

            node.name = "TestNode";
            AssetDatabase.AddObjectToAsset(node, target);
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
            node.NodeData.name = "ScriptableObject";
            AssetDatabase.AddObjectToAsset(node.NodeData, node);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        asset.Keys = EditorGUILayout.TextField("Keys", asset.Keys);
        asset.Values = EditorGUILayout.TextField("Values", asset.Values);

        if (GUILayout.Button("AddDictionaryKeyValue") && !string.IsNullOrEmpty(asset.Keys))
        {
            if (asset.SerializeClassDictionary == null)
            {
                asset.SerializeClassDictionary = new SerializeClassDictionary();
            }
            asset.SerializeClassDictionary.Add(int.Parse(asset.Keys), asset.Values);
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }

        if (GUILayout.Button("Debug Log Dictionary"))
        {
            if (asset.SerializeClassDictionary != null)
            {
                foreach (var temp in asset.SerializeClassDictionary)
                {
                    Debug.LogError("Key = " + temp.Key + " Value = " + temp.Value);
                }
            }
            Debug.LogError("MyType = " + asset.MyType + "--------------------------" + asset.MyType.SystemType); 
        }

        if (GUILayout.Button("Set Type"))
        {
            asset.MyType = new SerializableSystemType(typeof(SerializeClass));
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }

        serializedObject.ApplyModifiedProperties();
    }
}

//[CustomEditor(typeof(GUITimeline))]
//public class GUITimelineInspector : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        base.OnInspectorGUI();
//    }
//}
