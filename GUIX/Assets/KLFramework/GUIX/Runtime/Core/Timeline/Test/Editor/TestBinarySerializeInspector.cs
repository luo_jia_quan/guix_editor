﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;

//[ExecuteInEditMode]
[CustomEditor(typeof(TestBinarySerialize))]
public class TestBinarySerializeInspector : Editor //: MonoBehaviour
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var asset = target as TestBinarySerialize;
        if(asset == null) return;

        if (GUILayout.Button("Binary Serialize"))
        {
            FileStream fs = new FileStream("Assets/SkillEditorWindow/Editor/Base/Node/testBin.bin", FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, asset);
            fs.Close();
            Debug.LogError("Binary Serialize finish!!!");
        }

        GUILayout.Space(10f);
        if (GUILayout.Button("Binary Deserialize"))
        {
            FileStream fs = new FileStream("Assets/SkillEditorWindow/Editor/Base/Node/testBin.bin", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            TestBinarySerialize tbs = bf.Deserialize(fs) as TestBinarySerialize;
            fs.Close();
            tbs.Output();
        }
    }

    //void OnGUI()
    //{
    //    if (GUI.Button(new Rect(10, 10, 160, 20),  "Binary Serialize"))
    //    {
    //        TestBinarySerialize asset = new TestBinarySerialize(100, "Luo");
    //        FileStream fs = new FileStream("Assets/SkillEditorWindow/Editor/Base/Node/testBin.bin", FileMode.OpenOrCreate);
    //        BinaryFormatter bf = new BinaryFormatter();
    //        bf.Serialize(fs, asset);
    //        fs.Close();
    //        Debug.LogError("Binary Serialize finish!!!");
    //    }

    //    if (GUI.Button(new Rect(10, 50, 160, 20), "Binary Deserialize"))
    //    {
    //        FileStream fs = new FileStream("Assets/SkillEditorWindow/Editor/Base/Node/testBin.bin", FileMode.Open);
    //        BinaryFormatter bf = new BinaryFormatter();
    //        TestBinarySerialize tbs = bf.Deserialize(fs) as TestBinarySerialize;
    //        fs.Close();
    //        tbs.Output();
    //    }
    //}
}
