﻿using System;
using UnityEngine;

[Serializable]
public class SerializeList
{
    public int year = 2021;
    public int month = 6;
    public int date = 7;
}

