﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using KLFramework.GUIX.Timeline;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class NewInstance : ScriptableObject
{
    public string name;

    public NewInstance()
    {
        
    }

}

public class TestScript : Editor
{
    [SerializeField]
    private static List<Object> _objList = new List<Object>();
    [SerializeField]
    private static object[] _objArray = new object[3];

    private static NewInstance _record = null;

    private static int _Count = 0;

    [MenuItem("Assets/Test/Create ScriptObj")]
    public static void CreateScriptObj()
    {
        Undo.undoRedoPerformed += MyUnDoCallback;
        var obj1 = ScriptableObject.CreateInstance<NewInstance>();
        _record = obj1;
        Undo.RegisterCreatedObjectUndo(obj1, "Create obj1");
        _objList.Add(obj1);
    }

    private static void MyUnDoCallback()
    {
        Debug.LogError("Undo callback list count = " + _objList.Count + " record obj = " + _record);
        for (int i = _objList.Count - 1; i >= 0; i--)
        {
            //if (_objList[i] == null)
            //{
            //    _objList.RemoveAt(i);
            //}
            if (_objList[i] != null)
            {
                Debug.LogWarning(" object name = " + _objList[i].name);
            }
        }
        //Debug.LogError("handle remove list count = " + _objList.Count + " record obj = " + _record);
    }

    [MenuItem("Assets/Test/Remove UndoRedo Callback Event")]
    public static void RemoveUndoEvent()
    {
        Undo.undoRedoPerformed -= MyUnDoCallback;
    }

    [MenuItem("Assets/Test/Add UndoRedo Callback")]
    public static void AddUndoRedoCallback()
    {
        Undo.undoRedoPerformed += MyUnDoCallback;
    }

    [MenuItem("Assets/Test/Add Undo Object")]
    public static void AddUndoObject()
    {
        _Count++;
        var obj = ScriptableObject.CreateInstance<NewInstance>();
        obj.name = "Obj_" +  _Count.ToString();
        Undo.RegisterCreatedObjectUndo(obj, string.Format("Create obj_{0}", _Count));
        _objList.Add(obj);
    }

    [MenuItem("Assets/Test/Perform Undo Once")]
    public static void PerformUndoOnce()
    {
        Undo.PerformUndo();
    }

    [MenuItem("Assets/Test/CreateThenUndo")]
    public static void TestCreateThenUndo()
    {
        var obj1 = ScriptableObject.CreateInstance<NewInstance>();
        _record = obj1;
        Undo.RegisterCreatedObjectUndo(obj1, "Create Object");
        _objList.Add(obj1);
        _objArray[0] = obj1;

        Debug.LogError("After Create, List count = " + _objList.Count + " record obj = " + _record + " obj array = " + _objArray[0]);
        Undo.PerformUndo();
        Debug.LogError("After Undo, List Count = " + _objList.Count + " record obj = " + _record + " obj array = " + _objArray[0]);
    }

    [MenuItem("Assets/Test/Log Result")]
    public static void LogResult()
    {
        Debug.LogError("List count = " + _objList.Count + " record obj = " + _record);
    }

    [MenuItem("Assets/Test/Clear List")]
    public static void ClearList()
    {
        _objList.Clear();
        _record = null;
    }

    [MenuItem("Assets/Test/Serialize")]
    public static void TestSerialize()
    {
        //SerializeClass temp = new SerializeClass();
        //temp.SerializeLists = new List<SerializeList>();
        //temp.SerializeLists.Add(new SerializeList());
        //temp.Layers = new List<ITimelineLayer>();
        //temp.Layers.Add(new BaseLayer());
        string path = "Assets/SkillEditorWindow/Editor/Base/Node/TestSerialize.asset";
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        //SerializeClass temp = ScriptableObject.CreateInstance<SerializeClass>();
        //temp.SerializeLists = new List<SerializeList>();
        //var listObj = new SerializeList();
        //temp.SerializeLists.Add(listObj);

        //AssetDatabase.CreateAsset(temp, path);
        //AssetDatabase.SaveAssets();
        //AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Test/Deserialize")]
    public static void TestDeserialize()
    {
        var obj = AssetDatabase.LoadAssetAtPath<SerializeClass>("Assets/SkillEditorWindow/Editor/Base/Node/TestSerialize.asset");
        //Debug.LogError("obj.age = " + obj.age + " obj.height = " + obj.height + " obj list[0].year = " + obj.SerializeLists[0].year);
    }

}
