﻿using System;
using UnityEngine;
using Debug = UnityEngine.Debug;

[Serializable]
public class TestBinarySerialize : ScriptableObject
{
    public int _id;
    public string _myName;

    public TestBinarySerialize(int id, string myName)
    {
        _id = id;
        _myName = myName;
    }

    public TestBinarySerialize()
    {

    }

    public void Output()
    {
        Debug.LogError("id = " + _id + " myName = " + _myName);
    }
}
