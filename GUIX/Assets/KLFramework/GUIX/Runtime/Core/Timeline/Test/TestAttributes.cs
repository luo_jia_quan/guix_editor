﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using KLFramework.GUIX.Timeline;
using SerializeExtend;
using UnityEditor;
using UnityEngine;
using AnimationNodeData = KLFramework.GUIX.Timeline.AnimationNodeData;


public class TestAttributes
{
    [MenuItem("Tools/TestAttributes")]
    public static void TestAttribute()
    {
        if (true)
        {
            Type type = typeof(AnimationNodeData);
            string assembleyName = type.Assembly.GetName(false).Name;
            Debug.LogError("get type = " + Type.GetType(type.FullName + "," + assembleyName) + " assembly = " + assembleyName);

            Dictionary<string, object> dict = new Dictionary<string, object>()
           {
               {"gameobject", new GameObject("GameObject")},
           };

           var json = new JsonSerializer().Serialize(dict);
           Debug.LogError(json);

           var deserialized = new JsonDeSerializer().DeSerialize<Dictionary<string, object>>(json);
           Debug.LogError("deserialized = " + deserialized);

            return;
        }

        Test();
    }

    public static void Test()
    {
        PrintAuthorInfo(typeof(FirstClass));
        PrintAuthorInfo(typeof(SecondClass));
        PrintAuthorInfo(typeof(ThirdClass));
    }

    private static void PrintAuthorInfo(System.Type t)
    {
        Debug.LogError( "Author information for " + t.Name);

        // Using reflection.  
        System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // Reflection.  

        // Displaying output.  
        foreach (System.Attribute attr in attrs)
        {
//            if (attr is Author)
//            {
//                Author a = (Author)attr;
//                //System.Console.WriteLine("   {0}, version {1:f}", a.GetName(), a.version);
//                Debug.LogError("a.name = " + a.GetName() + " version = " + a.version);
//            }

            Debug.LogError("attribute = " + attr);
        }
    }

}

[System.AttributeUsage(System.AttributeTargets.Class |
                       System.AttributeTargets.Struct,
        AllowMultiple = true)  // Multiuse attribute.  
]
public class Author : System.Attribute
{
    string name;
    public double version;

    public Author(string name)
    {
        this.name = name;

        // Default value.  
        version = 1.0;
    }

    public string GetName()
    {
        return name;
    }
}

// Class with the Author attribute.  
[Author("P. Ackerman")]
[NodeEventType(typeof(AnimationNodeEvent))]
public class FirstClass : ScriptableObject
{
    // ...  
}

// Class without the Author attribute.  
public class SecondClass
{
    // ...  
}

// Class with multiple Author attributes.  
[Author("P. Ackerman"), Author("R. Koch", version = 2.0)]
public class ThirdClass
{
    // ...  
}

