﻿using System;
using System.Collections.Generic;
using KLFramework.GUIX.Timeline;
using UnityEngine;

[Serializable]
public class TestJsonSerialize : ScriptableObject
{

    public SerializationDictionary<int, string> _SerializationDictionary;

    public SampleDictionary _SampleDictionary;

    //[SerializeReference]
    //public Dictionary<ITimelineLayer, Type> _LayerDict = new Dictionary<ITimelineLayer, Type>();

    public void Output()
    {
        if (_SerializationDictionary == null)
        {
            Debug.LogError("_SerializationDictionary == null");
        }
        else
        {
            foreach (var temp in _SerializationDictionary.ToDictionary())
            {
                Debug.LogError("Temp.Key = " + temp.Key + " Temp.Value = " + temp.Value);
            }
        }
    }
}

[Serializable]
public class SampleDictionary : SerializationDictionary<int, string>
{
    public SampleDictionary(Dictionary<int, string> target) : base(target)
    {
    }
}


[Serializable]
public class TestJsonSerializeWrapper
{
    //public int _id;
    //public string _name;

    //[SerializeReference]
    //public List<ITestJson> _List = new List<ITestJson>();

    [SerializeField]
    public SerializationDictionary<int, string> _SerializationDictionary;

    //public SerializableSystemType _Type;

    //[SerializeReference]
    //public Dictionary<ITimelineLayer, Type> _LayerDict = new Dictionary<ITimelineLayer, Type>();

    //public TestJsonSerializeWrapper(int id, string name, List<ITestJson> list, SerializationDictionary<int, string> dict, SerializableSystemType type, Dictionary<ITimelineLayer, Type> layerDict)
    //{
    //    _id = id;
    //    _name = name;
    //    _List = list;
    //    _SerializationDictionary = dict;
    //    _Type = type;
    //    _LayerDict = layerDict;
    //}

    public TestJsonSerializeWrapper(SerializationDictionary<int, string> dict)
    {
        _SerializationDictionary = dict;
    }

    public void Output()
    {
        if (_SerializationDictionary == null)
        {
            Debug.LogError("_SerializationDictionary == null");
        }

        foreach (var temp in _SerializationDictionary.ToDictionary())
        {
            Debug.LogError("temp.Key = " + temp.Key + " temp.value = " + temp.Value);
        }
    }
}

public interface ITestJson
{
    void Test();
}

[Serializable]
public class TestJsonClass : ITestJson
{
    public int Age;
    public float Month;

    [SerializeField]
    private int Day;

    [SerializeReference]
    public AbstractNode Node;

    public ScriptableObject _Date;

    public TestJsonClass(int age, float month, int day)
    {
        Age = age;
        Month = month;
        Day = day;
    }

    public int GetDay()
    {
        return Day;
    }

    public void Test()
    {
        
    }
}

