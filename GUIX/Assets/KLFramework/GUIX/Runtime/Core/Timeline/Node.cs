﻿
using SerializeExtend;
using UnityEngine;
using Reflections = KLFramework.Utils.Reflections;

namespace KLFramework.GUIX.Timeline
{
    public class Node : ScriptableObject
    {
        [CustomSerializeField]
        [Reflections.CloneField]
        private INodeEvent _NodeEvent; 
        [CustomSerializeField]
        [Reflections.CloneField]
        private AbstractNode _AbstractNode;
        [CustomSerializeField]
        [Reflections.CloneField]
        private ScriptableObject _Data;

        public Node()
        {
            //use default abstractNodeRenderer
            //this._AbstractNode = new BaseNode();
            //this._Inspector = null;
        }

        public void SetNodeRenderer(AbstractNode abstractNodeRenderer)
        {
            this._AbstractNode = abstractNodeRenderer;
        }

        public AbstractNode GetNodeRenderer()
        {
            return this._AbstractNode;
        }

        public void SetData(ScriptableObject data)
        {
            _Data = data;
        }

        public ScriptableObject GetData()
        {
            return _Data;
        }

        public void SetNodeEvent(INodeEvent nodeEvent)
        {
            _NodeEvent = nodeEvent;
        }

        public INodeEvent GetNodeEvent()
        {
            return _NodeEvent;
        }

    }
}
