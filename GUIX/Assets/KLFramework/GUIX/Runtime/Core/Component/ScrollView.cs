using UnityEngine;

namespace KLFramework.GUIX
{
    
    //******************************************
    // ScrollView
    // Just a scrollview. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 21:13
    //******************************************
    public class ScrollView : AbstractUI<ScrollView>
    {
        private int _Width;
        private int _Height;
        private IUI _UI;

        private bool _Horizontal;
        private bool _Vertical;
        
        private Vector2 _SV;

        public ScrollView(int width, int height) : this(width, height,  null)
        {
        }

        public ScrollView(IUI ui):this(0, 0, ui)
        {
        }

        public ScrollView(int width, int height, IUI ui)
        {
            _Width = width;
            _Height = height;
            _UI = ui;
        }

        public ScrollView SetHorizontal(bool horizontal)
        {
            _Horizontal = horizontal;
            return this;
        }

        public ScrollView SetVertical(bool vertical)
        {
            _Vertical = vertical;
            return this;
        }

        public ScrollView SetUI(IUI ui)
        {
            _UI = ui;
            return this;
        }
        
        public ScrollView SetWidth(int width)
        {
            _Width = width;
            return this;
        }
        
        public ScrollView SetHeight(int height)
        {
            _Height = height;
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            Vector2 size = _UI.Size();
            var rect = new Rect(x, y, _Width, _Height);
            
            _SV = GUI.BeginScrollView(rect, _SV, new Rect(0, 0, size.x, size.y), _Horizontal, _Vertical);
            _UI.Render(0, 0, ref repaint);
            GUI.EndScrollView();

            return rect;
        }

        protected override Vector2 SelfSize()
        {
            return new Vector2(_Width, _Height);
        }
    }
}