using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // Label
    // Just a label. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 09:44
    //******************************************
    public class Label : AbstractUI<Label>
    {

        private string _Text;
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;
        
        public string Text { get { return _Text; } }
        public int FontSize { get { return _FontSize; } }

        public Color FontColor { get { return _FontColor; } }

        public Label()
        {
        }
        
        public Label(string text)
        {
            this._Text = text;
        }

        public Label SetText(string text)
        {
            this._Text = text;
            return this;
        }
        
        public Label SetFontSize(int fontSize)
        {
            this._FontSize = fontSize;
            return this;
        }
        
        public Label SetFontColor(Color color)
        {
            _SettedColor = true;
            this._FontColor = color;
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var style = _GetStyle();
            Vector2 size = style.CalcSize(new GUIContent(Text));
            GUI.Label(new Rect(x, y, size.x, size.y), Text, style);
            return new Rect(new Vector2(x, y), size);
        }

        protected override Vector2 SelfSize()
        {
            return _GetStyle().CalcSize(new GUIContent(Text));
        }

        private GUIStyle _GetStyle()
        {
            var style = new GUIStyle(GUI.skin.label);
            if (FontSize > 0)
            {
                style.fontSize = FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = FontColor;    
            }
            
            return style;
        }
    }
}