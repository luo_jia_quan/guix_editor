using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace KLFramework.GUIX
{
    
    //******************************************
    // TextField
    // Just a textfield. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 19:44
    //******************************************
    public class TextField : AbstractUI<TextField>
    {
        private const int _DEFAULT_WDITH = 100;
        
        private int _Width = _DEFAULT_WDITH;
        private string _Text;
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;
        
        private List<Action<TextField, string>> _OnChangeEvents = new List<Action<TextField, string>>();
        
        public string Text { get { return _Text; } }
        public int FontSize { get { return _FontSize; } }

        public Color FontColor { get { return _FontColor; } }

        public TextField AddChangeEvent(Action<TextField, string> onChange)
        {
            if (onChange != null && !_OnChangeEvents.Contains(onChange))
            {
                _OnChangeEvents.Add(onChange);
            }

            return this;
        }
        
        public TextField RemoveChangeEvent(Action<TextField, string> onChange)
        {
            if (onChange != null)
            {
                _OnChangeEvents.Remove(onChange);
            }
            return this;
        }
        
        public TextField ClearChangeEvent()
        {
            _OnChangeEvents.Clear();
            return this;
        }

        public TextField SetText(string text)
        {
            this._Text = text;
            return this;
        }
        
        public TextField SetFontSize(int fontSize)
        {
            this._FontSize = fontSize;
            return this;
        }
        
        public TextField SetFontColor(Color color)
        {
            _SettedColor = true;
            this._FontColor = color;
            return this;
        }

        public override TextField SetSizeAndReturn(int width, int height)
        {
            _Width = width;
            return base.SetSizeAndReturn(width, height);
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var style = _Style();
            var size = style.CalcSize(new GUIContent(_Text));
            var rect = new Rect(x, y + 2, _Width, size.y);

            var oldText = _Text;
            #if UNITY_EDITOR
            _Text = EditorGUI.TextField(rect, _Text, style);
            #else
            _Text = GUI.TextField(rect, _Text, style);
            #endif

            if (!string.Equals(oldText, _Text))
            {
                foreach (var onChange in _OnChangeEvents)
                {
                    onChange(this, _Text);
                }
            }

            return rect;
        }

        private GUIStyle _Style()
        {
            var style = new GUIStyle(GUI.skin.textField);

            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;
                style.hover.textColor = _FontColor;
                style.active.textColor = _FontColor;
            }
            
            return style;
        }

        protected override Vector2 SelfSize()
        {
            var size = _Style().CalcSize(new GUIContent(_Text));
            return new Vector2(_Width, size.y);
        }
    }
}