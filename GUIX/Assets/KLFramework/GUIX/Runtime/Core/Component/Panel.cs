using System.Collections.Generic;
using KLFramework.GUIX.Layout;
using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // Panel
    // Component container
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 21:12
    //******************************************
    public class Panel : AbstractUI<Panel>
    {
        private ILayout _Layout;
        private List<IUI> _UIs = new List<IUI>();

        public Panel(IEnumerable<IUI> uis):this(null, uis)
        {
        }
        
        public Panel(params IUI[] uis):this(null, uis)
        {
        }

        public Panel(ILayout layout, IEnumerable<IUI> uis)
        {
            SetLayout(layout);
            AddUI(uis);
        }
        
        public Panel(ILayout layout, params IUI[] uis)
        {
            SetLayout(layout);
            AddUI(uis);
        }

        public Panel SetLayout(ILayout layout)
        {
            if (layout == null)
            {
                layout = new FlowLayout();
            }
            _Layout = layout;
            return this;
        }

        public Panel AddUI(params IUI[] uis)
        {
            if (uis != null)
            {
                foreach (var ui in uis)
                {
                    _UIs.Add(ui);
                }    
            }
            
            return this;
        }
        
        public Panel AddUI(IEnumerable<IUI> uis)
        {
            if (uis != null)
            {
                foreach (var ui in uis)
                {
                    _UIs.Add(ui);
                }    
            }
            
            return this;
        }
        
        public Panel AddUI(IUI ui)
        {
            if (ui != null)
            {
                _UIs.Add(ui);    
            }
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            return _Layout.RenderSelf(_UIs, x, y, ref repaint);
        }

        protected override Vector2 SelfSize()
        {
            return _Layout.SelfSize(_UIs);
        }
    }
}