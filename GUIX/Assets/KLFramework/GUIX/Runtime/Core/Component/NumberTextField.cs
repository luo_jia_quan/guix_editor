﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections.Generic;
using KLFramework.GUIX.Timeline;
using KLFramework.GUIX.Timeline.Editor;
using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // TextField For Number
    // Can Change Number By Dragging，Easy To Edit
    //
    // @Author: ロロノア・ゾロ
    // @Email: jiaquan.luo@kunlun-inc.com
    // @Date: 2021-03-24 20:54
    //******************************************
    public class NumberTextField : AbstractUI<NumberTextField>
    {
        private const int _DEFAULT_WDITH = 100;

        private int _Width = _DEFAULT_WDITH;
        private string _Text;
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;
        private bool _IsCanDrag = false;
        private bool _LastIsInDragArea = false;
        private float _Precision;

        private List<Action<NumberTextField, float>> _OnChangeEvents = new List<Action<NumberTextField, float>>();

        public string Text { get { return _Text; } }
        public int FontSize { get { return _FontSize; } }

        public Color FontColor { get { return _FontColor; } }

        private float _Min;
        private float _Max;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="precision">拖动精度</param>
        public NumberTextField(float precision, float min = float.MinValue, float max = float.MaxValue)
        {
            _Precision = precision;
            _Min = min;
            _Max = max;
        }

        public NumberTextField Min(float min)
        {
            _Min = min;
            return this;
        }

        public NumberTextField Max(float max)
        {
            _Max = max;
            return this;
        }

        public NumberTextField AddChangeEvent(Action<NumberTextField, float> onChange)
        {
            if (onChange != null && !_OnChangeEvents.Contains(onChange))
            {
                _OnChangeEvents.Add(onChange);
            }

            return this;
        }

        public NumberTextField RemoveChangeEvent(Action<NumberTextField, float> onChange)
        {
            if (onChange != null)
            {
                _OnChangeEvents.Remove(onChange);
            }
            return this;
        }

        public NumberTextField ClearChangeEvent()
        {
            _OnChangeEvents.Clear();
            return this;
        }

        public NumberTextField SetText(string text)
        {
            this._Text = _LimitNumber(float.Parse(text));
            return this;
        }

        public NumberTextField SetText(float text)
        {
            this._Text = _LimitNumber(text);
            return this;
        }

        public NumberTextField SetFontSize(int fontSize)
        {
            this._FontSize = fontSize;
            return this;
        }

        public NumberTextField SetFontColor(Color color)
        {
            _SettedColor = true;
            this._FontColor = color;
            return this;
        }

        public override NumberTextField SetSizeAndReturn(int width, int height)
        {
            _Width = width;
            return base.SetSizeAndReturn(width, height);
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var style = _Style();
            var size = style.CalcSize(new GUIContent(_Text));
            var rect = new Rect(x, y + 2, _Width, size.y);

            var oldText = _Text;

            Vector2 mousePosition = Event.current.mousePosition - new Vector2(x, y + 2);
            var dragRect = new Rect(x - 15, y + 2, 15, 15);
            //GUI.DrawTexture(dragRect, GUIColorTexture.GetOrCreateColorTexture(Color.red));
            if (dragRect.Contains(Event.current.mousePosition))
            {
                _LastIsInDragArea = true;
                var texture = TimelineUtility.GetAssetTexture(TimelineConst.MOUSE_DRAG_ICON);
                TimelineUtility.SetCurrentViewCursor(texture, new Vector2(texture.width, texture.height) / 2, MouseCursor.CustomCursor);
            }
            else
            {
                if (_LastIsInDragArea)
                {
                    TimelineUtility.SetCurrentViewCursor(null, Vector2.zero, MouseCursor.Arrow);
                }
                _LastIsInDragArea = false;
            }
            repaint = true;
            string text = _Text;
            if (Event.current.isMouse)
            {
                switch (Event.current.rawType)
                {
                    case EventType.MouseDown:
                        if (_LastIsInDragArea)
                        {
                            _IsCanDrag = true;
                        }
                        break;
                    case EventType.MouseDrag:
                        if (_IsCanDrag)
                        {
                            var oldVal = float.Parse(oldText);
                            oldVal += Event.current.delta.x * _Precision;
                            text = oldVal.ToString();
                        }
                        break;
                    case EventType.MouseUp:
                        _IsCanDrag = false;
                        break;
                }
            }

#if UNITY_EDITOR
            text = EditorGUI.TextField(rect, text, style);
#else
            text = GUI.TextField(rect, text, style);
#endif

            float result;
            bool validNumber = float.TryParse(text, out result);
            if (validNumber)
            {
                _Text = _LimitNumber(result);
                if (!string.Equals(oldText, _Text))
                {
                    foreach (var onChange in _OnChangeEvents)
                    {
                        onChange(this, result);
                    }
                }
            }
            else
            {
                if (text.Equals("+") || text.Equals("-"))
                {
                    _Text = text;
                }
            }
            return rect;
        }

        private string _LimitNumber(float result)
        {
            return Mathf.Min(Mathf.Max(result, _Min), _Max).ToString();
        }

        private GUIStyle _Style()
        {
            var style = new GUIStyle(GUI.skin.textField);

            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;
                style.hover.textColor = _FontColor;
                style.active.textColor = _FontColor;
            }

            return style;
        }

        protected override Vector2 SelfSize()
        {
            var size = _Style().CalcSize(new GUIContent(_Text));
            return new Vector2(_Width, size.y);
        }
    }
}
