using System;
using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX
{
    
    //******************************************
    // Slider
    // Just a slider. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 19:44
    //******************************************
    public class Slider : AbstractUI<Slider>
    {
        public const int _DEFAULT_WIDTH = 100;
        public const int _DEFAULT_HEIGHT = 20;
        public const int _DEFAULT_HANDLER_SIZE = 16;
        
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;

        private bool _Dragging;

        private List<Action<Slider, float>> _OnValueChanged = new List<Action<Slider, float>>();

        private int _Width = _DEFAULT_WIDTH;
        private int _Height = _DEFAULT_HEIGHT;
        private int _HandlerSize = _DEFAULT_HANDLER_SIZE;

        private float _SliderRatio;

        private float _Min;
        private float _Max;
        private float _Value;

        private bool _ShowValue;

        private Func<float, float> _ValueConverter;
        
        public Slider(float min, float max, float value, bool showValue = true)
        {
            _Min = min;
            _Max = max;
            _Value = value;
            _ShowValue = showValue;
            SetValueConverter(null);
            SetRatio((value - min) / (max - min));
        }

        public Slider AddValueEvent(Action<Slider, float> action)
        {
            if (action != null && !_OnValueChanged.Contains(action))
            {
                _OnValueChanged.Add(action);
            }

            return this;
        }

        public Slider RemoveValueEvent(Action<Slider, float> action)
        {
            if (action != null)
            {
                _OnValueChanged.Remove(action);
            }

            return this;
        }

        public Slider ClearValueEvent()
        {
            _OnValueChanged.Clear();
            return this;
        }
        
        public Slider SetFontSize(int fontSize)
        {
            _FontSize = fontSize;
            return this;
        }

        public Slider SetFontColor(Color color)
        {
            _SettedColor = true;
            _FontColor = color;
            return this;
        }

        public Slider SetValue(float value)
        {
            _Value = Mathf.Clamp(value, _Min, _Max);
            _SliderRatio = (_Value - _Min) / (_Max - _Min);
            return this;
        }

        public Slider SetRatio(float ratio)
        {
            _SliderRatio = Mathf.Clamp(ratio, 0f, 1);
            _Value = _ValueConverter(_SliderRatio * (_Max - _Min) + _Min);
            return this;
        }
        
        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var sliderRect = _RenderSlider(x, y, ref repaint);
            return sliderRect;
        }
            
        public Slider SetValueConverter(Func<float, float> valueConverter)
        {
            _ValueConverter = valueConverter ?? ((v) => v);
            SetRatio(_SliderRatio);
            return this;
        }

        private Rect _RenderSlider(int x, int y, ref bool repaint)
        {
            var handerX = _SliderRatio * (_Width - _HandlerSize);
            var bgRect = new Rect(x, y, _Width, _Height);
            var fillRect = new Rect(x, y, handerX, _Height);
            var handerRect = new Rect(x + handerX, y, _HandlerSize, _Height);
            
            GUI.Box(bgRect, "", _BGStyle());
            GUI.Box(fillRect, "", _FillStyle());
            GUI.Box(handerRect, "", _HandlerStyle());

            if (_ShowValue)
            {
                var style = _ValueStyle();
                var content = new GUIContent(_Value.ToString());
                var valueSize = style.CalcSize(content);
                GUI.Label(new Rect(x + (_Width - valueSize.x) / 2, y, _Width, _Height), content, style);
            }

            if (Event.current != null)
            {
                switch (Event.current.rawType)
                {
                    case EventType.MouseDown:
                        if (handerRect.Contains(Event.current.mousePosition))
                        {
                            _Dragging = true;
                        }
                        break;
                    case EventType.MouseDrag:
                        if (_Dragging)
                        {
                            var deltaRatio = Event.current.delta.x / (_Width - _HandlerSize);
                            _SliderRatio += deltaRatio;
                            SetRatio(_SliderRatio);
                            foreach (var action in _OnValueChanged)
                            {
                                action(this, _Value);
                            }
                            repaint = true;    
                        }
                        break;
                    case EventType.MouseUp:
                        _Dragging = false;
                        break;
                }
            }
            
            return bgRect;
        }

        private GUIStyle _BGStyle()
        {
            var style = new GUIStyle(GUI.skin.box);
            style.normal.background = GUIColorTexture.GetOrCreateGradientTexture(GUIColorTexture.Crystal(new Color(0.4f, 0.4f, 0.4f)));
            style.border = new RectOffset();
            return style;
        }
        
        private GUIStyle _FillStyle()
        {
            var style = new GUIStyle(GUI.skin.box);
            style.normal.background = GUIColorTexture.GetOrCreateGradientTexture(GUIColorTexture.Crystal(new Color(0.8f, 0.8f, 0.8f)));
            style.border = new RectOffset();
            return style;
        }

        private GUIStyle _HandlerStyle()
        {
            var style = new GUIStyle(GUI.skin.box);
            style.border = new RectOffset();
            style.normal.background = GUIColorTexture.WHITE;
            return style;
        }

        public GUIStyle _ValueStyle()
        {
            var style = new GUIStyle(GUI.skin.label);
            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;    
            }
            return style;
        }

        protected override Vector2 SelfSize()
        {
            return new Vector2(_Width, _Height);
        }
    }
}