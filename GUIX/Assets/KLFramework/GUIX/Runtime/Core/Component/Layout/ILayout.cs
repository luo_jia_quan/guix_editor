using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX.Layout
{
    //******************************************
    // ILayout
    // For container
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 14:20
    //******************************************
    public interface ILayout
    {
        Rect RenderSelf(List<IUI> uis, int x, int y, ref bool repaint);

        Vector2 SelfSize(List<IUI> uis);
    }
}