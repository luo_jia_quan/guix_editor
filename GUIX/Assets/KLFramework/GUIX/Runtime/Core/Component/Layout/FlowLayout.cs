using System;
using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX.Layout
{
    //******************************************
    // FlowLayout
    // For container
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 11:24
    //******************************************
    public class FlowLayout : ILayout
    {
        public enum Direction
        {
            Horizontal,
            Vertical
        }
        
        public enum SizeMode
        {
            Fit,
            Min,
            Max,
        }

        private const int DEFAULT_PADDING = 5;
        private Direction _Direction = Direction.Vertical;
        private int _Padding = DEFAULT_PADDING;
        
        private SizeMode _HoritontalMode = SizeMode.Fit;
        private SizeMode _VerticalMode = SizeMode.Fit;
        

        public FlowLayout()
        {
        }

        public FlowLayout(Direction direction) : this(direction, 0)
        {
        }
        
        public FlowLayout(int padding) : this(Direction.Vertical, padding)
        {
        }

        public FlowLayout(Direction direction, int padding)
        {
            SetPadding(padding);
            SetDirection(direction);
        }
        
        public FlowLayout SetSizeMode(SizeMode vertical, SizeMode horizontal)
        {
            SetVerticalSizeMode(vertical);
            SetHorizontalSizeMode(horizontal);
            return this;
        }

        public FlowLayout SetVerticalSizeMode(SizeMode mode)
        {
            throw new NotImplementedException();
            _VerticalMode = mode;
            return this;
        }
        
        public FlowLayout SetHorizontalSizeMode(SizeMode mode)
        {
            throw new NotImplementedException();
            _HoritontalMode = mode;
            return this;
        }

        public FlowLayout SetDirection(Direction direction)
        {
            _Direction = direction;
            return this;
        }

        public FlowLayout SetPadding(int padding)
        {
            _Padding = padding;
            return this;
        }

        public Rect RenderSelf(List<IUI> uis, int x, int y, ref bool repaint)
        {
            SelfSize(uis);
            if (_Direction == Direction.Vertical)
            {
                int totalHeight = 0;
                float maxWidth = 0;
                for (int i = 0; i < uis.Count; i++)
                {
                    var ui = uis[i];
                    var uiRect = ui.Render(x, y + totalHeight, ref repaint);
                    
                    maxWidth = Mathf.Max(maxWidth, uiRect.width);
                    totalHeight += (int) uiRect.height;
                    
                    if (i < uis.Count - 1)
                    {
                        totalHeight += _Padding;
                    }
                }

                return new Rect(x, y, maxWidth, totalHeight);
            }
            else
            {
                int totalWidth = 0;
                float maxHeight = 0;
                
                for (int i = 0; i < uis.Count; i++)
                {
                    var ui = uis[i];
                    
                    var uiRect = ui.Render(x + totalWidth, y, ref repaint);
                    
                    maxHeight = Mathf.Max(maxHeight, uiRect.height);
                    totalWidth += (int) uiRect.width;
                    
                    if (i < uis.Count - 1)
                    {
                        totalWidth += _Padding;
                    }
                }
                return new Rect(x, y, totalWidth, maxHeight); 
            }
        }

        public Vector2 SelfSize(List<IUI> uis)
        {
            if (_Direction == Direction.Vertical)
            {
                float totalHeight = 0;
                float maxWidth = 0;
                for (int i = 0; i < uis.Count; i++)
                {
                    var ui = uis[i];
                    var size = ui.Size();
                    maxWidth = Mathf.Max(size.x, maxWidth);
                    totalHeight += size.y;
                }

                return new Vector2(maxWidth, totalHeight + _Padding * (uis.Count - 1));
            }

            if (_Direction == Direction.Horizontal)
            {
                float totalWidth = 0;
                float maxHeight = 0;
                float minHeight = int.MaxValue;
                float maxWidth = 0;
                float minWidth = int.MaxValue;
                for (int i = 0; i < uis.Count; i++)
                {
                    var ui = uis[i];
                    var size = ui.Size();
                    maxHeight = Mathf.Max(size.y, maxHeight);
                    minHeight = Mathf.Max(size.y, minHeight);
                    maxWidth = Mathf.Max(size.x, maxWidth);
                    minWidth = Mathf.Min(size.x, minWidth);
                    totalWidth += size.x;
                }

                float fixedWidth = -1;
                float fixedHeight = -1;
                
                if (_HoritontalMode == SizeMode.Max)
                {
                    totalWidth = maxWidth * uis.Count;
                    fixedWidth = maxWidth;
                }else if (_HoritontalMode == SizeMode.Min)
                {
                    totalWidth = minWidth * uis.Count;
                    fixedWidth = minWidth;
                }
                
                if (_VerticalMode == SizeMode.Min)
                {
                    maxHeight = minHeight;
                    fixedHeight = minHeight;
                }else if (_VerticalMode == SizeMode.Max)
                {
                    fixedHeight = maxHeight;
                }
                
                for (int i = 0; i < uis.Count; i++)
                {
                    var ui = uis[i];
                    //ui.SetSize((int)fixedWidth, (int)fixedHeight);
                }
                

                return new Vector2(totalWidth + _Padding * (uis.Count - 1), maxHeight);
            }

            throw new System.NotImplementedException("Unsupport direction : " + _Direction);
        }
    }
}