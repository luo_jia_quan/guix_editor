using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX.Layout
{
    //******************************************
    // AbsoluteLayout
    // For container
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-06-17 15:03
    //******************************************
    public class AbsoluteLayout : ILayout
    {
        struct Coord
        {
            public readonly int X;
            public readonly int Y;

            public Coord(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
        
        private Dictionary<IUI, Coord> _Coords = new Dictionary<IUI, Coord>();
        
        public AbsoluteLayout()
        {
        }

        public AbsoluteLayout SetPosition(IUI ui, int x, int y)
        {
            var coord = new Coord(x, y);
            if (_Coords.ContainsKey(ui))
            {
                _Coords[ui] = coord;    
            }
            else
            {
                _Coords.Add(ui, coord);
            }

            return this;
        }

        public Rect RenderSelf(List<IUI> uis, int x, int y, ref bool repaint)
        {
            int minX = int.MaxValue, maxX = int.MinValue, minY = int.MaxValue, maxY = int.MinValue;
            
            for (int i = 0; i < uis.Count; i++)
            {
                var ui = uis[i];
                var coord = _Coords.ContainsKey(ui) ? _Coords[ui] : new Coord(0, 0);
                var rect = ui.Render(coord.X + x, coord.Y + y, ref repaint);
                
                maxX = (int)Mathf.Max(maxX, coord.X + rect.width);
                maxY = (int)Mathf.Max(maxY, coord.Y + rect.height);
            }

            return new Rect(x, y, maxX, maxY);
        }

        public Vector2 SelfSize(List<IUI> uis)
        {
            int minX = int.MaxValue, maxX = int.MinValue, minY = int.MaxValue, maxY = int.MinValue;
            
            for (int i = 0; i < uis.Count; i++)
            {
                var ui = uis[i];
                var coord = _Coords.ContainsKey(ui) ? _Coords[ui] : new Coord(0, 0);
                var size = ui.Size();
                
                maxX = (int)Mathf.Max(maxX, coord.X + size.x);
                maxY = (int)Mathf.Max(maxY, coord.Y + size.y);
            }

            return new Vector2(maxX, maxY);
        }
    }
}