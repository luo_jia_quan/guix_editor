using System;
using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // Checkbox
    // Just a checkbox. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 14:33
    //******************************************
    public class Checkbox : AbstractUI<Checkbox>
    {
        private const int _PADDING = 3;
        private const string _SELECTED_CHAR = "√";

        public bool Selected { get { return _Selected; } }

        private string _Text;
        private int _FontSize;
        private Color _FontColor;
        private bool _SettedColor;

        private bool _Selected;

        private List<Action<Checkbox, bool>> _OnSelected = new List<Action<Checkbox, bool>>();


        public Checkbox()
        {
        }
        
        public Checkbox(string text, bool selected = false)
        {
            _Text = text;
            _Selected = selected;
        }

        public Checkbox SetSelected(bool selected)
        {
            _Selected = selected;
            return this;
        }

        public Checkbox AddSelectEvent(Action<Checkbox, bool> onSelected)
        {
            if (onSelected != null && !_OnSelected.Contains(onSelected))
            {
                _OnSelected.Add(onSelected);
            }

            return this;
        }
        
        public Checkbox RemoveSelectEvent(Action<Checkbox, bool> onSelected)
        {
            if (onSelected != null)
            {
                _OnSelected.Remove(onSelected);
            }

            return this;
        }
        
        public Checkbox ClearSelectEvent()
        {
            _OnSelected.Clear();
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var style = _GetStyle();
            var content = new GUIContent(_Text);
            var size = style.CalcSize(content);
            
            var btnSize = GUI.skin.button.CalcSize(new GUIContent(_SELECTED_CHAR));
            float height = Mathf.Max(btnSize.y, size.y, btnSize.x);

            if (GUI.Button(new Rect(x, y, height, height), _Selected ? _SELECTED_CHAR : ""))
            {
                _Selected = !Selected;

                foreach (var action in _OnSelected)
                {
                    action(this, _Selected);
                }
            }
            
            GUI.Label(new Rect(x + height + _PADDING, y + (height - size.y) / 2, size.x, size.y), content, style);
            return new Rect(x, y, _PADDING + height + size.x, height); 
        }

        protected override Vector2 SelfSize()
        {
            var style = _GetStyle();
            var size = style.CalcSize(new GUIContent(_Text));

            var btnSize = GUI.skin.button.CalcSize(new GUIContent(_SELECTED_CHAR));
            
            float height = Mathf.Max(btnSize.y, size.y, btnSize.x);
            float width = height + _PADDING + size.x;
            return new Vector2(width, height);
        }
        
        public Checkbox SetText(string text)
        {
            _Text = text;
            return this;
        }

        public Checkbox SetFontSize(int fontSize)
        {
            _FontSize = fontSize;
            return this;
        }

        public Checkbox SetFontColor(Color color)
        {
            _SettedColor = true;
            _FontColor = color;
            return this;
        }

        private GUIStyle _GetStyle()
        {
            var style = new GUIStyle(GUI.skin.label);
            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;
                style.hover.textColor = _FontColor;
                style.active.textColor = _FontColor;
            }
            
            return style;
        }
    }
}