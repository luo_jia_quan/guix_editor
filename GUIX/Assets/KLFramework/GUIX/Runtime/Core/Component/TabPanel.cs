using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX
{
    
    //******************************************
    // TabPanel
    // Tab ! Panel !
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 09:11
    //******************************************
    public class TabPanel : AbstractUI<TabPanel>
    {
        public class Item
        {
            public string Label;
            public IUI UI;

            public Item(string label, IUI ui)
            {
                Label = label;
                UI = ui;
            }
        }
        
        public enum Position
        {
            TopLeft,
            Top,
            TopRight,
            RightTop,
            Right,
            RightBottom,
            BottomRight,
            Bottom,
            BottomLeft,
            LeftBottom,
            Left,
            LeftTop
        }

        public readonly static Position[] POSITIONS = { 
            Position.TopLeft,
            Position.Top,
            Position.TopRight,
            Position.RightTop,
            Position.Right,
            Position.RightBottom,
            Position.BottomRight,
            Position.Bottom,
            Position.BottomLeft,
            Position.LeftBottom,
            Position.Left,
            Position.LeftTop
        };

        private List<Item> _Items = new List<Item>();
        private int _Index;
        private Position _Position = Position.TopLeft;
        
        private int _TabFontSize = -1;
        private Color _TabFontColor;
        private bool _SettedTabColor;
        
        private int _HighlightTabFontSize = -1;
        private Color _HighlightTabFontColor;
        private bool _SettedHighlightTabColor;

        private int[] _UIBorder = new int[] {1, 1, 1, 1};
        private Color _UIBorderColor = Color.gray;

        public TabPanel()
        {
        }
        
        public TabPanel AddItem(string label, IUI ui)
        {
            return AddItem(new Item(label, ui));
        }

        public TabPanel AddItem(IEnumerable<Item> items)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    AddItem(item);
                }
            }

            return this;
        }

        public TabPanel AddItem(params Item[] items)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    AddItem(item);
                }
            }

            return this;
        }

        public TabPanel AddItem(Item item)
        {
            if (item != null)
            {
                if (item.UI == null)
                {
                    item.UI = new Label("NULL");
                }
                
                _Items.Add(item);
            }

            return this;
        }

        public TabPanel SetIndex(int index)
        {
            _Index = index;
            return this;
        }
        
        public TabPanel Next()
        {
            _Index = (_Index + 1) % _Items.Count;
            return this;
        }
        
        public TabPanel Prev()
        {
            Debug.Log("SrcIndex " + _Index + " DstIndex : " + ((_Index - 1) % _Items.Count));
            //_Index = (_Index - 1) % _Items.Count;
            return this;
        }

        public TabPanel SetTabPosition(Position position)
        {
            _Position = position;
            return this;
        }

        public TabPanel SetTabFontSize(int fontSize)
        {
            this._TabFontSize = fontSize;
            return this;
        }
        
        public TabPanel SetTabFontColor(Color color)
        {
            _SettedTabColor = true;
            this._TabFontColor = color;
            return this;
        }
        
        public TabPanel SetHighlightTabFontSize(int fontSize)
        {
            this._HighlightTabFontSize = fontSize;
            return this;
        }
        
        public TabPanel SetHighlightTabFontColor(Color color)
        {
            _SettedHighlightTabColor = true;
            this._HighlightTabFontColor = color;
            return this;
        }
        
        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            float maxWidth = 0;
            float maxHeight = 0;

            float maxHightlightTabHeight = 0;
            float maxHightlightTabWidth = 0;
            
            float maxTabHeight = 0;
            float maxTabWidth = 0;
            
            float totalTabHeight = 0;
            float totalTabWidth = 0;

            for (int i = 0; i < _Items.Count; i++)
            {
                var item = _Items[i];
                var size = item.UI.Size();
                maxWidth = Mathf.Max(size.x, maxWidth);
                maxHeight = Mathf.Max(size.y, maxHeight);

                var maxHightlightStyle = _HightlightTabStyle();
                var maxHighlightTabSize = maxHightlightStyle.CalcSize(new GUIContent(item.Label));
                maxHightlightTabWidth = Mathf.Max(maxHighlightTabSize.x, maxHightlightTabWidth);
                maxHightlightTabHeight = Mathf.Max(maxHighlightTabSize.y, maxHightlightTabHeight);
                
                var maxStyle = _TabStyle();
                var maxTabSize = maxStyle.CalcSize(new GUIContent(item.Label));
                maxTabWidth = Mathf.Max(maxTabSize.x, maxTabWidth);
                maxTabHeight = Mathf.Max(maxTabSize.y, maxTabHeight);
                
                var style = i == _Index ? _TabStyle() : _HightlightTabStyle();
                var tabSize = style.CalcSize(new GUIContent(item.Label));
                // totalTabHeight += tabSize.y;
                // totalTabWidth += tabSize.x;
            }

            totalTabWidth = (_Items.Count - 1) * maxTabWidth + maxHightlightTabWidth;
            totalTabHeight = (_Items.Count - 1) * maxTabHeight + maxHightlightTabHeight;
            
            Vector2 uiSize = new Vector2(maxWidth, maxHeight);
            uiSize = _UISize(uiSize, maxHightlightTabWidth, maxHightlightTabHeight, maxTabWidth, maxTabHeight);
            Vector2 finalSize = _FinalSize(uiSize, maxHightlightTabWidth, maxHightlightTabHeight);
            Vector2 tabStartPos = _TabStartPos(x, y, maxHightlightTabWidth, maxHightlightTabHeight, totalTabWidth, totalTabHeight, uiSize);

            float offsetTabX = 0, offsetTabY = 0;
            for (int i = 0; i < _Items.Count; i++)
            {
                GUIStyle tabStyle = i == _Index ? _HightlightTabStyle() : _TabStyle();
                Rect tabRect = _CalTabPosition(i == _Index, maxHightlightTabWidth, maxHightlightTabHeight, maxTabWidth, maxTabHeight, tabStartPos, maxWidth, maxHeight, ref offsetTabX, ref offsetTabY);
                if (GUI.Button(tabRect, _Items[i].Label, tabStyle))
                {
                    _Index = i;
                }
            }

            var uiStartPos = _UIStartPos(x, y, maxHightlightTabWidth, maxHightlightTabHeight);
            _Items[_Index].UI.Render((int)uiStartPos.x, (int)uiStartPos.y, ref repaint);
            
            RenderBorder(new Rect((int)uiStartPos.x, (int)uiStartPos.y, uiSize.x, uiSize.y), _UIBorder, _UIBorderColor);
            
            return new Rect(x, y, finalSize.x, finalSize.y);
        }
        
        private Vector2 _UIStartPos(float x, float y, float maxTabWidth, float maxTabHeight)
        {
            switch (_Position)
            {
                case Position.Top:
                case Position.TopLeft:
                case Position.TopRight:
                    return new Vector2(x, y + maxTabHeight);
                case Position.Bottom:
                case Position.BottomLeft:
                case Position.BottomRight:
                    return new Vector2(x, y);
                case Position.Left:
                case Position.LeftBottom:
                case Position.LeftTop:
                    return new Vector2(x + maxTabWidth, y);
                case Position.Right:
                case Position.RightBottom:
                case Position.RightTop:
                    return new Vector2(x, y);
                default:
                    throw new System.NotImplementedException("Position = " + _Position);        
            }
        }

        private Vector2 _TabStartPos(int x, int y, float maxTabWidth, float maxTabHeight, float totalTabWidth, float totalTabHeight, Vector2 uiSize)
        {
            switch (_Position)
            {
                case Position.Top:
                    return new Vector2(x + (uiSize.x - totalTabWidth) / 2, y + maxTabHeight);
                case Position.TopLeft:
                    return new Vector2(x, y + maxTabHeight);
                case Position.TopRight:
                    return new Vector2(x + uiSize.x - totalTabWidth, y + maxTabHeight);
                case Position.Bottom:
                    return new Vector2(x + (uiSize.x - totalTabWidth) / 2, y + uiSize.y);
                case Position.BottomLeft:
                    return new Vector2(x, y + uiSize.y);
                case Position.BottomRight:
                    return new Vector2(x + uiSize.x - totalTabWidth, y + uiSize.y);
                case Position.Left:
                    return new Vector2(x + maxTabWidth, y + (uiSize.y - totalTabHeight) / 2);
                case Position.LeftBottom:
                    return new Vector2(x + maxTabWidth, y + (uiSize.y - totalTabHeight));
                case Position.LeftTop:
                    return new Vector2(x + maxTabWidth, y);
                case Position.Right:
                    return new Vector2(x + uiSize.x, y + (uiSize.y - totalTabHeight) / 2);
                case Position.RightBottom:
                    return new Vector2(x + uiSize.x, y + (uiSize.y - totalTabHeight));
                case Position.RightTop:
                    return new Vector2(x + uiSize.x, y);
                default:
                    throw new System.NotImplementedException("Position = " + _Position);        
            }
        }
        
        private Vector2 _UISize(Vector2 uiSize, float maxHightlightTabWidth, float maxHightlightTabHeight, float maxTabWidth, float maxTabHeight)
        {
            switch (_Position)
            {
                case Position.Top:
                case Position.TopLeft:
                case Position.TopRight:
                case Position.Bottom:
                case Position.BottomLeft:
                case Position.BottomRight:
                    float totalTabWidth = maxTabWidth * (_Items.Count - 1) + maxHightlightTabWidth;
                    return new Vector2(Mathf.Max(uiSize.x, totalTabWidth), uiSize.y);
                case Position.Left:
                case Position.LeftBottom:
                case Position.LeftTop:
                case Position.Right:
                case Position.RightBottom:
                case Position.RightTop:
                    float totalTabHeight = maxTabHeight * (_Items.Count - 1) + maxHightlightTabHeight;
                    return new Vector2(uiSize.x, Mathf.Max(uiSize.y, totalTabHeight));
                default:
                    throw new System.NotImplementedException("Position = " + _Position);        
            }
        }

        private Vector2 _FinalSize(Vector2 uiSize, float maxHightlightTabWidth, float maxHightlightTabHeight)
        {
            switch (_Position)
            {
                case Position.Top:
                case Position.TopLeft:
                case Position.TopRight:
                case Position.Bottom:
                case Position.BottomLeft:
                case Position.BottomRight:
                    return uiSize + new Vector2(0, maxHightlightTabHeight);
                case Position.Left:
                case Position.LeftBottom:
                case Position.LeftTop:
                case Position.Right:
                case Position.RightBottom:
                case Position.RightTop:
                    return uiSize + new Vector2(maxHightlightTabWidth, 0);
                default:
                    throw new System.NotImplementedException("Position = " + _Position);        
            }
        }

        private Rect _CalTabPosition(bool hightlight, float maxHightlightTabWidth, float maxHightlightTabHeight, float maxTabWidth, float maxTabHeight, Vector2 tabStartPos, float maxWidth, float maxHeight, ref float offsetTabX, ref float offsetTabY)
        {
            Vector2 tabSize = hightlight ? new Vector2(maxHightlightTabWidth, maxHightlightTabHeight) : new Vector2(maxTabWidth, maxTabHeight);
            Rect rect;
            switch (_Position)
            {
                case Position.Top:
                case Position.TopLeft:
                case Position.TopRight:
                    rect = new Rect(tabStartPos.x + offsetTabX, tabStartPos.y - tabSize.y, tabSize.x, tabSize.y);
                    offsetTabX += rect.width;
                    break;
                case Position.Bottom:
                case Position.BottomLeft:
                case Position.BottomRight:
                    rect = new Rect(tabStartPos.x + offsetTabX, tabStartPos.y, tabSize.x, tabSize.y);
                    offsetTabX += rect.width;
                    break;
                case Position.Left:
                case Position.LeftBottom:
                case Position.LeftTop:
                    rect = new Rect(tabStartPos.x - tabSize.x, tabStartPos.y + offsetTabY, tabSize.x, tabSize.y);
                    offsetTabY += rect.height;
                    break;
                case Position.Right:
                case Position.RightBottom:
                case Position.RightTop:
                    rect = new Rect(tabStartPos.x, tabStartPos.y + offsetTabY, tabSize.x, tabSize.y);
                    offsetTabY += rect.height;
                    break;
                default:
                    throw new System.NotImplementedException("Position = " + _Position);        
            }
            return rect;
        }

        protected override Vector2 SelfSize()
        {
            float maxWidth = 0;
            float maxHeight = 0;

            float maxHightlightTabHeight = 0;
            float maxHightlightTabWidth = 0;
            
            float maxTabHeight = 0;
            float maxTabWidth = 0;
                
            foreach (var item in _Items)
            {
                var size = item.UI.Size();
                maxWidth = Mathf.Max(size.x, maxWidth);
                maxHeight = Mathf.Max(size.y, maxHeight);

                var hightlightStyle = _HightlightTabStyle();
                var hightlightTabSize = hightlightStyle.CalcSize(new GUIContent(item.Label));
                maxHightlightTabWidth = Mathf.Max(hightlightTabSize.x, maxHightlightTabWidth);
                maxHightlightTabHeight = Mathf.Max(hightlightTabSize.y, maxHightlightTabHeight);
                
                var style = _TabStyle();
                var tabSize = style.CalcSize(new GUIContent(item.Label));
                maxTabWidth = Mathf.Max(tabSize.x, maxTabWidth);
                maxTabHeight = Mathf.Max(tabSize.y, maxTabHeight);
            }
            
            Vector2 uiSize = new Vector2(maxWidth, maxHeight);

            switch (_Position)
            {
                case Position.Top:
                case Position.TopLeft:
                case Position.TopRight:
                case Position.Bottom:
                case Position.BottomLeft:
                case Position.BottomRight:
                    float totalTabWidth = maxTabWidth * (_Items.Count - 1) + maxHightlightTabWidth;
                    return  new Vector2(Mathf.Max(uiSize.x, totalTabWidth), maxHightlightTabHeight + uiSize.y);
                case Position.Left:
                case Position.LeftBottom:
                case Position.LeftTop:
                case Position.Right:
                case Position.RightBottom:
                case Position.RightTop:
                    return uiSize + new Vector2(maxHightlightTabWidth, 0);
                default:
                    throw new System.NotImplementedException("Position = " + _Position);        
            }
        }

        private GUIStyle _HightlightTabStyle()
        {
            var  style = new GUIStyle(GUI.skin.button);

            if (_HighlightTabFontSize > 0)
            {
                style.fontSize = _HighlightTabFontSize;    
            }

            if (_SettedHighlightTabColor)
            {
                style.normal.textColor = _HighlightTabFontColor;    
            }

            return style;
        }

        private GUIStyle _TabStyle()
        {
            var  style = new GUIStyle(GUI.skin.button);

            if (_TabFontSize > 0)
            {
                style.fontSize = _TabFontSize;    
            }

            if (_SettedTabColor)
            {
                style.normal.textColor = _TabFontColor;    
            }

            return style;
        }
    }
}