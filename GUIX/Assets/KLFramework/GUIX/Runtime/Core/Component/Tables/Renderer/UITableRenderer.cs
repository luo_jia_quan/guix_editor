using System;
using UnityEngine;

namespace KLFramework.GUIX.Tables
{
    //******************************************
    // Table renderer port for ui
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 14:40
    //******************************************
    public class UITableRenderer : ITableRenderer
    {
        private IUI _UI;
        private Action<object, ITableDataModel, int, int, Table> _OnUpdate;
        
        public UITableRenderer(IUI ui, Action<object, ITableDataModel, int, int, Table> onUpdate)
        {
            _UI = ui;
            _OnUpdate = onUpdate;
        }

        public Vector2 Size()
        {
            return _UI.Size();
        }

        public void SetSize(int width, int height)
        {
            _UI.SetSize(width, height);
        }

        public Rect Render(int x, int y, ref bool repaint)
        {
            return _UI.Render(x, y, ref repaint);
        }

        public void Update(object data, ITableDataModel tableDataModel, int column, int row, Table table)
        {
            if(_OnUpdate != null)
            {
                _OnUpdate(data, tableDataModel, column, row, table);
            }
        }
    }
}