namespace KLFramework.GUIX.Tables
{
    
    //******************************************
    // TextField renderer for table
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 20:29
    //******************************************
    public class TextFieldRenderCreater : ITableRendererCreater
    {
        private int _Width;
        
        public TextFieldRenderCreater(int width = 100)
        {
            _Width = width;
        }

        public ITableRenderer Create(object data, int column, int row, Table table)
        {
            var textField = new TextField().SetText(data.ToString()).AddChangeEvent((tf, text) =>
            {
                table.GetDataModel().SetData(column, row, text);
            }).SetSizeAndReturn(_Width, -1);
            return new UITableRenderer(textField, (o, model, c, r, tbl) => { textField.SetText(o.ToString()); });
        }
    }
}