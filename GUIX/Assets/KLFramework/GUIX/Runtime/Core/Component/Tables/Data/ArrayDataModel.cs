using UnityEngine;

namespace KLFramework.GUIX.Tables
{
    //******************************************
    // Table data model based 2D array
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 17:09
    //******************************************
    public class ArrayDataModel : ITableDataModel
    {
        public int Row {
            get { return _Data.Length; }
        }

        public int Column {
            get
            {
                if (_Data == null || _Data.Length <= 0)
                {
                    return 0;
                }
                return _Data[0].Length;
            }
        }

        private object[][] _Data;
        
        public ArrayDataModel(object[][] data)
        {
            _Data = data ?? new object[0][];
        }

        public object GetData(int column, int row)
        {
            return _Data[row][column];
        }

        public void SetData(int column, int row, object value)
        {
            _Data[row][column] = value;
        }
    }
}