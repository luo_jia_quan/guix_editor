using KLFramework.GUIX;
using KLFramework.GUIX.Tables;

namespace DefaultNamespace
{
    //******************************************
    // Checkbox renderer for table
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-08-28 10:56
    //******************************************
    public class CheckboxRenderCreater : ITableRendererCreater
    {
        public ITableRenderer Create(object data, int column, int row, Table table)
        {
            var checkbox = new Checkbox("");
            return new UITableRenderer(checkbox.SetSelected(bool.Parse(data.ToString())).AddSelectEvent(((
                cb, b) =>
            {
                table.GetDataModel().SetData(column, row, b);
            })), (o, model, c, r, tbl) => { checkbox.SetSelected(bool.Parse(o.ToString())); });
        }
    }
}