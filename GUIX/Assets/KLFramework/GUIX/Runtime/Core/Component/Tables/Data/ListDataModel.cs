using System.Collections.Generic;

namespace KLFramework.GUIX.Tables
{
    public class ListDataModel : ITableDataModel
    {
        public int Row {
            get { return _Data.Count; }
        }
        public int Column {
            get
            {
                if (Row == 0)
                {
                    return 0;
                }

                return _Data[0].Count;
            }
        }

        private List<List<object>> _Data;

        public ListDataModel(List<List<object>> data)
        {
            _Data = data;
        }

        public object GetData(int column, int row)
        {
            return _Data[row][column];
        }

        public void SetData(int column, int row, object value)
        {
            _Data[row][column] = value;
        }
    }
}