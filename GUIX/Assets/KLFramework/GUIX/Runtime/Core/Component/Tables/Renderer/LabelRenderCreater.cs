namespace KLFramework.GUIX.Tables
{
    //******************************************
    // Label renderer for table
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 14:40
    //******************************************
    public class LabelRenderCreater : ITableRendererCreater
    {
        private int _Padding;
        
        public LabelRenderCreater(int padding = 5)
        {
            _Padding = padding;
        }

        public ITableRenderer Create(object data, int column, int row, Table table)
        {
            var label = new Label(data.ToString());
            return new UITableRenderer(label.SetPadding(_Padding), (o, model, c, r, tbl) =>
            {
                label.SetText(o.ToString());
            });
        }
    }
}