namespace KLFramework.GUIX.Tables
{
    //******************************************
    // Interface of table renderer
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 15:31
    //******************************************
    public interface ITableRenderer : IUI
    {
        void Update(object data, ITableDataModel tableDataModel, int column, int row, Table table);
    }
}