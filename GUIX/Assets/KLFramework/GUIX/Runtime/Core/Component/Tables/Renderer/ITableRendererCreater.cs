namespace KLFramework.GUIX.Tables
{
    
    //******************************************
    // Interface of table renderer creater
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 15:20
    //******************************************
    public interface ITableRendererCreater
    {
        ITableRenderer Create(object data, int column, int row, Table table);
    }
}