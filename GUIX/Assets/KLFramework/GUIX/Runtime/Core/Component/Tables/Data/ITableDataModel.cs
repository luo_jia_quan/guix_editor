namespace KLFramework.GUIX.Tables
{
    
    //******************************************
    // Interface of table data model
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 16:12
    //******************************************
    public interface ITableDataModel
    {
        int Row { get; }
        int Column { get; }

        object GetData(int column, int row);
        
        void SetData(int column, int row, object value);
    }
}