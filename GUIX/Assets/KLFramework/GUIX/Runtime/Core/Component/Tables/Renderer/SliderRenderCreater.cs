using System;

namespace KLFramework.GUIX.Tables
{
    //******************************************
    // Slider renderer for table
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 14:40
    //******************************************
    public class SliderRenderCreater : ITableRendererCreater
    {
        private float _Min;
        private float _Max;
        private Func<float, object> _Converter;
        
        public SliderRenderCreater(float min, float max, Func<float, object> convert)
        {
            _Converter = convert;
            _Min = min;
            _Max = max;
        }

        public ITableRenderer Create(object data, int column, int row, Table table)
        {
            var slider = new Slider(_Min, _Max, float.Parse(data.ToString()));
            slider.AddValueEvent((s, value) =>
            {
                table.GetDataModel().SetData(column, row, _Converter(value));
            });
            return new UITableRenderer(slider, (o, model, c, r, tbl) => { slider.SetValue(float.Parse(o.ToString())); });
        }
    }
}