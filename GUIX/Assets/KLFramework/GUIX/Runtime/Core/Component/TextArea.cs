using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace KLFramework.GUIX
{
    
    //******************************************
    // TextArea
    // Just a textarea. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 20:55
    //******************************************
    public class TextArea : AbstractUI<TextArea>
    {
        private const int _DEFAULT_WDITH = 100;
        private const int _DEFAULT_HEIGHT = 60;
        
        private int _Width = _DEFAULT_WDITH;
        private int _Height = _DEFAULT_HEIGHT;
        
        private string _Text;
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;
        
        private List<Action<TextArea, string>> _OnChangeEvents = new List<Action<TextArea, string>>();
        
        public string Text { get { return _Text; } }
        public int FontSize { get { return _FontSize; } }

        public Color FontColor { get { return _FontColor; } }

        public TextArea AddChangeEvent(Action<TextArea, string> onChange)
        {
            if (onChange != null && !_OnChangeEvents.Contains(onChange))
            {
                _OnChangeEvents.Add(onChange);
            }

            return this;
        }

        public TextArea SetTextAreaWidth(int width)
        {
            this._Width = width;
            return this;
        }
        
        public TextArea SetTextAreaHeight(int height)
        {
            this._Height = height;
            return this;
        }

        public TextArea RemoveChangeEvent(Action<TextArea, string> onChange)
        {
            if (onChange != null)
            {
                _OnChangeEvents.Remove(onChange);
            }
            return this;
        }
        
        public TextArea ClearChangeEvent()
        {
            _OnChangeEvents.Clear();
            return this;
        }

        public TextArea SetText(string text)
        {
            this._Text = text;
            return this;
        }
        
        public TextArea SetFontSize(int fontSize)
        {
            this._FontSize = fontSize;
            return this;
        }
        
        public TextArea SetFontColor(Color color)
        {
            _SettedColor = true;
            this._FontColor = color;
            return this;
        }
        
        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var style = _Style();
            var rect = new Rect(x, y + 2, _Width, _Height);

            var oldText = _Text;
            #if UNITY_EDITOR
            _Text = EditorGUI.TextArea(rect, _Text, style);
            #else
            _Text = GUI.TextArea(rect, _Text, style);
            #endif

            if (!string.Equals(oldText, _Text))
            {
                foreach (var onChange in _OnChangeEvents)
                {
                    onChange(this, _Text);
                }
            }

            return rect;
        }

        private GUIStyle _Style()
        {
            var style = new GUIStyle(GUI.skin.textField);

            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;
                style.hover.textColor = _FontColor;
                style.active.textColor = _FontColor;
            }
            
            return style;
        }

        protected override Vector2 SelfSize()
        {
            return new Vector2(_Width, _Height);
        }
    }
}