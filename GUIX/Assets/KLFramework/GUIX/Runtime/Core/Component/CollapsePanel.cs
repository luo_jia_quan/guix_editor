using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // CollapsePanel
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 21:12
    //******************************************
    public class CollapsePanel : AbstractUI<CollapsePanel>
    {
        
        private string _OpenTitle;
        private string _CloseTitle;
        private bool _Opened;
        private IUI _UI;
        
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;

        private int[] _Border = { 1, 1, 1, 1};
        private Color _BorderColor = Color.gray;

        public CollapsePanel()
        {
        }

        public CollapsePanel(string title, IUI ui)
        {
            _UI = ui;
            SetText(title);
        }

        public CollapsePanel SetToggle(bool open)
        {
            _Opened = open;
            return this;
        }

        public CollapsePanel SetText(string text)
        {
            _OpenTitle = "+" + text;
            _CloseTitle = "-" + text;
            return this;
        }
        
        public CollapsePanel SetFontSize(int fontSize)
        {
            this._FontSize = fontSize;
            return this;
        }
        
        public CollapsePanel SetFontColor(Color color)
        {
            _SettedColor = true;
            this._FontColor = color;
            return this;
        }
        
        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var titleSize = _TitleSize();

            var btnRect = new Rect(x, y, titleSize.x, titleSize.y);
            if (GUI.Button(btnRect, _Opened ? _CloseTitle : _OpenTitle, _GetStyle()))
            {
                _Opened = !_Opened;
            }
            var uiRect = _UI.Size();
            if (_Opened)
            {
                //RenderBorder(btnRect, _Border, _BorderColor);
                _UI.Render(x, y + (int) titleSize.y, ref repaint);
            }
            else
            {
                RenderBorder(new Rect(x, y, Mathf.Max(titleSize.x, uiRect.x), titleSize.y), _Border, _BorderColor);
            }

            if (_Opened)
            {
                RenderBorder(new Rect(x, y + (int)titleSize.y, Mathf.Max(titleSize.x, uiRect.x), uiRect.y), _Border, _BorderColor);
            }

            return new Rect(x, y, Mathf.Max(titleSize.x, uiRect.x), titleSize.y + ( _Opened ? uiRect.y : 0 ));
        }

        protected override Vector2 SelfSize()
        {
            var uiSize = _UI.Size();
            var titleSize = _TitleSize();
            
            return new Vector2(Mathf.Max(uiSize.x, titleSize.x), titleSize.y + (_Opened ? uiSize.y : 0));
        }

        private Vector2 _TitleSize()
        {
            var style = _GetStyle();
            var openTitleSize = style.CalcSize(new GUIContent(_OpenTitle));
            var closeTitleSize = style.CalcSize(new GUIContent(_CloseTitle));
            return new Vector2(Mathf.Max(openTitleSize.x, closeTitleSize.x), Mathf.Max(openTitleSize.y, closeTitleSize.y));
        }

        private GUIStyle _GetStyle()
        {
            var style = new GUIStyle(GUI.skin.button);
            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;    
            }
            
            return style;
        }
    }
}