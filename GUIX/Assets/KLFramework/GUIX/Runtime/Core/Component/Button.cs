using System;
using System.Collections.Generic;
using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // Button
    // Just a button. Emmmmm.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 09:34
    //******************************************
    public class Button : AbstractUI<Button>
    {
        private string _Text;
        private int _FontSize = -1;
        private Color _FontColor;
        private bool _SettedColor;

        private Texture2D _Background;
        private List<Action> _OnClick = new List<Action>();

        public Button()
        {
        }

        public Button(string text)
        {
            SetText(text);
        }

        public Button SetText(string text)
        {
            _Text = text;
            return this;
        }

        public Button AddClickEvent(Action action)
        {
            if (action != null && !_OnClick.Contains(action))
            {
                _OnClick.Add(action);
            }
            return this;
        }

        public Button RemoveClickEvent(Action action)
        {
            if (action != null)
            {
                _OnClick.Remove(action);
            }
            return this;
        }

        public Button ClearClickEvent()
        {
            _OnClick.Clear();
            return this;
        }

        public Button SetFontSize(int fontSize)
        {
            _FontSize = fontSize;
            return this;
        }

        public Button SetFontColor(Color color)
        {
            _SettedColor = true;
            _FontColor = color;
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            var style = _GetStyle();
            var content = new GUIContent(_Text);
            var size = style.CalcSize(content);
            if (GUI.Button(new Rect(x, y, size.x, size.y), content, style))
            {
                foreach (var onClick in _OnClick)
                {
                    onClick();
                }
            }
            return new Rect(x, y, size.x, size.y);
        }

        protected override Vector2 SelfSize()
        {
            return _GetStyle().CalcSize(new GUIContent(_Text));
        }

        private GUIStyle _GetStyle()
        {
            var style = new GUIStyle(GUI.skin.button);
            if (_FontSize > 0)
            {
                style.fontSize = _FontSize;    
            }

            if (_SettedColor)
            {
                style.normal.textColor = _FontColor;
                style.hover.textColor = _FontColor;
                style.active.textColor = _FontColor;
            }

            if (_Background)
            {
                style.normal.background = _Background;
                style.hover.background = _Background;
                style.active.background = _Background;
            }
            
            return style;
        }

        public Button SetBackground(Color start, Color end, bool horizontal = false)
        {
            _Background = GUIColorTexture.GetOrCreateGradientTexture(start, end, 32, horizontal);
            return this;
        }
        
        public Button SetBackground(GUIColorTexture.GradientInfo[] gradientInfos,  bool horizontal = false)
        {
            _Background = GUIColorTexture.GetOrCreateGradientTexture(gradientInfos, 32, horizontal);
            return this;
        }
        
        public Button SetBackground(Texture2D background)
        {
            _Background = background;
            return this;
        }
    }
}