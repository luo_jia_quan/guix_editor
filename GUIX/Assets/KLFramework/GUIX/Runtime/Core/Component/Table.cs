using KLFramework.GUIX.Tables;
using UnityEngine;

namespace KLFramework.GUIX
{
    //******************************************
    // Table
    // A table.
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-08 09:11
    //******************************************
    public class Table : AbstractUI<Table>
    {
        private readonly static ITableDataModel _EMPTY_DATA_MODEL = new ArrayDataModel(null);
        
        private ITableDataModel _TableDataModel = _EMPTY_DATA_MODEL;
        private ITableRenderer[][] _BodyRenderers;
        private Label[] _TitleRenderers;
        private ITableRendererCreater[] _TableRendererCreaters;

        private int _TitleFontSize = -1;
        private Color _TitleFontColor;
        private bool _SettedTitleFontColor;
        
        private string[] _Titles;
        private int _Height = -1;

        private bool _NeedCalUI = true;
        
        private int[] _ColumnWidth;
        private int[] _LineHeight;
        private int _TitleHeight;
        private int _TotalBodyHeight;
        private int _TotalWidth;

        private int[] _Borders = { 1, 1, 1, 1};
        private Color _BorderColor = Color.gray;

        public Table(string[] titles)
        {
            SetTitle(titles);
        }

        public Table SetTitle(string[] titles)
        {
            _Titles = titles;
            
            _TitleRenderers = new Label[titles.Length];
            for (int i = 0; i < _TitleRenderers.Length; i++)
            {
                _TitleRenderers[i] = new Label(_Titles[i]).SetPadding(5).SetFontSize(_TitleFontSize);
                if (_SettedTitleFontColor)
                {
                    _TitleRenderers[i].SetFontColor(_TitleFontColor);    
                }
            }

            _NeedCalUI = true;
            return this;
        }

        public Table SetDataModel(ITableDataModel dataModel)
        {
            this._TableDataModel = dataModel ?? _EMPTY_DATA_MODEL;
            _NeedCalUI = true;
            return this;
        }

        public Table SetRendererCreaters(ITableRendererCreater[] tableRendererCreaters)
        {
            _TableRendererCreaters = tableRendererCreaters;
            _NeedCalUI = true;
            return this;
        }

        protected override Rect RenderSelf(int x, int y, ref bool repaint)
        {
            _CheckDirty();
            
            float xSrc = x;
            for (int i = 0; i < _Titles.Length; i++)
            {
                var labelRect = new Rect(xSrc, y, _ColumnWidth[i], _TitleHeight);
                _TitleRenderers[i].Render((int)xSrc, y, ref repaint);
                RenderBorder(labelRect, _Borders, _BorderColor);
                xSrc += _ColumnWidth[i] - 1;
            }
            
            int columnY = y + _TitleHeight;
            for (int row = 0; row < _BodyRenderers.Length; row++)
            {
                float lineHeight = 0;
                int columnX = x;
                for (int column = 0; column < _BodyRenderers[0].Length; column++)
                {
                    var renderer = _BodyRenderers[row][column];
                    renderer.Render(columnX, columnY, ref repaint);
                    RenderBorder(new Rect(columnX, columnY, _ColumnWidth[column], _LineHeight[row]), _Borders, _BorderColor);
                    columnX += _ColumnWidth[column] - 1;
                }
                columnY += _LineHeight[row];
            }

            Vector2 size = SelfSize();
            return new Rect(x, y, size.x, size.y);
        }
        
        private void _CheckDirty()
        {
            if (!_NeedCalUI)
            {
                return;
            }

            _NeedCalUI = false;

            if (_TableRendererCreaters == null || _TableRendererCreaters.Length != _Titles.Length)
            {
                _TableRendererCreaters = new ITableRendererCreater[_Titles.Length];

                for (int i = 0; i < _TableRendererCreaters.Length; i++)
                {
                    _TableRendererCreaters[i] = new LabelRenderCreater();
                }
            } 

            if (_BodyRenderers == null || _BodyRenderers.Length != _TableDataModel.Row)
            {
                _BodyRenderers = new ITableRenderer[_TableDataModel.Row][];
            }
 
            if (_BodyRenderers.Length > 0 && (_BodyRenderers[0] == null || _BodyRenderers[0].Length != _Titles.Length))
            {
                for (int row = 0; row < _BodyRenderers.Length; row++)
                {
                    _BodyRenderers[row] = new ITableRenderer[_Titles.Length];
                    
                    for (int column = 0; column < _BodyRenderers[0].Length; column++)
                    {
                        _BodyRenderers[row][column] = _TableRendererCreaters[column].Create(_TableDataModel.GetData(column, row), column, row, this);
                    }
                }
            }
            else
            {
                for (int row = 0; row < _BodyRenderers.Length; row++)
                {
                    for (int column = 0; column < _BodyRenderers[0].Length; column++)
                    {
                        _BodyRenderers[row][column].Update(_TableDataModel.GetData(column, row), _TableDataModel, column, row, this);
                    }
                }
            }

            if (_LineHeight == null || _LineHeight.Length != _BodyRenderers.Length)
            {
                _LineHeight = new int[_BodyRenderers.Length];
            }

            if (_ColumnWidth == null || _ColumnWidth.Length != _Titles.Length)
            {
                _ColumnWidth = new int[_Titles.Length];
            }

            _TitleHeight = 0;
            for (int i = 0; i < _Titles.Length; i++)
            {
                var size = _TitleRenderers[i].Size();
                _ColumnWidth[i] = (int) size.x;
                _TitleHeight = (int)Mathf.Max(_TitleHeight, size.y);
            }

            for (int row = 0; row < _BodyRenderers.Length; row++)
            {
                float lineHeight = 0;
                for (int column = 0; column < _BodyRenderers[0].Length; column++)
                {
                    var renderer = _BodyRenderers[row][column];
                    var size = renderer.Size();
                    
                    lineHeight = Mathf.Max(size.y);
                    _ColumnWidth[column] = (int) Mathf.Max(_ColumnWidth[column], size.x);
                }
                _LineHeight[row] = (int)lineHeight;
            }

            _TotalBodyHeight = 0;
            _TotalWidth = 0;
            foreach (var lineHeight in _LineHeight)
            {
                _TotalBodyHeight += lineHeight;
            }
            foreach (var columnWidth in _ColumnWidth)
            {
                _TotalWidth += columnWidth - 1;
            }
        }

        protected override Vector2 SelfSize()
        {
            _CheckDirty();
            return new Vector2(_TotalWidth, _Height >= 0 ? _Height : _TitleHeight + _TotalBodyHeight);
        }
        
        public Table SetFontSize(int fontSize)
        {
            this._TitleFontSize = fontSize;
            if (_TitleRenderers != null)
            {
                for (int i = 0; i < _TitleRenderers.Length; i++)
                {
                    _TitleRenderers[i].SetFontSize(fontSize);
                }
            }
            return this;
        }
        
        public Table SetFontColor(Color color)
        {
            _SettedTitleFontColor = true;
            this._TitleFontColor = color;
            if (_TitleRenderers != null)
            {
                for (int i = 0; i < _TitleRenderers.Length; i++)
                {
                    _TitleRenderers[i].SetFontColor(color);
                }
            }
            return this;
        }

        public ITableDataModel GetDataModel()
        {
            return _TableDataModel;
        }

        public string[] GetTitles()
        {
            return _Titles;
        }
    }
}