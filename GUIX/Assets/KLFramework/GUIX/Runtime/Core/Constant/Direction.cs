using System;
using System.ComponentModel;

namespace KLFramework.GUIX
{
    
    //******************************************
    // Direction
    // Definition pf 4 directions about TOP、RIGHT、BOTTOM and LEFT 
    //
    // @Author: Kakashi
    // @Email: junhong.cai@kunlun-inc.com
    // @Date: 2020-04-07 15:00
    //******************************************
    public enum Direction
    {
        TOP = 0,
        RIGHT = 1,
        BOTTOM = 2,
        LEFT = 3,
    }

    public class Directions
    {
        public static readonly Direction[] DIRECTIONS = { Direction.TOP, Direction.RIGHT, Direction.BOTTOM, Direction.LEFT };
        
        public static Direction ValueOf(int value)
        {
            switch (value)
            {
                case (int)Direction.TOP:
                    return Direction.TOP;
                case (int)Direction.RIGHT:
                    return Direction.RIGHT;
                case (int)Direction.BOTTOM:
                    return Direction.BOTTOM;
                case (int)Direction.LEFT:
                    return Direction.LEFT;
                default:
                    throw new InvalidEnumArgumentException("Argument `value` must in [0, 3]");
            }
        }
    }
}