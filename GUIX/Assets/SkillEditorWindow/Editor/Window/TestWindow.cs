﻿using UnityEditor;
using UnityEngine;

public class TestWindow : EditorWindow
{
    [MenuItem("Tools/TestWindow")]
    public static void GetWindow()
    {
        GetWindow<TestWindow>();
    }

    private int LogCount = 0;

    void OnGUI()
    {
        GUIStyle GuistyleBoxDND = new GUIStyle(GUI.skin.box);
        GuistyleBoxDND.alignment = TextAnchor.MiddleCenter;
        GuistyleBoxDND.fontStyle = FontStyle.Italic;
        GuistyleBoxDND.fontSize = 12;

        var myRect = new Rect(0, 0, 200, 30);
        GUI.Box(myRect, "Box", GuistyleBoxDND);
        if (myRect.Contains(Event.current.mousePosition))
        {
            if (Event.current.type == EventType.DragUpdated)
            {
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                Debug.LogError("DragUpdated");
                Event.current.Use();
            }

            if (Event.current.type == EventType.DragPerform)
            {
                Debug.LogError("DragPerform");
                Debug.Log(DragAndDrop.objectReferences.Length);
                for (int i = 0; i < DragAndDrop.objectReferences.Length; i++)
                {
                    Debug.LogError("drap obj = " + (DragAndDrop.objectReferences[i] as GameObject));
                }
                Event.current.Use();
            }

            if (Event.current.type == EventType.DragExited)
            {
                Debug.LogError("DragExited");
                Event.current.Use();
            }
        }

        if (GUI.Button(new Rect(0, 150, 100, 20), "clear"))
        {
            LogCount = 0;
        }

        if (GUI.Button(new Rect(0, 300, 100, 20), "Show"))
        {
            EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, null, 0);
        }

        if (Event.current.commandName == "ObjectSelectorUpdated")
        {
            LogCount++;
            Debug.LogError("ObjectSelectorUpdated" + "  LogCount = " + LogCount);
            Event.current.Use();
        }
        else if (Event.current.commandName == "ObjectSelectorClosed")
        {
            LogCount++;
            Debug.LogError("ObjectSelectorClosed-----------" + "  LogCount = " + LogCount);
            Event.current.Use();
        }



    }


}
