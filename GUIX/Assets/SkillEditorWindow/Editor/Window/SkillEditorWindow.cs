﻿using System;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using KLFramework.GUIX.Timeline;

namespace KLFramework.GUIX.Editor
{
    public class SkillEditorWindow : EditorWindow
    {
        public static SkillEditorWindow instance { get; private set; }

        //[SerializeField]
        //EditorGUIUtility.EditorLockTracker m_LockTracker = new EditorGUIUtility.EditorLockTracker();

        private ITimeline _GUITimeline;
        private Assets.KLFramework.GUIX.Timeline _Timeline;
        private string _Path;
        private object _lastSelectObject;
        private GameObject _lastSelectGameObject;

        void OnEnable()
        {
            if (instance == null)
                instance = this;
        }

        void OnDisable()
        {
            if (instance == this)
                instance = null;

        }

        void OnDestroy()
        {
            Debug.LogError("Window OnDestroy");
            //TimelineSerialize.Serialize();
            if (_GUITimeline != null)
            {
                _GUITimeline.Destroy();
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        void OnFocus()
        {
        }

        void OnLostFocus()
        {
        }

        void OnHierarchyChange()
        {
        }

        void OnGUI()
        {
            GUITimeline timeline = null;
            if (Selection.activeObject != null)
            {
                timeline = Selection.activeObject as GUITimeline;
                if (timeline != null && _lastSelectObject != Selection.activeObject)
                {
                    _lastSelectObject = Selection.activeObject;
                    string path = AssetDatabase.GetAssetPath(Selection.activeObject);
                    if (string.IsNullOrEmpty(path)) return;
                    InitTimeline(timeline, path);
                    _lastSelectGameObject = null;
                }
            }

            if (Selection.activeGameObject != null)
            {
                GameObject gameObject = Selection.activeGameObject;
                if (gameObject != null)
                {
                    if (_lastSelectGameObject != gameObject)
                    {
                        SkillDirector director = gameObject.GetComponent<SkillDirector>();
                        if (director != null && director._Timeline != null && director._Timeline != _GUITimeline)
                        {
                            string path = AssetDatabase.GetAssetPath(director._Timeline);
                            if (string.IsNullOrEmpty(path)) return;
                            InitTimeline(director._Timeline, path);
                            _lastSelectObject = null;
                        }
                    }
                    _lastSelectGameObject = gameObject;
                }
            }

            if (_GUITimeline == null)
            {
                if (GUI.Button(new Rect((Screen.width - 150) / 2, (Screen.height - 50) / 2, 150, 50), "Create"))
                {
                    string path = GetSelectionDirectoryPath();
                    string savePath = EditorUtility.SaveFilePanel("Save", path, null, "asset"); //full path
                    savePath = savePath.Substring(savePath.IndexOf("Assets"));
                    timeline = ScriptableObject.CreateInstance<GUITimeline>();
                    InitTimeline(timeline, AssetDatabase.GetAssetPath(timeline));
                    AssetDatabase.CreateAsset(timeline, savePath);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
                return;
            }

            TimelineGUIs.CurrentTimeline = _GUITimeline;
            bool repaint = false;
            _GUITimeline.SetSize(Screen.width, Screen.height);
            _GUITimeline.Render(0, 0, ref repaint);
            if (repaint)
            {
                Repaint();
            }
        }

        private void InitTimeline(GUITimeline timeline, string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogError("path == null");
                return;
            }
            timeline.Path = path;
            timeline.Init(Screen.width, Screen.height);
            timeline.RegisterNodeType<AnimationNodeData>("Animation");
            timeline.RegisterNodeType<DDDDD>("DDDDD");
            _GUITimeline = timeline;
        }

        #region Entrance
        [MenuItem("Assets/Create/SkillEditor", false, 460)]
        public static void CreateNewTimeline()
        {
            GetWindow<SkillEditorWindow>();
            instance.Focus();
        }

        [MenuItem("Tools/SkillEditor", false, 1)]
        public static void ShowWindow()
        {
            GetWindow<SkillEditorWindow>();
            instance.Focus();
        }

        [OnOpenAsset(1)]
        public static bool OnDoubleClick(int instanceID, int line)
        {
            var assetDoubleClick = EditorUtility.InstanceIDToObject(instanceID) as GUITimeline;
            if (assetDoubleClick == null)
                return false;

            ShowWindow();
            //instance.SetCurrentTimeline(assetDoubleClicked);

            return true;
        }

        [MenuItem("Assets/Create/SkillAsset", false, 470)]
        public static void CreateSkillAsset()
        {
            string path = GetSelectionDirectoryPath();
            if (path == null) return;
          
            string suffix = ".asset";
            string tempFileName = "SkillAsset" + suffix;
            int num = 0;
            while (File.Exists(Path.Combine(path, tempFileName)))
            {
                num++;
                tempFileName = string.Format("SkillAsset({0}){1}", num, suffix);
            }

            string assetPath = Path.Combine(path, tempFileName);
            GUITimeline timeline = ScriptableObject.CreateInstance<GUITimeline>();
            timeline.Init(Screen.width, Screen.height);
            //GUITimeline timeline = new GUITimeline(Screen.width, Screen.height);
            AssetDatabase.CreateAsset(timeline, assetPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Selection.activeObject = timeline;
            //string savePath = EditorUtility.SaveFilePanel("Save", path, null, "asset");
            //Debug.LogError("savePAth = " + savePath);
        }
        #endregion

        public static string GetSelectionDirectoryPath()
        {
            if (Selection.activeObject != null)
            {
                string path = AssetDatabase.GetAssetPath(Selection.activeObject);
                if (!Directory.Exists(path))
                {
                    path = path.Substring(0, path.LastIndexOf('/'));
                }
                return path;
            }

            return null;
        }

    }
}

